
// Read input
var codeJs = _INPUT_.code.input.js;
var display = nash.instance.load("display.xml");

display.bind("code.exec", {
	"code": null,
	"output": null,
	"exception": null,
});

if ((codeJs == null) || (codeJs === "")) return;


// Start exec
try {
	display.bind("code.exec", {
		"code": "<h1>Code</h1><pre><code>" + codeJs + "</code></pre>",
	});

	var output = eval("(function(i) {" + codeJs + "})()")

	display.bind("code.exec", {
		"output": "<h1>Ouput</h1><pre><code>" + output + "</code></pre>",
	});

} catch (error_msg) {
	display.bind("code.exec", {
		"exception": "<h1>Exception</h1><pre><code>" + error_msg + "</code></pre>",
	});
}
