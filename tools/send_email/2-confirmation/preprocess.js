//recievers list
var courrielList = '';
var totalReceivers = _INPUT_.dataGroup.receiverGroup.email.size();

for (i = 0; i < totalReceivers; i++) {
    courrielList = courrielList + '\n' + _INPUT_.dataGroup.receiverGroup.email[i];
}

// Recivers group
var groupRecievers = spec.createGroup({
    'id': 'recievers',
    'label': 'Destinataire(s)',
    'data': [
        spec.createData({
            'id': 'dataEmail',
            'type': 'TextReadOnly',
            'value': courrielList 
        })
    ]
});

//Object and Content group
var bodyGroup = spec.createGroup({
    'id': 'courrielGroup',
    'label': 'Informations relatives au courrier électronique',
    'data': [
        spec.createData({
            'id': 'object',
            'label': 'Objet :',
            'type': 'StringReadOnly',
            'value': _INPUT_.dataGroup.courrielGroup.object
        }),
        spec.createData({
            'id': 'content',
            'label': 'Contenu :',
            'type': 'TextReadOnly',
            'value': _INPUT_.dataGroup.courrielGroup.content
        }),
        spec.createData({
            'id': 'attachment',
            'label': 'Pièce jointe :',
            'type': 'FileReadOnly',
            'value': _INPUT_.dataGroup.courrielGroup.attachment
        })
    ]
});


return spec.create({
    id : 'groups',
    label : 'Confirmation des informations',
    help: 'Ci-dessous, un récapitulatif de votre saisie vous est présenté. À la confirmation de cette étape, votre courrier électronique sera envoyé.',
    groups : [ 
         spec.createGroup({
            id : 'recieversGroup',
            label : 'Rappel des informations',
            data : [groupRecievers,bodyGroup]
        }),
    ]
});