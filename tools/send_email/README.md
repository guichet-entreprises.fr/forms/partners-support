# Description of sending an email :
<br />

The goal is to send an email request using the email channel.

<br />
<br />

# Team(s) concerned :
* The support team

# First step description : Form input
<br />

The form allow the user to enter the input information for the recipient of your choice.
The email could be sent to multiple recipients (if necessary). 

<br />
<br />

## Second step description : Confirm form
<br />

This step will confirm the input information entered by the user previously.


## Third step description : Send an email request
<br />

This step will send an email request using the email channel.


