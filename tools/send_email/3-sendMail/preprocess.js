//--------------- prepare info  before send mail -------------------------

// number of receivers
var totalReceivers = _INPUT_.dataGroup.receiverGroup.email.size();

// the object of email 
var object = _INPUT_.dataGroup.courrielGroup.object;

// the content of email
var content = "<html>" + _INPUT_.dataGroup.courrielGroup.content + "</html>";

// the attachment name
var attachment = _INPUT_.dataGroup.courrielGroup.attachment[0].getLabel();

_log.info("status attachment is {}", attachment);


// the attachment path
var filePath = '/1-data/uploaded/dataGroup.courrielGroup.attachment-1-'+attachment;

// the email sender
var exchangeSender = _CONFIG_.get('exchange.email.sender');

// the record uid
var attachmentExtention = attachment.substring(attachment.lastIndexOf(".")+1, attachment.length).toLowerCase();
var nomDossier = "DOSSIER-" + nash.record.description().recordUid + '.' + attachmentExtention;

// the receivers list as string
var receiversList = _INPUT_.dataGroup.receiverGroup.email.toString().replace('[', '"').replace(']', '"');

// the receivers list as string table
receiversList = receiversList.replace(/, /g , '", "');

var data = [];

// call exchange service to send mail with pj
try {
    var response = nash.service.request('${exchange.baseUrl}/email/send') //
	.connectionTimeout(2000) //
	.receiveTimeout(5000) //
	.dataType("form") //
	.param("sender", [ exchangeSender ]) //
	.param("recipient", [receiversList]) //
	.param("object", [ object ]) //
	.param("content", [ content ]) //
	.param("attachmentPDFName", [ nomDossier ]) //
	.post({ "file" : nash.util.resourceFromPath(filePath)});

	//Show success message if response is 200
	if(response.status == 200){
		return spec.create({ 
			id : 'ConfirmSend',
			label : 'Résultat de la demande',
			groups : [ spec.createGroup({
				id : 'group',
				label : 'Statut',
				help : 'Votre courrier électronique a été envoyé avec succès.'
			}) ]
		});
	}
	//show message with the potential error and block the step
	else{
		//Response
		return spec.create({ 
			id : 'ConfirmSend',
			label : 'Résultat de la demande',
			groups : [ 
				spec.createGroup({
					id : 'group',
					label : 'Statut',
					warnings : [
						{ id : 'warningConfiguration', label : response.status }
					]
				}) 
			]
		});
	}
//show error message and block the step	
}catch(error) {
	//Response
	return spec.create({ 
		id : 'ConfirmSend',
		label : 'Résultat de la demande',
		groups : [ 
			spec.createGroup({
				id : 'group',
				label : 'Statut',
				warnings : [
					{ id : 'warningConfiguration', label : error  }
				]
			}) 
		]
	});
}