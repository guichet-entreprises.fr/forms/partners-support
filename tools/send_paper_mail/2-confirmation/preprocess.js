//Object and Content group
var contentGroup = spec.createGroup({
    'id': 'courrielGroup',
    'label': 'Informations postales du destinataire',
    'data': [
        spec.createData({
            'id': 'referenceId',
            'label': 'Référence du courrier :',
            'type': 'StringReadOnly',
            'value': $courrier.dataGroup.receiverInfoGroup.referenceId
        }),
        spec.createData({
            'id': 'recipientName',
            'label': 'Nom du destinataire :',
            'type': 'StringReadOnly',
            'value': $courrier.dataGroup.receiverInfoGroup.recipientName
        }),
        spec.createData({
            'id': 'recipientNameCompl',
            'label': 'Nom complet du destinataire :',
            'type': 'StringReadOnly',
            'value': $courrier.dataGroup.receiverInfoGroup.recipientNameCompl
        }),
        spec.createData({
            'id': 'recipientAddressName',
            'label': 'Adresse :',
            'type': 'StringReadOnly',
            'value': $courrier.dataGroup.receiverInfoGroup.recipientAddressName
        }),
        spec.createData({
            'id': 'recipientAddressNameCompl',
            'label': 'Complément d\'adresse :',
            'type': 'StringReadOnly',
            'value': $courrier.dataGroup.receiverInfoGroup.recipientAddressNameCompl
        }),
        spec.createData({
            'id': 'recipientPostalCode',
            'label': 'Code postal :',
            'type': 'StringReadOnly',
            'value': $courrier.dataGroup.receiverInfoGroup.recipientPostalCode
        }),

        spec.createData({
            'id': 'recipientCity',
            'label': 'Ville :',
            'type': 'StringReadOnly',
            'value': $courrier.dataGroup.receiverInfoGroup.recipientCity
        })
    ]
});


//Attachment group
var attachmentGroup = spec.createGroup({
    'id': 'attachmentGroup',
    'label': 'Courrier :',
    'data': [
        spec.createData({
            'id': 'attachment',
            'type': 'FileReadOnly',
            'value': $courrier.dataGroup.attachmentGroup.attachment
        })
    ]
});


return spec.create({
    id : 'groups',
    label : 'Confirmation des informations',
    help: 'Vous êtes sur le point d’envoyer un courrier postal avec un accusé de réception. Ci-dessous, un récapitulatif de votre saisie vous est présenté.',
    groups : [
         spec.createGroup({
            id : 'recieversGroup',
            label : 'Rappel des informations',
            data : [contentGroup,attachmentGroup]
        }),
    ]
});