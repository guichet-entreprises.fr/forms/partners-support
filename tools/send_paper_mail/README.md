# Description of posting a mail :
<br />

The goal is to create a postmail request using the postmail channel.

<br />
<br />

# Team(s) concerned :
* The support team

# First step description : Form input
<br />

The form allow the user to enter the postal coordinates of the recipient of your choice 


<br />
<br />

# Second step description : Confirm form
<br />

This step will confirm the input information entered by the user previously.


# Third step description : Send a postmail request
<br />

This step will send a postmail request using the postmail channel.


