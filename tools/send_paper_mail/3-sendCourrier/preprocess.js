// prepare info to send

//the reference id
var referenceId = _INPUT_.dataGroup.receiverInfoGroup.referenceId;

// the recipient name
var recipientName = _INPUT_.dataGroup.receiverInfoGroup.recipientName;

// the complet name of the recipient
var recipientNameCompl = !_INPUT_.dataGroup.receiverInfoGroup.recipientNameCompl ? '' :_INPUT_.dataGroup.receiverInfoGroup.recipientNameCompl;

// the recipient address
var recipientAddressName = _INPUT_.dataGroup.receiverInfoGroup.recipientAddressName;

// the complet address of the recipient
var recipientAddressNameCompl =  !_INPUT_.dataGroup.receiverInfoGroup.recipientAddressNameCompl ? '' :_INPUT_.dataGroup.receiverInfoGroup.recipientAddressNameCompl;

// the postal code
var recipientPostalCode = _INPUT_.dataGroup.receiverInfoGroup.recipientPostalCode;

// the attachment name
var recipientCity = _INPUT_.dataGroup.receiverInfoGroup.recipientCity;

// the recipient city
var attachment = _INPUT_.dataGroup.attachmentGroup.attachment[0].getLabel();

// the attachment path
var filePath = '/1-data/uploaded/dataGroup.attachmentGroup.attachment-1-'+attachment;

var data = [];

// call exchange service to send mail with pj
try {
    var response = nash.service.request('${exchange.baseUrl}/private/postmail') //
	.connectionTimeout(2000) //
	.receiveTimeout(5000) //
	.dataType("form") //
	.param("referenceId", referenceId) //
	.param("recipientName", recipientName) //
	.param("recipientNameCompl", recipientNameCompl) //
	.param("recipientAddressName", recipientAddressName) //
	.param("recipientAddressNameCompl", recipientAddressNameCompl) //
	.param("recipientPostalCode", recipientPostalCode) //
	.param("recipientCity", recipientCity) //
	.put({ "attachmentFile" : nash.util.resourceFromPath(filePath)});

	//Show success message if response is 200
	if(response.status == 200){
		return spec.create({ 
			id : 'ConfirmSend',
			label : 'Résultat de la demande',
			groups : [ spec.createGroup({
				id : 'group',
				label : 'Statut',
				help : 'Votre courrier papier a été envoyé avec succès.'
			}) ]
		});
	}
	//show message with the potential error and block the step
	else{
		//Response
		return spec.create({ 
			id : 'ConfirmSend',
			label : 'Résultat de la demande',
			groups : [ 
				spec.createGroup({
					id : 'group',
					label : 'Statut',
					warnings : [
						{ id : 'warningConfiguration', label : "Une erreur technique est survenue lors de l\'envoi de votre courrier papier. Veuillez contacter le support technique à l'adresse suivante : support.guichet-entreprises@helpline.fr." }
					]
				}) 
			]
		});
	}
//show error message and block the step	
}catch(error) {
	//Response
	return spec.create({ 
		id : 'ConfirmSend',
		label : 'Résultat de la demande',
		groups : [ 
			spec.createGroup({
				id : 'group',
				label : 'Statut',
				warnings : [
					{ id : 'warningConfiguration', label : "Une erreur technique est survenue lors de l\'envoi de votre courrier papier. Veuillez contacter le support technique à l'adresse suivante : support.guichet-entreprises@helpline.fr." }
				]
			}) 
		]
	});
}