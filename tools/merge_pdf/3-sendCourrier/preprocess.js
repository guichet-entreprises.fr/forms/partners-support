// Cas Courriel
if(Value('id').of($dk45T2.dk45.identificationDestinataire.canal).eq('courriel')){
	//--------------- prepare info  before send mail -------------------------

	// number of receivers
	var totalReceivers = $dk45T2.dk45.receiverGroup.email.size();

	// the object of email 
	var object = $dk45T2.dk45.courrielGroup.object;

	// the content of email
	var content = "<html>" + $dk45T2.dk45.courrielGroup.content + "</html>";

	// the attachment name
	//var attachment = $dk45T2.dk45.courrielGroup.attachment[0].getLabel();

	//_log.info("status attachment is {}", attachment);


	// the attachment path
	var filePath = _INPUT_.finalDocItem;

	// the email sender
	var exchangeSender = _CONFIG_.get('exchange.email.sender');

	// the record uid
	//var attachmentExtention = attachment.substring(attachment.lastIndexOf(".")+1, attachment.length).toLowerCase();
	//var nomDossier = "DOSSIER-" + nash.record.description().recordUid + '.' + attachmentExtention;

	// the receivers list as string
	var receiversList = $dk45T2.dk45.receiverGroup.email.toString().replace('[', '"').replace(']', '"');

	// the receivers list as string table
	receiversList = receiversList.replace(/, /g , '", "');

	var data = [];

	// call exchange service to send mail with pj
	try {
	    var response = nash.service.request('${exchange.baseUrl}/email/send') //
		.connectionTimeout(2000) //
		.receiveTimeout(5000) //
		.dataType("form") //
		.param("sender", [ exchangeSender ]) //
		.param("recipient", [receiversList]) //
		.param("object", [ object ]) //
		.param("content", [ content ]) //
		//.param("attachmentPDFName", [ nomDossier ]) //
		.post({ "file" : [ filePath ]});

		//Show success message if response is 200
		if(response.status == 200){
			return spec.create({ 
				id : 'ConfirmSend',
				label : 'Résultat de la demande',
				groups : [ spec.createGroup({
					id : 'group',
					label : 'Statut',
					help : 'Votre courrier électronique a été envoyé avec succès.'
				}) ]
			});
		}
		//show message with the potential error and block the step
		else{
			//Response
			return spec.create({ 
				id : 'ConfirmSend',
				label : 'Résultat de la demande',
				groups : [ 
					spec.createGroup({
						id : 'group',
						label : 'Statut',
						warnings : [
							{ id : 'warningConfiguration', label : "Une erreur technique est survenue lors de l'envoi de votre courrier électronique. Veuillez contacter le support technique à l'adresse suivante : support.guichet-entreprises@helpline.fr." }
						]
					}) 
				]
			});
		}
	//show error message and block the step	
	}catch(error) {
		//Response
		return spec.create({ 
			id : 'ConfirmSend',
			label : 'Résultat de la demande',
			groups : [ 
				spec.createGroup({
					id : 'group',
					label : 'Statut',
					warnings : [
						{ id : 'warningConfiguration', label : "Une erreur technique est survenue lors de l'envoi de votre courrier électronique. Veuillez contacter le support technique à l'adresse suivante : support.guichet-entreprises@helpline.fr." }
					]
				}) 
			]
		});
	}
}


// Cas Courrier
if(Value('id').of($dk45T2.dk45.identificationDestinataire.canal).eq('courrier')){
	// prepare info to send

	//the reference id
	var referenceId = '';

	// the recipient name
	var recipientName = $groupDirectory.autoriteGroup.contentGroup.content1;

	// the complet name of the recipient
	var recipientNameCompl = '';

	// the recipient address
	var recipientAddressName = $groupDirectory.autoriteGroup.contentGroup.content2;

	// the complet address of the recipient
	var recipientAddressNameCompl =  '';


	var cpCitySplit = $groupDirectory.autoriteGroup.contentGroup.content3.split(", ");
	// the postal code
	var recipientPostalCode = cpCitySplit[0];

	// the recipient city
	var recipientCity = cpCitySplit[1];

	// the attachment path
	var filePath = _INPUT_.finalDocItem;

	var data = [];

	// call exchange service to send mail with pj
	try {
	    var response = nash.service.request('${exchange.baseUrl}/private/postmail') //
		.connectionTimeout(2000) //
		.receiveTimeout(5000) //
		.dataType("form") //
		.param("referenceId", referenceId) //
		.param("recipientName", recipientName) //
		.param("recipientNameCompl", recipientNameCompl) //
		.param("recipientAddressName", recipientAddressName) //
		.param("recipientAddressNameCompl", recipientAddressNameCompl) //
		.param("recipientPostalCode", recipientPostalCode) //
		.param("recipientCity", recipientCity) //
		.put({ "attachmentFile" : [ filePath ]});

		//Show success message if response is 200
		if(response.status == 200){
			return spec.create({ 
				id : 'ConfirmSend',
				label : 'Résultat de la demande',
				groups : [ spec.createGroup({
					id : 'group',
					label : 'Statut',
					help : 'Votre courrier papier a été envoyé avec succès.'
				}) ]
			});
		}
		//show message with the potential error and block the step
		else{
			//Response
			return spec.create({ 
				id : 'ConfirmSend',
				label : 'Résultat de la demande',
				groups : [ 
					spec.createGroup({
						id : 'group',
						label : 'Statut',
						warnings : [
							{ id : 'warningConfiguration', label : "Une erreur technique est survenue lors de l'envoi de votre courrier papier. Veuillez contacter le support technique à l'adresse suivante : support.guichet-entreprises@helpline.fr." }
						]
					}) 
				]
			});
		}
	//show error message and block the step	
	}catch(error) {
		//Response
		return spec.create({ 
			id : 'ConfirmSend',
			label : 'Résultat de la demande',
			groups : [ 
				spec.createGroup({
					id : 'group',
					label : 'Statut',
					warnings : [
						{ id : 'warningConfiguration', label : "Une erreur technique est survenue lors de l'envoi de votre courrier papier. Veuillez contacter le support technique à l'adresse suivante : support.guichet-entreprises@helpline.fr." }
					]
				}) 
			]
		});
	}
}