// Cas Courrier
if(Value('id').of($dk45T2.dk45.identificationDestinataire.canal).eq('courrier')){

	var destFuncId = $dk45T2.dk45.receiverInfoGroup.destinatiare.toUpperCase();

	// call directory with funcId to find all information of Authorithy

	var response = nash.service.request('${directory.baseUrl}/v2/ref/list/authority')
				   .param('filters','label:'+destFuncId)			   
		           .connectionTimeout(10000) //
		           .receiveTimeout(10000) //
		           .accept('json') //
		           .get();

	//result			   
				   
	var responseInfo = response.asObject();

	if (responseInfo.totalResults != 0 ){
	// prepare all information of receiver to create data.xml

	var receiverInfo = responseInfo.content[0]; 
	var funcId = !receiverInfo.entityId ? null : receiverInfo.entityId;
	var funcLabel = !receiverInfo.label ? null :receiverInfo.label;
	var details = !receiverInfo.details ? null : receiverInfo.details;
	_log.info("details is  {}",details);
	//var payment = !details.payment ? null : details.payment;
	var email = !details.profile.email ? null : details.profile.email;
	var tel = !details.profile.tel ? null : details.profile.tel;
	var fax = !details.profile.fax ? null : details.profile.fax;


	_log.info("adress is  {}",details.profile.address);
	_log.info("recipientName is  {}",details.profile.address.recipientName);
	_log.info("addressNameCompl is  {}",details.profile.address.addressNameCompl);
	_log.info("cityNumber is  {}",details.profile.address.cityNumber);


	var nomAuth = !details.profile.address.recipientName ? null : details.profile.address.recipientName;
	var authCP = !details.profile.address.postalCode ? null : details.profile.address.postalCode;
	var compl = !details.profile.address.compl ? null : details.profile.address.compl;
	var authAddressNameCompl = !details.profile.address.addressNameCompl ? null : details.profile.address.addressNameCompl;
	var authSpecial = !details.profile.address.special ? null : details.profile.address.special;
	var authCity = !details.profile.address.cityNumber ? null : details.profile.address.cityNumber;
	var cedex = !details.profile.address.cedex ? null : details.profile.address.cedex;


	var responseCommune = nash.service.request('${directory.baseUrl}/v2/ref/list/communes')
				   .param('filters','entityId:'+authCity)			   
		           .connectionTimeout(10000) //
		           .receiveTimeout(10000) //
		           .accept('json') //
		           .get();

	var responseInfoC = responseCommune.asObject();

	if (responseInfoC.totalResults != 0 ){
	var receiverInfoC = responseInfoC.content[0]; 
	var communeLabel = !receiverInfoC.label ? null :receiverInfoC.label;	
	}	
	var finalDoc = nash.doc.load('Documents/Document.pdf');

	function appendPj(fld) {
		fld.forEach(function (elm) {
	        finalDoc.append(elm);
	    });
	}
	/*
	 * Ajout des PJs
	 */
	appendPj($dk45T2.dk45.attachmentGroup.attachment0);
	appendPj($dk45T2.dk45.attachmentGroup.attachment1);
	appendPj($dk45T2.dk45.attachmentGroup.attachment2);
	appendPj($dk45T2.dk45.attachmentGroup.attachment3);
	appendPj($dk45T2.dk45.attachmentGroup.attachment4);
	appendPj($dk45T2.dk45.attachmentGroup.attachment5);
	appendPj($dk45T2.dk45.attachmentGroup.attachment6);
	appendPj($dk45T2.dk45.attachmentGroup.attachment7);
	appendPj($dk45T2.dk45.attachmentGroup.attachment8);
	appendPj($dk45T2.dk45.attachmentGroup.attachment9);

	/*
	 * Enregistrement du fichier (en mémoire)
	 */
	var finalDocItem = finalDoc.save('Dossier_final.pdf');
	
	var contentGroup = spec.createGroup({
			'id': 'contentGroup',
			'label': 'Envoi du courrier à :',
			'data': [
				spec.createData({
					'id': 'content1',
					'type': 'String',
					'value': nomAuth
				}),
				spec.createData({
					'id': 'content2',
					'type': 'String',
					'value': authAddressNameCompl + (!authSpecial ? '' : ', ' + authSpecial)
				}),
				spec.createData({
					'id': 'content3',
					'type': 'String',
					'value': authCP + ', ' + communeLabel
				}),
				spec.createData({
					'id': 'content4',
					'type': 'FileReadOnly',
					'label': 'Dossier à envoyer :',
					'value' : [finalDocItem]
				})
			]
		});		
	}
	return spec.create({
		    id : 'groupDirectory',
		    label : 'Confirmation des informations',
			help: 'Voici les informations du CFE : ' + nomAuth,
		    groups : [
		 		spec.createGroup({
					id : 'autoriteGroup',
					label : 'Rappel des informations',
					data : [contentGroup]
				}),
			]
		});

}

// Cas Merge
/*if(Value('id').of($dk45T2.dk45.identificationDestinataire.canal).eq('merge')){
	var contentGroup = spec.createGroup({
		'id': 'mergeGroup1',
		'label': 'Informations sur le merge',
		'data': [
			spec.createData({
				'id': 'referenceId',
				'label': 'Fichiers principal du merge :',
				'type': 'FileReadOnly',
				'value': $dk45T2.dk45.mergeGroup.attachment0
			})
		]
	});

	//Attachment group
	return spec.create({
	    id : 'groupMerge',
	    label : 'Confirmation des informations',
		help: "Vous êtes sur le point de merger des fichiers à celui ci. Le fichier ci-dessous sera en première page, si vous souhaitez continuer veuillez passer à l'étape suivante.",
	    groups : [
	 		spec.createGroup({
				id : 'mergeGroup',
				label : 'Rappel des informations',
				data : [contentGroup]
			}),
		]
	});
}*/


// Cas Courriel
if(Value('id').of($dk45T2.dk45.identificationDestinataire.canal).eq('courriel')){
	//recievers list
	var courrielList = '';
	var totalReceivers = _INPUT_.dk45.receiverGroup.email.size();
	for (i = 0; i < totalReceivers; i++) {
		courrielList = courrielList + '\n' + _INPUT_.dk45.receiverGroup.email[i];
	}
	var finalDoc = nash.doc.load('Documents/Document.pdf');

	function appendPj(fld) {
		fld.forEach(function (elm) {
	        finalDoc.append(elm);
	    });
	}
	/*
	 * Ajout des PJs
	 */
	appendPj($dk45T2.dk45.courrielGroup.attachment0);
	appendPj($dk45T2.dk45.courrielGroup.attachment1);
	appendPj($dk45T2.dk45.courrielGroup.attachment2);
	appendPj($dk45T2.dk45.courrielGroup.attachment3);
	appendPj($dk45T2.dk45.courrielGroup.attachment4);
	appendPj($dk45T2.dk45.courrielGroup.attachment5);
	appendPj($dk45T2.dk45.courrielGroup.attachment6);
	appendPj($dk45T2.dk45.courrielGroup.attachment7);
	appendPj($dk45T2.dk45.courrielGroup.attachment8);
	appendPj($dk45T2.dk45.courrielGroup.attachment9);

	/*
	 * Enregistrement du fichier (en mémoire)
	 */
	var finalDocItem = finalDoc.save('Dossier_final.pdf');
	// Recivers group
	var groupRecievers = spec.createGroup({
		'id': 'recievers',
		'label': 'Destinataire(s)',
		'data': [
			spec.createData({
				'id': 'dataEmail',
				'type': 'TextReadOnly',
				'value': courrielList 
			})
		]
	});

	//Object and Content group
	var bodyGroup = spec.createGroup({
		'id': 'courrielGroup',
		'label': 'Informations relatives au courrier électronique',
		'data': [
			spec.createData({
				'id': 'object',
				'label': 'Objet :',
				'type': 'StringReadOnly',
				'value': _INPUT_.dk45.courrielGroup.object
			}),
			spec.createData({
				'id': 'content',
				'label': 'Contenu :',
				'type': 'TextReadOnly',
				'value': _INPUT_.dk45.courrielGroup.content
			}),
			spec.createData({
				'id': 'attachment',
				'label': 'Pièce jointe :',
				'type': 'FileReadOnly',
				'value': [finalDocItem]
			})
		]
	});


	return spec.create({
	    id : 'groupCourriel',
	    label : 'Confirmation des informations',
		help: 'Ci-dessous, un récapitulatif de votre saisie vous est présenté. À la confirmation de cette étape, votre courrier électronique sera envoyé.',
	    groups : [ 
	 		spec.createGroup({
				id : 'recieversGroup',
				label : 'Rappel des informations',
				data : [groupRecievers,bodyGroup]
			}),
		]
	});
}