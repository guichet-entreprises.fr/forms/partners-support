function pad(s) { return (s < 10) ? '0' + s : s; }

var regulation = _input.data.regulation;

var reference = regulation.reference;
var counter = regulation.counter;
var applicationDate = regulation.applicationDate;

//-->Formatting date
var dateTmp = new Date(parseInt(regulation.applicationDate.getTimeInMillis()));
var applicationDateStr = dateTmp.getFullYear().toString();
var month = dateTmp.getMonth() + 1;
applicationDateStr = applicationDateStr.concat(pad(month.toString()));
applicationDateStr = applicationDateStr.concat(pad(dateTmp.getDate().toString()));

var comment = !regulation.comment ? null : regulation.comment;

var response_message = null;

try {
    var response = nash.service.request('${ws.clicker.url}/v1/event') //
    .param("ref", reference) //
    .param("counter", counter) //
    .param("effectDate", applicationDateStr) //
    .param("comment", comment) //
    .post(null);
    
    // Success
    if(response.status == 200){
        response_message = 'La régulation a été enregistré avec succès.';
    }
    // Error
    else{
        response_message = 'Une erreur technique est survenue lors de l\'archivage.';
    }
    
} catch(error) {
    log.info('Erreur lors de l\'enregistrement de la régulation => ' + error);
    response_message = 'Erreur lors de l\'enregistrement de la régulation.';
}

var data = nash.instance.load('data-generated.xml');
data.bind('regulation', {
    'result':{
        'response': response_message
    }
});
