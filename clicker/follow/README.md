# Description of monitoring Guichet Entreprises activity from CLICKER :

The goal is to monitor Guichet Entreprises activity.

# Teams concerned :
* The admin team

## First step : Monitoring Guichet Entreprises activity using criteria

Searching for events based on multiples criteria like the following ones :
* All records created or finalized
* Period date
* Any key word

This step will display the events resulting from the criteria.

Here are the information displayed :
* Event reference
* Total counter
