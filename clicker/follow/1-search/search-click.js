// try to find the id to search
var event = _INPUT_.record.search.event;
var reference = _INPUT_.record.search.reference;
var dateSearch = _INPUT_.record.search.date;

if (null == event) {
	return ;
}

// --------------------------------------------------------------------------------------findEvents
// Find the events
// --------------------------------------------------------------------------------------
function findEvents(event, dateSearch, reference, xmlDisplay, resultsXmlPath) {
	fullUrl = _CONFIG_.get('ws.clicker.url') + '/v1/event';

	//save call dfindEventsata
	xmlDisplay.bind(resultsXmlPath + ".input", {
		"url": fullUrl
	});
	
	
	try {		
		var clickerRequest = nash.service.request(fullUrl) //
			.connectionTimeout(10000) //
			.receiveTimeout(10000) //
			.param('startIndex', 0) //
			.param('maxResults', 100) //
			.param('orders', 'created:desc') //
			.param('orders', 'reference:desc') //
		;

		if ("other" == event.getId())	{
			clickerRequest.param('filters', "reference~" + reference);
			log.info('Filter by reference : {}', reference);
		} else if ("authority" == event.getId())	{
			var checkAuthority = _INPUT_.record.search.searchAuthority.check;
			if (checkAuthority) {
				clickerRequest.param('filters', "reference~" + _INPUT_.record.search.searchAuthority.authority.getLabel());
				log.info('Filter by reference : {}', _INPUT_.record.search.searchAuthority.authority.getLabel());
			} else {
				clickerRequest.param('filters', "reference~PROCESS");
				log.info('Filter by reference : PROCESS');
			}
		} else {
			clickerRequest.param('filters', "reference~" + event.getId());
			log.info('Filter by reference : {}', event.getId());
		}		
		
		if (null != dateSearch)	{
			if (null != dateSearch && null != dateSearch.from) {
				clickerRequest.param('filters', 'effectDateMin>=' + dateSearch.from.toInstant().toString().substring(0, 10));
				log.info('Filter by date effect inf : {}', dateSearch.from.toInstant().toString().substring(0, 10));
			}
			
			if (null != dateSearch && null != dateSearch.to) {
				clickerRequest.param('filters', 'effectDateMax<=' + dateSearch.to.toInstant().toString().substring(0, 10));
				log.info('Filter by date effect sup : {}', dateSearch.to.toInstant().toString().substring(0, 10));
			}
		}
			
		var searchResult = clickerRequest.get();
	} catch (error) {
		log.error('=> Error occured while calling clicker : ' + error);
		xmlDisplay.bind(resultsXmlPath + ".output", {
			"status": "ERROR",
			"response": "" + error,
		});
		return null;
	};

	responseObj = searchResult.asObject();
	
	xmlDisplay.bind(resultsXmlPath + ".output", {
		"status": searchResult.getStatus(),
		"response": "" + responseObj,
	});

	// test the 200 code
	if (searchResult == null || searchResult.getStatus() != 200) {
		return null;
	}

	return responseObj;
};
// --------------------------------------------------------------------------------------


// Load the screen for the user
var display = nash.instance.load("display-search.xml");

var events = findEvents(event, dateSearch, reference, display, "record.debug.clicker");

if (null == events) {
	display.bind("record.results.clicker", { "totalCounter": "0" });
	display.bind("record.results.clicker", { "event": [] });
	return; 
}

display.bind("record.results.clicker", { "totalCounter": events.totalCounter });
events.content.forEach(function (event) {
	var dateTemp = new Date(parseInt(event.created));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
	event.effectDate = year + "-" + month + "-" + day + ' ' + dateTemp.getHours() + ':' + dateTemp.getMinutes() + ':' + dateTemp.getSeconds() + ':' + dateTemp.getMilliseconds();
});
display.bind("record.results.clicker", { "event": events.content });