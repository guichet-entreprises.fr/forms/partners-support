// ======================================================================================
// ======================================================================================
// --------------------------------------------------------------------------------------
// Core module for cliker application
// --------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------
// Change the date
// --------------------------------------------------------------------------------------
function jsDate(dateObj) {
    return new Date(parseInt(dateObj.getTimeInMillis()));
};

// --------------------------------------------------------------------------------------
// Change the date
// --------------------------------------------------------------------------------------
function jsDateToStr(date) {
    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    return date.toLocaleDateString("fr_FR", options);
};

// --------------------------------------------------------------------------------------
// Change the date
// --------------------------------------------------------------------------------------
function jsToStrDate(date) {
    var year = date.getFullYear();
    var month = ((date.getMonth() + 1) < 10) ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1);
    var day = (date.getDate() < 10) ? "0" + (date.getDate()) : date.getDate();
    return year + "-" + month + "-" + day;
};

// --------------------------------------------------------------------------------------
// test the date
// --------------------------------------------------------------------------------------
function isValidDate(year, month, day) {
    if (month < 1 || month > 12) {
        return false;
    }

    var daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    if (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)) {
        daysInMonth[1] = 29;
    }

    return day <= daysInMonth[--month];
};

// --------------------------------------------------------------------------------------
// test date format
//
// @param date the date to test
// @return test if the date is valide
// --------------------------------------------------------------------------------------
function checkDate(date) {
    if (null == date) {
        throw "The date parameter can not be empty or null";
    };

    var date_format = /(20[0123]\d)[^\d]([01]\d)[^\d]([0123]\d)/;
    var match = date_format.exec(date);

    if (null == match //
        || null == match[1] //
        || null == match[2] //
        || null == match[3]) {
        throw "the date parameter '" + date + "' must valid pattern YYYY-MM-dd";
    }

    if (!isValidDate(match[1], match[2], match[3])) {
        throw "the date parameter '" + date + "' is not a valid date";
    }

    return match[1] + "-" + match[2] + "-" + match[3];
};

// --------------------------------------------------------------------------------------
// Count number of event in [from, to[ 
// the day "to" is exclude
//
// @param event the reference for the event
// @param from date of the day of the first count date is a js object
// @param to date of the last date is a js object
//
// @return the number of event count
// --------------------------------------------------------------------------------------
function countEvents(event, from, to) {
    if (null == event) {
        throw "Parameter 'event' can not be empty";
    }

    var clickerRequest = nash.service.request('${ws.clicker.url}/v1/event') //
        .connectionTimeout(10000) //
        .receiveTimeout(10000) //
        .param('filters', 'reference~' + event) //
        .param('filters', 'effect_date>=' + jsToStrDate(from)) //
        .param('filters', 'effect_date<' + jsToStrDate(to)) //
        ;

    return clickerRequest.get().asObject()['totalCounter'];
};

// --------------------------------------------------------------------------------------
// Count number of event in a day
//
// @param event the reference for the event
// @param day the day of count date is a js object
//
// @return the number of event count
// --------------------------------------------------------------------------------------
function countEventsInDay(event, day) {
    if (null == event) {
        throw "Parameter 'event' can not be empty";
    }

    var next_day = new Date();
    next_day.setDate(day.getDate() + 1);

    var clickerRequest = nash.service.request('${ws.clicker.url}/v1/event') //
        .connectionTimeout(10000) //
        .receiveTimeout(10000) //
        .param('filters', 'reference~' + event) //
        .param('filters', 'effect_date>=' + jsToStrDate(day)) //
        .param('filters', 'effect_date<' + jsToStrDate(next_day)) //
        ;

    return clickerRequest.get().asObject()['totalCounter'];
};


// ======================================================================================
// ======================================================================================

// Read input
var dateRange = _INPUT_.clicker.search.range;
if (null == dateRange || null == dateRange.from || null == dateRange.to) return;

// Read and transform dates
var date_from = jsDate(dateRange.from);
var date_to = jsDate(dateRange.to);
var display = nash.instance.load("display.xml");

// --------------------------------------------------------------------------------------
// prepare
var phases = {
    "FINISH": "Déposé",
    "CREATE": "Ouvert",
};

var label = {
    "/Formalités SCN/ENT/Création": "Création",
    "/Formalités SCN/ENT/Modification": "Modification",
    "/Formalités SCN/ENT/Cessation": "Cessation",
    "/Formalités SCN/DS": "Directive service",
    "/Formalités SCN/GQ": "Qualification Professionnelle"
}

// init count
var count = {}
var count_days = {}
for (var phase in phases) {
    count[phase] = {};
    count_days[phase] = {};
    for (var type in label) {
        count[phase][type] = 0;
        count_days[phase][type] = {};
    }
}

display.bind("clicker.result", {
    "search":
        "<h1 align='center'> Depuis le " + jsDateToStr(date_from)
        + " jusqu'au " + jsDateToStr(date_to)
        + " (non inclus)</h1>",
    "message": null,
});

// --------------------------------------------------------------------------------------
// count
try {
    debug = ""

    for (var phase in count) {
        for (var type in count[phase]) {
            count[phase][type] = countEvents(phase + type, date_from, date_to);
        }
    }


    // for (var day = date_from; day < date_to; day.setDate(day.getDate() + 1)) {
    //     for (var phase in count) {
    //         for (var type in count[phase]) {
    //             var temp = countEventsInDay(phase + type, day);
    //             count_days[phase][type][jsDateToStr(day)] = temp;
    //             debug += "" + day + " - " + phase + type + " : " + temp + "\n\n"
    //         }
    //     }
    // }

    ouput = ""

    // Add the global count
    ouput += '<div class="table-responsive"><table  class="table table-striped text-center"><thead><tr><th></th>';
    for (var col in label) {
        ouput += "<th>" + label[col] + "</th>";
    }
    ouput += "</tr></thead><tbody>";

    for (var phase in count) {
        ouput += "<tr><td>" + phases[phase] + "</td>";
        for (type in count[phase]) {
            ouput += "<td>" + count[phase][type] + "</td>";
        }
        ouput += "</tr>";
    }
    ouput += "</tbody></table></div>";

    // // Add the count for each day
    // for (var phase in count_days) {
    //     ouput += '<div class="table-responsive"><table  class="table table-striped text-center"><thead><tr>';
    //     ouput += "<th>" + phases[phase] + "</th>";
    //     var sample_data = {}
    //     for (var type in count_days[phase]) {
    //         ouput += "<th>" + label[type] + "</th>";
    //         sample_data = count_days[phase][type]
    //     }
    //     ouput += "</tr></thead><tbody>";
    //     for (var day in sample_data) {
    //         ouput += "<tr><td>" + day + "</td>";
    //         for (type in count_days[phase]) {
    //             ouput += "<td>" + count_days[phase][type][day] + "</td>";
    //         }
    //         ouput += "</tr>";
    //     }
    //     ouput += "</tbody></table></div>";
    // }

    // display.bind("clicker.result", { "debug": debug });
    display.bind("clicker.result", { "data": ouput });

} catch (error_msg) {
    display.bind("clicker.result", { "message": error_msg });
};
