// --------------------------------------------------------------------------------------
// Core module for cliker application
// --------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------
// Change the date
// --------------------------------------------------------------------------------------
function getStdDate(dateObj) {
    var dateTemp = new Date(parseInt(dateObj.getTimeInMillis()));
    var year = dateTemp.getFullYear();
    var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
    var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
    return year + "-" + month + "-" + day;
};

// --------------------------------------------------------------------------------------
// test the date
// --------------------------------------------------------------------------------------
function validDate(year, month, day) {
    if (month < 1 || month > 12) {
        return false;
    }

    var daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    if (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)) {
        daysInMonth[1] = 29;
    }

    return day <= daysInMonth[--month];
};

// --------------------------------------------------------------------------------------
// test date format
//
// @param date the date to test
// @return test if the date is valide
// --------------------------------------------------------------------------------------
function checkDate(the_date) {
    if (null == date_to_test) {
        throw "The date parameter can not be empty or null";
    };

    var date_format = /(20[0123]\d)[^\d]([01]\d)[^\d]([0123]\d)/;
    var match = date_format.exec(the_date);

    if (null == match //
        || null == match[1] //
        || null == match[2] //
        || null == match[3]) {
        throw "the date parameter '" + the_date + "' must valid pattern YYYY-MM-dd";
    }

    if (!validDate(match[1], match[2], match[3])) {
        throw "the date parameter '" + the_date + "' is not a valid date";
    }

    return match[1] + "-" + match[2] + "-" + match[3];
};

// --------------------------------------------------------------------------------------
// Count number of event in [from, to[ 
// the day "to" is exclude
//
// @param event the reference for the event
// @param from date of the day of the first count "YYY-MM-JJ"
// @param event date of the day+1 of the last count count "YYY-MM-JJ"
//
// @return the number of event count
// --------------------------------------------------------------------------------------
function countEvents(event, from, to) {
    if (null == event) {
        throw "Parameter 'event' can not be empty";
    }

    var clickerRequest = nash.service.request('${ws.clicker.url}/v1/event') //
        .connectionTimeout(10000) //
        .receiveTimeout(10000) //
        .param('filters', 'reference~' + event) //
        .param('filters', 'effect_date>=' + checkDate(from)) //
        .param('filters', 'effect_date<' + checkDate(to)) //
        ;

    return clickerRequest.get().asObject()['totalCounter'];
};
