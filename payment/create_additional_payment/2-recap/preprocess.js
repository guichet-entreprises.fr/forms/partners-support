var dataPayment = _input.complementaryPaymentInfos.paymentInfos;
var dataPaymentAC = _input.complementaryPaymentInfos.paymentInfos.paymentInfosAC;

var idTrackerUser = dataPayment.idTrackerUser;
var refDossierInitial = dataPayment.refDossierInitial;
var titleDossierInitial = dataPayment.titleDossierInitial;
var dateDossierInitial = dataPayment.dateDossierInitial;
var purpose = dataPayment.purpose;

var acDestinationId = dataPaymentAC.acDestinationId;
var amountToPayAC = dataPaymentAC.amountToPayAC;
var data = nash.instance.load('data.xml');

//Récupérer les informations du user
var userResponse = nash.service.request('${account.ge.baseUrl.read}/private/users/{idTrackerUser}', idTrackerUser) //
				.dataType('application/json') //
				.accept('json') //
				.get();
var user = userResponse.asObject();
_log.info("user is {}", user);
	
	
//Récupérer les informations de l'autorité
var response = nash.service.request('${directory.baseUrl}/v1/authority/{acDestinationId}', acDestinationId) //
									.connectionTimeout(10000) //
									.receiveTimeout(10000) //
									.accept('json') //
									.get();
_log.info("response is {}", response);
receiverInfo = response.asObject();
_log.info("receiverInfo is {}", receiverInfo);
	
var acDestinationLabel = receiverInfo.label;
_log.info("label is {}", acDestinationLabel);	

data.bind('complementaryPaymentInfos.paymentInfos', {
	'idTrackerUser' : user.trackerId,
	'emailUser' : user.email,
	'refDossierInitial' : refDossierInitial,
	'titleDossierInitial' : titleDossierInitial,
	'dateDossierInitial' : dateDossierInitial,
	'purpose' : purpose
});

data.bind('complementaryPaymentInfos.paymentInfos.paymentInfosAC', {
	'acDestinationId' : acDestinationId,
	'acDestinationLabel' : acDestinationLabel,
	'amountToPayAC' : amountToPayAC
});