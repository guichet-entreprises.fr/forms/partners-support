# Description of  complementary payment :

The goal is to creat a payment on the user's dashboard.

# Authorities concerned :
* *GE*
	* The support team
	* The admin team

* *GE/Helpline*
	* The support team

## First step : Input form 

The form allows the user to enter the information of the payment.

## Second step : Confirmation form

This step will check the existance of the user and the Authority and display it.

## Third step : Replay request

This step will send the payment to the user's dashboard.