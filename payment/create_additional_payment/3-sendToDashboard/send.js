log.info('===> dashboard preprocess');
//data input
var dataPayment = _input.complementaryPaymentInfos.paymentInfos;
var dataPaymentAC = _input.complementaryPaymentInfos.paymentInfos.paymentInfosAC;

var idTracker = dataPayment.idTrackerUser;
var refDossierInitial = dataPayment.refDossierInitial;
var titleDossierInitial = dataPayment.titleDossierInitial;
var dateDossierInitial = dataPayment.dateDossierInitial;
var purpose = dataPayment.purpose;

var acDestinationId = dataPaymentAC.acDestinationId;
var acDestinationLabel = dataPaymentAC.acDestinationLabel;
var amountToPayAC = dataPaymentAC.amountToPayAC;

//record
var recordUid = nash.record.description().recordUid;
var recordTitle = nash.record.description().title;
var nashBaseUrl = _CONFIG_ ? _CONFIG_.get('nash.baseUrl') : '';

var display = nash.instance.load("display.xml");
display.bind("request", {
    "input" : {
        "idTracker" : idTracker
    },
    'output' : {
        'url' : nashBaseUrl
    }
});

    log.info('nash.ws.private.url2 {}', _CONFIG_.get('nash.ws.private.url'));
    log.info('_config_ {}', _CONFIG_.asMap());
    log.info('idTracker {}', idTracker);
//var recordDashboard = nash.instance.from('${nash.baseUrl}').createRecord("local:Paiement").meta([ref:Paiement complémentaire
var recordDashboard = nash.instance.from('${nash.ws.private.url}').createRecord("local:Paiement")
				.setAuthor(idTracker)
	            .role([
	                {
	                    "entity" : idTracker,
	                    "role" : '*'
	                }
	            ]);
recordDashboard.description.setAuthor(idTracker);
recordDashboard.description.setTitle(recordTitle);
recordDashboard.description.setFormUid(recordUid);


// Binding summary.xml

var summary = recordDashboard.load('1-data/data.xml');
summary.bind("page01.complementaryPaymentInfos", { 
	"active" : "Activée"
});
summary.bind("page01.complementaryPaymentInfos.paymentInfos", { 
	"refDossierInitial" : refDossierInitial,
	"titleDossierInitial" : titleDossierInitial,
	"dateDossierInitial" : dateDossierInitial,
	"purpose" : purpose
});
summary.bind("page01.complementaryPaymentInfos.paymentInfos.paymentInfosAC", { 
	"acDestinationId" : acDestinationId,
	"acDestinationLabel" : acDestinationLabel,
	"amountToPayAC" : amountToPayAC
});

try {
    var recordDashboardUid = recordDashboard.save();
	log.info('recordDashboardUid {}', recordDashboardUid);
    nash.tracker.link(recordDashboardUid);
    log.info('forms record identifier {}', recordDashboardUid);
    display.bind("request", {
        "output" : {
             'status' : "200",
             'confirm' : "OK",
             'formsId' : recordDashboardUid,
             'response' : 'Le dossier a été crée avec succès sur le dashboard de l\'utilisateur.'
        }
    });
} catch (error) {
    log.error("forms error : " + error);
    display.bind("request", {
        "output" : {
             'status' : "500",
             'confirm' : 'KO',
             'response' : "Une erreur technique est survenue lors de l\'envoi sur le dashboard. " + error 
        }
    });
}