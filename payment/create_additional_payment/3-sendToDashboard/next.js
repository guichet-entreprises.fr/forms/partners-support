_log.info("===> postprocess sendDashboardRecord");

var message = "Un dossier avec la référence " + _input.request.output.formsId + " a été déposé sur le dashboard de l'utilisateur : " + _input.request.input.idTracker;


nash.instance //
    .load('output.xml') //
    .bind('result', {
        'tracker' : {
        	'message' : message
        },
        'dashboard' : {
        	'dashboardId' : _input.request.output.formsId
        }
    });
