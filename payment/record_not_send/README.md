Description of monitoring Guichet Entreprises activity (payments) :
===================================================================

The goal is to monitor Guichet Entreprises activity (payments).

Teams concerned :
=================

-   The admin team

First step : Monitoring Guichet Entreprises activity using criteria
-------------------------------------------------------------------

Searching for payments in success based on \* period \* and analyze record
status associated (unknown, finalized or not)

This step will display the events resulting from the criteria.

Here are the information displayed : \* Events list \* Total counter and so on.

Events list can be downloaded or copy and paste.
