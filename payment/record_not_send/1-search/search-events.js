// --------------------------------------------------------------------------------------
// Convert string to bytes
// --------------------------------------------------------------------------------------

function convertStringToByte(str){
	var bytes = []; // char codes
	for (var i = 0; i < str.length; ++i) {
		var code = str.charCodeAt(i);
		bytes = bytes.concat([code & 0xff, code / 256 >>> 0]);
	}	
	return bytes;
};

// format date time string UTC to local date time

function convertDateTime(dateUTC, flgISO) {
	var result = '';
	var dateTemp = new Date(parseInt(dateUTC));
	var year = dateTemp.getFullYear();
	var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
	var day = (dateTemp.getDate() < 10) ? "0" + dateTemp.getDate() : dateTemp.getDate();
	var hour = (dateTemp.getHours() < 10) ? "0" + dateTemp.getHours() : dateTemp.getHours() ;
	var minutes = (dateTemp.getMinutes() < 10) ? "0" + dateTemp.getMinutes() : dateTemp.getMinutes() ;
	var seconds = (dateTemp.getSeconds() < 10) ? "0" + dateTemp.getSeconds() : dateTemp.getSeconds() ;
	if (flgISO) {
		result = year + "-" + month + "-" + day ;
	} else {
		result = day + "/" + month + "/" + year ;
	}
	result +=  " " + hour + ":" + minutes + ":" + seconds ;
	return result;
};

// test last step user to done
function testLastStepUserDone(recordUid) {
	var result = '';
	var lastUserSteps = '';
	if (typeof recordUid.details.steps.elements === 'object' && recordUid.details.steps.elements !== null) {
		recordUid.details.steps.elements.forEach (function (elm) {
			if (typeof elm !== 'undefined' && (!elm.user || elm.user === 'user')) {
				lastUserSteps = elm.status;
			}
			return;
		});
		if (lastUserSteps === 'done') {
			result = 'finalized';
		} else {
			result = lastUserSteps;
		}
	} else {
		result = 'invalid Uid';
	}
	return result;
}

// --------------------------------------------------------------------------------------
// Find status of recordUid
// --------------------------------------------------------------------------------------
function searchRecordUid(recordUid, xmlDisplay, debugXmlPath) {
	try {
		var nashRequest = nash.service.request('${nash.ws.private.url}/v1/Record/code/{uid}', recordUid) //
												.connectionTimeout(60000) //
												.receiveTimeout(60000) //
												.param('startIndex', '0') //
												.param('maxResults', '1'); //
		var searchResult = nashRequest.get();
		if (typeof searchResult === 'undefined' || searchResult === null) {
			if (typeof searchResult !== 'undefined' && searchResult.getStatus() !== 200) {
				xmlDisplay.bind(debugXmlPath, {"status": "yes"});
				xmlDisplay.bind(debugXmlPath + ".infos", {"step": "searchRecordUid",
														  "error": "no",
														  "code": "SEARCH-FORM-CODE", 
														  "response": "" + error });
			}
			return null;
		}
		return searchResult.asObject();
	} catch (error) {
		xmlDisplay.bind(debugXmlPath, {"status": "yes"});
		xmlDisplay.bind(debugXmlPath + ".infos", {"step": "searchRecordUid",
												  "error": "yes",
												  "code": "SEARCH-FORM-ERROR", 
												  "response": "" + error });
		return null;
	};
}

// --------------------------------------------------------------------------------------
// Find the carts
// --------------------------------------------------------------------------------------
function findCartsProcess(startIndex, maxResults, dateSearch, xmlDisplay, debugXmlPath) {
	try {	
		var fullUrl = _CONFIG_.get('payment.proxy.api.private.baseUrl') + '/api/v1/cart';
		var proxyRequest = nash.service.request(fullUrl) //
			.connectionTimeout(60000) //
			.receiveTimeout(60000) //
			.param('startIndex', startIndex.toString()) //
			.param('maxResults', maxResults.toString()) //
			.param('orders', 'created:desc') //
			.param('orders', 'reference:desc') //
			.param('filters', 'status~SUCCESS') ; //
		if (typeof dateSearch === 'object' && dateSearch !== null)	{
			if (dateSearch.from !== null) {
				var dateTemp = Date.parse(dateSearch.from.toInstant().toString());
				proxyRequest.param('filters', 'created' + '>=' + convertDateTime(dateTemp, true).substring(0,10));
			}
			if (dateSearch.to !== null) {
				var dateTemp = Date.parse(dateSearch.to.toInstant().toString());
				proxyRequest.param('filters', 'updated' + '<=' + convertDateTime(dateTemp, true).substring(0,10));
			}
		}		
		var searchResult = proxyRequest.get(); //
		if (typeof searchResult === 'undefined' || searchResult === null) {
			if (typeof searchResult !== 'undefined' && searchResult.getStatus() !== 200) {
				xmlDisplay.bind(debugXmlPath, {"status": "yes"});
				xmlDisplay.bind(debugXmlPath + ".infos", {"step": "searchRecordUid",
														  "error": "no",
														  "code": "PAYMENT-EVENTS-CODE", 
														  "response": "" + error });
			}
			return null;
		}
		return searchResult.asObject();
	} catch (error) {
		xmlDisplay.bind(debugXmlPath, {"status": "yes"});
		xmlDisplay.bind(debugXmlPath + ".infos", {"step": "findCartsProcess",
												  "error": "yes",
												  "code": "PAYMENT-EVENTS-ERROR", 
												  "response": "" + error });
		return null;
	};
};

// --------------------------------------------------------------------------------------
// list payments
// --------------------------------------------------------------------------------------
function listEventsProcess(dateSearch, xmlDisplay, debugXmlPath) {
	try {
		var maxResults = 50;
		var totalEvents = 0;
		var startIndex = 0;
		var totalCounter = 0;
		var totalResults = 0;
		var dateFrom = 0;
		var DateTo = Date.now();
		var listEvents = "date;dossier;statut;civilite;nom;prenom;email\n" ;
		xmlDisplay.bind("record.results.events", { "totalCounter": "", "totalEvents": "", "listEvents": "" });
		if (typeof dateSearch === 'object' && dateSearch !== null)	{
			if (dateSearch.from !== null) {
				dateFrom = Date.parse(dateSearch.from.toInstant().toString());
			}
			if (dateSearch.to !== null) {
				dateTo = Date.parse(dateSearch.to.toInstant().toString());
			}
		}
		do {
			var Payments = findCartsProcess(startIndex, maxResults, dateSearch, xmlDisplay, debugXmlPath);
			if (Payments === null) {
				break; 
			}
			if(startIndex === 0) {
				totalCounter = Payments.totalResults;
				totalResults = Payments.totalResults;
			}
			Payments.content.forEach(function (payment) {
				var dateTemp = payment.created;
				if (dateTemp >= dateFrom && dateTemp <= dateTo) {
					if (payment.reference !== null && payment.reference !== '') {
						var dateEvent = convertDateTime(dateTemp, false);
						var recordUid = searchRecordUid(payment.recordUid, xmlDisplay, debugXmlPath);
						if (recordUid !== null) {
							var statusRecord = testLastStepUserDone(recordUid);
						} else {
							var statusRecord = 'Unknown';
						}
						listEvents += dateEvent + ";" + payment.recordUid + ";" + statusRecord + ";"
									+ payment.userCivility + ";" + payment.userLastName + ";" + payment.userFirstName + ";"
									+ payment.email + "\n";
						totalEvents++;
					}
				}
			});
			startIndex += maxResults;
			totalResults -= maxResults;
		} while (totalResults > 0);
		xmlDisplay.bind("record.results.events", { "totalCounter": totalCounter.toString(),
												   "totalEvents": totalEvents.toString(),
												   "listEvents": listEvents });
	} catch (error) {
		xmlDisplay.bind(debugXmlPath, {"status": "yes"});
		xmlDisplay.bind(debugXmlPath + ".infos", {"step": "listEventsProcess", 
												  "error": "yes",
												  "code": "PAYMENT-LIST-ERROR", 
												  "response": "" + error});
		listEvents = '';
	};
	return listEvents;
};

// --------------------------------------------------------------------------------------


// try to find the id to search
try {
	var xmlDisplay = nash.instance.load("display-search.xml");
	var debugXmlPath = "record.debug";
	xmlDisplay.bind(debugXmlPath, {"version": "v0.7 du 05/08/2020"});
	xmlDisplay.bind(debugXmlPath, {"status": "no"});
	var flgSearch = _INPUT_.record.search.searchButton.value === 'yes' ? true : false;
	if (flgSearch) {
		var dateSearch = _INPUT_.record.search.date;
		var listEvents = [];
		while (true) {
			listEvents = listEventsProcess(dateSearch, xmlDisplay, debugXmlPath);
			if (!listEvents.length) {
				break;
			}
			var flgFile = _INPUT_.record.file.fileGenerated;
			if (flgFile) {
				var fileName = '/2-Results/Events-report.csv';
				nash.record.saveFile(fileName, convertStringToByte(listEvents));
				xmlDisplay.bind("record.file", { "fileName": fileName });
				if(nash.record.meta() == null){
					nash.record.meta([{'name':'document', 'value': fileName}]);
				}
				xmlDisplay.bind(debugXmlPath + ".infos", {"step": "MainProcess", 
														  "error": "no",
														  "code": "MAIN-PROC-SAVE",
														  "response": "Save events finalized"});
			}
			break;
		}
	}
} catch (error) {
	xmlDisplay.bind(debugXmlPath, {"status": "yes"});
	xmlDisplay.bind(debugXmlPath + ".infos", {"step": "MainProcess", 
											  "error": "yes",
											  "code": "MAIN-PROC-ERROR", 
											  "response": "" + error});
};
return;
