log.info(_INPUT_);

var authority = _INPUT_.record.search.authority;
var errorCode = _INPUT_.record.search.errorCode;

// Test if a search is needed
if (null == authority || null == errorCode) {
	return;
};

// --------------------------------------------------------------------------------------
// Find the carts
// --------------------------------------------------------------------------------------
function findCart(authority, errorCode) {
	fullUrl = _CONFIG_.get('payment.proxy.api.private.baseUrl') + '/api/v1/cart?maxResults=20&filters=criticity:CRITICAL&filters=ediCode:' + authority.id + '&filters=errorCode:' + errorCode.id;

	// perform the call
	var searchResult = null;
	try {
		searchResult = nash.service.request(fullUrl) //
			.dataType('application/json') //
			.accept('json')
			.get();

	} catch (error) {
		log.info('=> Error occured while calling account : ' + error);
		return null;
	};
	
	if (searchResult == null || searchResult.getStatus() != 200) {
		return null;
	};

	return searchResult.asObject();
};
// --------------------------------------------------------------------------------------

// Load the screen for the user
var display = nash.instance.load("display-search.xml");

var cartFound = findCart(authority, errorCode);

if (null == cartFound) { return; };

display.bind("record.results.carts", { "nbCarts": cartFound.totalResults });
display.bind("record.results.carts", { "cart": cartFound.content });
