
log.info('_INPUT_ : {}', _INPUT_);
var carts = _INPUT_.record.results.carts.cart;
//var resetAll = _INPUT_.record.results.carts.resetAll;
//log.info('Replay all records : {}', resetAll);

// Test if a search is needed
if (null == carts || null == carts[0].recordUid) {
	return;
};


var cartsSuccess = [];
var cartsError = [];
var cartsNoReplay = [];

for (var idx in carts) {
	var cart = carts[idx];
	cart['notification'] = {
		'exchange' : 'Aucune action effectuée',
		'tracker' : 'Aucune action effectuée'
	};
	
	log.info('Replay the record {} : {}', cart.recordUid, cart.reset);
	//if (!cart.reset && !resetAll) {
	if (!cart.reset) {
		log.info('No replay for the record {}', cart.recordUid);
		cartsNoReplay.push(cart);
		continue;
	}
	log.info('Replay step for record {}', cart.recordUid);
	
	//--> Delete cart and proxy session
	var deleteCart = null;
	try {
		deleteCart = nash.service.request('${payment.proxy.api.private.baseUrl}/api/v1/cart/{token}', cart.token) //
			.dataType('application/json') //
			.delete();

	} catch (error) {
		log.info('=> Error occured while calling proxy session : ' + error);
	}
	if (deleteCart == null || deleteCart.getStatus() != 200) {
		cartsError.push(cart);
		continue;
	}
	//<--
	
	//-->Reset current step for record
	var resetStep = null;
	try {
		resetStep = nash.service.request('${forms.private.url}/v1/Record/code/{code}/step/reset', cart.recordUid) //
			.dataType('application/json') //
			.post(null);

	} catch (error) {
		log.info('=> Error occured while calling account : ' + error);
	}
	//<--

	// test the 200 code
	if (resetStep == null || resetStep.getStatus() != 200) {
		cartsError.push(cart);
		continue;
	}
	
	//-->Send email
	var sendEmail = null;
	try {

		var content = 
		"<html>" + //
		"<body style='font-family: Times New Roman, serif;'>" + //
		"<div style='text-align:center;'>" + //
		"<img style='display: block; margin: 0 auto;' src='https://account.guichet-entreprises.fr/assets/images/guichet-entreprises/logo/minefi.png'>" + //
		"<img style='display: block; margin: 0 auto;' src='https://account.guichet-entreprises.fr/assets/images/guichet-entreprises/logo/long-base.png'>" + //
		"</div>" + //
		"<div class='content' style='color:rgb(85,85,85); margin-left: auto; margin-right: auto; width: 50%;'>" + //
		"<div class='dear' style='padding-left: 40px; padding-bottom: 20px; text-transform: capitalize;'>Bonjour " + cart.userCivility + " " + cart.userLastName + " " + cart.userFirstName + ",</div>" + //
		"<div class='message' style='padding-left: 40px; line-height: 2em;'>" + //
		"Vous avez créé le dossier " + cart.recordUid + " sur Guichet-entreprises.fr et nous vous en remercions.<br/>" + //
		"Nous vous informons qu’il est de nouveau disponible depuis votre tableau de bord. Vous pouvez dès à présent procéder au règlement des frais qui lui sont rattachés.<br/>" + //
		"Nous vous remercions de votre confiance," + //
		"<br/>" + //
		"L'équipe du service Guichet Entreprises" + //
		"</div>" + //
		"</div>" + //
		"</body>" + //
		"</html>";
		
		//TODO ${exchange.baseUrl}
		sendEmail = nash.service.request('${exchange.baseUrl}/email/sendWithoutAttachement') //
		.connectionTimeout(2000) //
		.receiveTimeout(5000) //
		.dataType("form") //
		.param("sender", _CONFIG_.get('exchange.email.sender')) //
		.param("recipient", cart.email) //
		.param("object", 'Règlement de vos frais de dossier') //
		.param("content", content) //
		.post(null);
		cart['notification']['exchange'] = 'Courriel envoyé avec succès';
	} catch (error) {
		log.info('=> Error occured while calling exchange : ' + error);
		cart['notification']['exchange'] = 'KO';
	}
	//<--
	
	try {
		var response = nash.service.request('${tracker.baseUrl}/v1/uid/{ref}/msg', cart.recordUid) //
	        .connectionTimeout(10000) //
	        .receiveTimeout(10000) //
	        .dataType('application/json')//
	        .accept('json') //
			.param("content", "Votre dossier est de nouveau disponible depuis votre tableau de bord.") //
			.param("author", 'support') //
			.post(null);
		cart['notification']['tracker'] = 'Message envoyé avec succès';
	} catch (error) {
		log.error("Unable to post Tracker message for reference " + cart.recordUid);
		cart['notification']['tracker'] = 'KO';
	}
	
	cartsSuccess.push(cart);
}

// Load the screen for the user
var display = nash.instance.load("display-replay.xml");

display.bind("cart.results.success", { "total": cartsSuccess.length });
display.bind("cart.results.success", { "cart": cartsSuccess });

display.bind("cart.results.error", { "total": cartsError.length });
display.bind("cart.results.error", { "cart": cartsError });

display.bind("cart.results.noreplay", { "total": cartsNoReplay.length });
display.bind("cart.results.noreplay", { "cart": cartsNoReplay });
