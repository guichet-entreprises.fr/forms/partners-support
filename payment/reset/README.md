# Description of replay payment :

The goal is to replay payment step.

# Authorities concerned :
* *GE*
	* The admin team

## First step : Searching for an authority and suspended records

Search for a Guichet-Partenaires authority by its label and a payment code.
Next, this step will search records based on these parameters.
You can also unlock all records or select any one of these.

## Second step : Unlock records

This step will unlock records selected previously.
Next, it will display the result splitted on three groups :
1. Records unlocked with success
    - An email will be sent to the record's author allows him to replay payment step.
    - A Tracker message will be also sent to the same author telling him that the support team has unlocked its record
2. Records failed
3. Records ignored