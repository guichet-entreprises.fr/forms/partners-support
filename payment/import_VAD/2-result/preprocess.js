//--------------- prepare info  before import the file -------------------------
var targets = _INPUT_.parameters.select.targets;
log.info("targets checked are {}", targets);

var attachment = _INPUT_.parameters.files.attachment[0];

var plateforms = {
    'forms' : '${forms.baseUrl}/referentiel/supraCfeEdi/import',
    'nash' : '${payment.proxy.api.private.baseUrl}/v1/comptesvad/import'
};

var vad = [];
var target = null;
var csvContent = attachment.getContent();
for (var idx in targets) {
	target = targets[idx].getId();
    try {
        var response = nash.service.request(plateforms[target]) //
                .connectionTimeout(10000) //
                .receiveTimeout(20000) //
                .dataType("application/octet-stream") //
                .post(csvContent);

        var details = response.asObject();
        log.info("Details response : {}", details);
        
        var functionalStatus; 
        
        	if((details.erreur==='' || details.erreur===null) && (parseInt(details.nb_notUpdated_cfe) == 0 ) ){
        		functionalStatus ='OK';
        	
        	}else if (parseInt(details.nb_notUpdated_cfe) > 0 && !(details.updated_cfe==='' || details.updated_cfe===null) ){
        		functionalStatus ="KO fonctionnel partiel";
        	}else{
        		functionalStatus ="KO fonctionnel";
        	}
        	
        	
        vad.push({
            target : targets[idx],
            status : functionalStatus,
            details : {
                updatedCfe : details.updated_cfe,
                notUpdatedCfe : details.notUpdated_cfe,
                nbNotUpdatedCfe : details.nb_notUpdated_cfe,
                erreur : details.erreur
            }
        });
    } catch (erreur) {
        log.error(erreur);
        vad.push({
            target : targets[idx],
            status : 'KO Technique',
            message : erreur
        });
    }
}

//-->Bind all result calls
nash.instance //
	.load("data.xml") //
	.bind('output', {
	    vad : vad
	});
