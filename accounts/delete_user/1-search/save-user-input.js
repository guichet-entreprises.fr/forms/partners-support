// Load the screen for the user
var display = nash.instance.load("display-search.xml");

var changeSearchData = (_INPUT_.account.search.idOrEmail !== _INPUT_.account.results.searchId);

// Save the value 
display.bind("account.results", { "searchId": _INPUT_.account.search.idOrEmail, });

// reset the button
display.bind("account.search", { "searchButton": "" });
display.bind("account.results.confirm", { "deleteButton": "" });

// Check the confirmation
var delUserGe = (Value('id').of(_INPUT_.account.results.ge.output.found).eq('geok'))
	&& (Value('id').of(_INPUT_.account.results.ge.result.remove).eq('geremoveok'));

	
log.info("_INPUT_.account.search.idOrEmail {}", _INPUT_.account.search.idOrEmail);
log.info("_INPUT_.account.results.searchId {}", _INPUT_.account.results.searchId);
log.info("changeSearchData {}", changeSearchData);
log.info("!delUserGe {}", !delUserGe);

// unconfirm delete if needed
if ((changeSearchData) || (!delUserGe)) {
	display.bind("account.results.confirm", { "yes": "" });
};
