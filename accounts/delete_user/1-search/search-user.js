// try to find the id to search
var searchId = _INPUT_.account.results.searchId;

// Test if a search is needed
if ((searchId == null) || (searchId === "")) {
	return;
};

// Load the screen for the user
var display = nash.instance.load("display-search.xml");

// config values
var targetUrlAccounts = {
	"ge": _CONFIG_.get('account.ge.baseUrl.read')
};

// unconfirm
display.bind("account.results.confirm", { "yes": "" });

for (var plateform in targetUrlAccounts) {
	fullUrl = targetUrlAccounts[plateform] + '/private/users/' + searchId;
	resultsXmlPath = "account.results." + plateform

	//save call data
	display.bind(resultsXmlPath + ".input", {
		"searchId": searchId,
		"url": fullUrl,
	});

	// perform the call
	searchResult = null;
	try {
		searchResult = nash.service.request(fullUrl) //
			.dataType('application/json') //
			.accept('json')
			.get();

	} catch (error) {
		log.info('=> Error occured while calling account : ' + error);
		display.bind(resultsXmlPath + ".output", {
			"status": "ERROR",
			"response": "" + error,
		});
		continue;
	};

	responseObj = searchResult.asObject();

	display.bind(resultsXmlPath + ".output", {
		"status": searchResult.getStatus(),
		"response": "" + responseObj,
	});

	// test the 200 code
	if (searchResult == null || searchResult.getStatus() != 200) {
		continue;
	};

	// test the 1005 code
	if ((responseObj == null) || (responseObj['errorCode'] == "1005")) {
		display.bind(resultsXmlPath + ".output", { "found": "" });
		continue;
	};

	// OK there is a result
	display.bind(resultsXmlPath + ".output", {
		"found": plateform + "ok",
	});

	display.bind(resultsXmlPath + ".result", {
		"firstName": responseObj['firstName'],
		"lastName": responseObj['lastName'],
		"civility": responseObj['civility'],
		"language": responseObj['language'],
		"email": responseObj['email'],
		"password": responseObj['password'],
		"phone": responseObj['phone'],
		"trackerId": responseObj['trackerId'],
		"fcId": responseObj['fcId'],
		"phoneCheck": responseObj['phoneCheck'],
		"connectionDate": responseObj['connectionDate'],
		"mailType": responseObj['mailType'],
		"referrerFirstName": responseObj['referrerFirstName'],
		"referrerLastName": responseObj['referrerLastName'],
		"oldEmail": responseObj['oldEmail'],
		"remove": plateform + "removenok",
	});
};