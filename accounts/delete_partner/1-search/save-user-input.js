// Load the screen for the user
var display = nash.instance.load("display-search.xml");

var changeSearchData = (_INPUT_.account.search.idOrEmail !== _INPUT_.account.results.searchId);

// Save the value 
display.bind("account.results", { "searchId": _INPUT_.account.search.idOrEmail, });

// reset the button
display.bind("account.search", { "searchButton": "" });
display.bind("account.results.confirm", { "deleteButton": "" });

// Check the confirmation
var delUserGp = (Value('id').of(_INPUT_.account.results.gp.output.found).eq('gpok'))
	&& (Value('id').of(_INPUT_.account.results.gp.result.remove).eq('gpremoveok'));

	
log.info("_INPUT_.account.search.idOrEmail {}", _INPUT_.account.search.idOrEmail);
log.info("_INPUT_.account.results.searchId {}", _INPUT_.account.results.searchId);
log.info("changeSearchData {}", changeSearchData);
log.info("!delUserGp {}", !delUserGp);

// unconfirm delete if needed
if ((changeSearchData) || (!delUserGp)) {
	display.bind("account.results.confirm", { "yes": "" });
};
