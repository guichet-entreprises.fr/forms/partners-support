var recordUid = nash.record.description().getRecordUid();
log.info("Redirection to the first page for the record {}", recordUid);
return { url: "/record/" + recordUid + "/0/page/0" };