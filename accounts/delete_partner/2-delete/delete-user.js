// Load the screen for the user
var display = nash.instance.load("display-deleted.xml");

// URL liste
var targetUrlAccounts = {
	"gp": _CONFIG_.get('account.baseUrl.read')
};


for (var plateform in targetUrlAccounts) {
	var data = _INPUT_.account.results[plateform];

	var confirmDelete = Value('id').of(data.result.remove).eq(plateform + "removeok");

	if (!confirmDelete) {
		display.bind("results." + plateform, {
			"message": "Pas de demande de suppression."
		});
		continue;
	};

	var email = data.result.email;
	var fullUrl = targetUrlAccounts[plateform] + '/private/users/' + email;

	if ((email == null) || (email === "")) {
		display.bind("results." + plateform, {
			"message": "Pas de demande de suppression. L'email n'est pas précisé."
		});
		continue;
	};

	resultsXmlPath = "results." + plateform

	//save call data
	display.bind(resultsXmlPath + ".input", {
		"email": email,
		"url": fullUrl,
	});

	try {
		accountResponse = nash.service.request(fullUrl)
			.dataType('application/json') //
			.accept('json')
			.delete();
	} catch (error) {
		log.info('=> Error occured while calling account : ' + error);

		display.bind("results." + plateform, {
			"message": "Erreur au moment de l'appel à account."
		});

		display.bind(resultsXmlPath + ".output", {
			"status": "ERROR",
			"response": "" + error,
		});
		continue;
	};

	responseObj = accountResponse.asObject();

	display.bind(resultsXmlPath + ".output", {
		"status": accountResponse.getStatus(),
		"response": "" + responseObj,
	});
	// test the 200 code
	if (accountResponse == null || accountResponse.getStatus() != 200) {
		display.bind("results." + plateform, {
			"message": "Erreur HTTP " + accountResponse.getStatus()
		});
		continue;
	};

	// test the 1005 code
	if ((responseObj == null) || (responseObj['errorCode'] == "1005")) {
		display.bind("results." + plateform, {
			"message": "Email non trouvé."
		});
		continue;
	};
	display.bind("results." + plateform, {
		"message": "L'utilisateur " + email + " a été supprimé"
	});
};
