// Load the screen for the user
var display = nash.instance.load("display-result.xml");

// All value
var targetUrl = _INPUT_.import.target.url
var dryRun = _INPUT_.import.dryRun;
var csvFilePath = '/1-input/uploaded/import.file-1-' + _INPUT_.import.file[0].getLabel();
var content = nash.util.resourceFromPath(csvFilePath).getContent();
 
display.bind("result.input", {
	"url": targetUrl,
	"dryRun": dryRun,
	"filename": csvFilePath,
	"content": content,
});

// Url Vide
if ((targetUrl == null) || (targetUrl === "")) {
	display.bind("result.output", {
		"status": "Pas d'appel",
		"response": "L'url d'appel est vide. Aussi, manque-t-il cette url.",
	});
	return;
}

var response = null
try {
	var response = nash.service.request(targetUrl)
							.connectionTimeout(2000).receiveTimeout(5000)
							.dataType("application/octet-stream")
							.param("dryRun", dryRun)
							.post(content);
} catch (error) {
	_log.info("Error calling directory {}", error);
	display.bind("result.output", {
		            "status": "Error calling directory",
					"response": "" + error
				});
	return;
}

display.bind("result.output", {"status": response.status});	

if (response.status == 200) {
	display.bind("result.output", {"response": "Votre liste d'utilisateurs a été importée avec succès."});
} else {
 	display.bind("result.output", {"response": "Une erreur technique est survenue."});
};
