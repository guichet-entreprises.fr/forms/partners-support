
//--------------- prepare info  before import the file -------------------------
_log.info(" _INPUT_ is {}", _INPUT_);

var content = $verifUser.dataGroup.list.split(/\r?\n/);
_log.info("list des users : {}", content);

var i;
var result = [];
var response = "";
for(i=0 ; i < content.length; i++){
	try{
		_log.info("ListUser appel Account : {}", content[i]);
		if (content[i] != null) {
			var user = {
			'civility':  "M",
			'lastName' : "audit",
			'firstName' : "audit",
			'email' :  content[i],
			'language' : 'fr_FR', //
			'mailType' : '0', //
			'phone': "0612345678"
		};
		
		log.info("user send to the Account Web Service is " + JSON.stringify(user));
		
		var response = nash.service.request('${account.ge.baseUrl.read}/private/users') //
		.dataType('application/json') //
	    .accept('json') //
		.post(JSON.stringify(user));
		}
		if (response != null and response.getStatus() == 200) {
			res = response.asObject();
			if(res.errorCode == 1005){
				result.push(content[i]);
			}else{
				result.push(res.trackerId + ";" + res.email);
			}
		}else{
			result.push(null);
		}
	}catch(error) {
		_log.info("erreur lors de la formalité d'import des user Account : {}", error);
		//Response
		return spec.create({ 
			id : 'ConfirmSend',
			label : 'Résultat de la demande',
			groups : [ 
				spec.createGroup({
					id : 'group',
					label : 'Statut',
					warnings : [ 
						{ id : 'warningConfiguration', label : "Une erreur technique est survenue. " + error + " \ncontent :" + content}
					]
				}) 
			]
		});
	}
}
_log.info("ListUser resultat : {}", result);
return spec.create({ id : 'userData', groups : [
	spec.createGroup({ id : 'accountParameters', label : 'Paramètres des comptes', data : [
		spec.createData({ id : 'result', label : 'résultat', type: 'Text', value: result.join("\n")})
	]})
]});
