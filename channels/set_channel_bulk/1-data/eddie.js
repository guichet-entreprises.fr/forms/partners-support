var inf = _input.bloc.borne.inf;
var sup = _input.bloc.borne.sup;
var rootAuthority = _input.context.authority;
var uids = rootAuthority.children.uid;
for (var idx = inf; idx < sup; idx++) {
	if (idx < uids.length) {
	    var response =  nash.service.request('${directory.baseUrl}/v2/ref/list/authority?maxResults=1&filters=entityId:' + uids[idx]) //
	    .accept('json') //
	    .get();
	    if (200 == response.getStatus()) {
	        var child = response.asObject();
	        child['content'].forEach(function(authority) {
                var details = authority.details; 
                details['transferChannels']['ftp'] = {
                    "state": "enabled",
                    "token": rootAuthority.ftp.token,
                    "pathFolder": rootAuthority.ftp.pathFolder,
                    "ftpMode": "delegatedChannel",
                    "comment": "",
                    "delegationAuthority": rootAuthority.uid
                };
                authority.details = details;
                log.info('Details : {}', details);
	            var response = nash.service.request('${directory.baseUrl}/v1/authority/merge') //
	               .connectionTimeout(10000) //
	               .receiveTimeout(10000) //
	               .dataType('application/json')//
	               .accept('json') //
	               .put(authority);
	            log.info("Merging succesfully for the authority : " + uids[idx]);
	        });
	    }
	}
}