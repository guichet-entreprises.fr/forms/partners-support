var network = $selection.context.authority.type.getId();
log.info("Network selected : " + network);
var response = nash.service.request(
        '${directory.baseUrl}/v2/ref/list/authority?filters=details.ediCode~'
                + network + '&maxResults=999') //
.accept('json') //
.get();
if (200 == response.getStatus()) {
    var authorities = response.asObject();
    authorities['content']
            .forEach(function(authority) {
                log.info("Authority : " + authority);
                var details = authority.details;
                authority['details']['transferChannels']['backoffice']['state'] = 'enabled';
                authority.details = details;
                log.info('Details : {}', details);
                
                var response = nash.service.request(
                        '${directory.baseUrl}/v1/authority/merge') //
                .connectionTimeout(10000) //
                .receiveTimeout(10000) //
                .dataType('application/json') //
                .accept('json') //
                .put(authority);

                if (200 != response.getStatus()) {
                    log
                            .error(
                                    'An error occured when trying to merge authority {}, HTTP code {}',
                                    authority['label'], response.getStatus());
                } else {
                    log.info('Merging the authority {} with success',
                            authority['label']);
                }
                log.info('Ending enabling backoffice channel configuration')
            });
}
