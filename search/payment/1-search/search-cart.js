var recordUid = _INPUT_.record.search.recordId;
var orderId = _INPUT_.record.search.orderId;

// Test if a search is needed
if ( (null == recordUid || recordUid == "") && (null == orderId || orderId == ""))  {
	return;
};

// --------------------------------------------------------------------------------------
// Find the carts
// --------------------------------------------------------------------------------------
function findCart(recordUid, orderId) {
	fullUrl = _CONFIG_.get('payment.proxy.api.private.baseUrl') + '/api/v1/cart';

	// perform the call
	var searchResult = null;
	try {		
		var proxyRequest = nash.service.request(fullUrl) //
		.param('startIndex', 0) //
		.param('maxResults', 20) //
		;
		
		if (null != recordUid && recordUid !== "") {
			proxyRequest.param('filters', 'recordUid:' + recordUid);
		}
		
		if (null != orderId && orderId !== "") {
			proxyRequest.param('filters', 'orderId:' + orderId);
		}
		
		var searchResult = proxyRequest //
			.connectionTimeout(10000) //
			.receiveTimeout(10000) //
			.param('orders', 'recordUid:desc') //
			.accept('json')
			.get();

	} catch (error) {
		log.info('=> Error occured while calling proxy payment : ' + error);
		return null;
	};
	
	if (searchResult == null || searchResult.getStatus() != 200) {
		return null;
	};

	return searchResult.asObject();
};
// --------------------------------------------------------------------------------------

// Load the screen for the user
var display = nash.instance.load("display-search.xml");

var cartFound = findCart(recordUid, orderId);

if (null == cartFound) {
	display.bind("record.results.carts", { "nbCarts": "0" });
	display.bind("record.results.carts", { "cart": [] });
	return; 
}

display.bind("record.results.carts", { "nbCarts": cartFound.totalResults });
display.bind("record.results.carts", { "cart": cartFound.content });
