log.info("_INPUT_ : " + _input);
log.info("searchButton value : " + _input.record.search.searchButton.value);

var recordUid = nash.record.description().getRecordUid();
log.info("Back to the first page for the record {}", recordUid);
return { url: "/record/" + recordUid + "/0/page/0" };