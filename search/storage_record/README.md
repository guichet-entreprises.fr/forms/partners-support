# Description of search records from STORAGE :

The goal is to search STORAGE records.

# Teams concerned :
* The admin team
* The support team

## First step : Searching records using criteria

Searching for records based on multiples criteria like the following ones :
* Storage identifier
* Storage reference
* Storage date
* Storage tray
* Guichet-Entreprises identifier or email
* Guichet-Partenaires authority
* Record's title or description

This step will display the records resulting from the criteria.

Here are the information displayed :
* Storage identifier
* Storage reference
* Storage tray
* Tracker history

This service will allow you to download any Storage record displayed

