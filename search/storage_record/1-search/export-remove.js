// try to find the id to search
var records = _INPUT_.record.results.storageRecords.record;

if ( null == records ) {
	return;
}

// --------------------------------------------------------------------------------------
// Remove STORAGE record
// --------------------------------------------------------------------------------------
function removeRecord(uid, tray) {
	log.info('Start removing record {} from tray {}', uid, tray);	
	try {        
		nash.service.request('${storage.gp.url.ws.private}/v1/tray/{storageTray}/file/{storageId}', tray, uid) //
			.connectionTimeout(20000) //
			.receiveTimeout(20000) //
			.delete() //
			;      
	} catch (error) {
		log.error('=> Error occured while removing archive : ' + uid + ' with error : ' + error);
		throw error;
	}
	log.info("Removing record {} with success", uid);
}

// --------------------------------------------------------------------------------------
// Export record to support
// --------------------------------------------------------------------------------------
function exportRecord(uid, tray) {
	log.info('Start exporting record {} to support', uid);
	try {        
		nash.service.request('${storage.gp.url.ws.private}/v1/tray/{storageTray}/file/{storageId}/export', tray, uid) //
			.connectionTimeout(20000) //
			.receiveTimeout(20000) //
			.post(null) //
			;      
	} catch (error) {
		log.error('=> Error occured while removing archive : ' + uid + ' with error : ' + error);
		throw error;
	}
	log.info("Exporting record to support with success : {}", uid);
}

// --------------------------------------------------------------------------------------
// Main
// --------------------------------------------------------------------------------------
for (var idx = 0; idx < records.size(); idx++) {
	var rec = records[idx];
	if (null != rec.id && '' != rec.id) {		
		if (rec.removeButton.value == 'yes') {
			removeRecord(rec.id, rec.tray);
			return { url: "/record/" + nash.record.description().getRecordUid() + "/0/page/0" };
		} else if (rec.exportButton.value == 'yes') {
			exportRecord(rec.id, rec.tray);
			return { url: "/record/" + nash.record.description().getRecordUid() + "/0/page/0" };
		}
	}
}
// --------------------------------------------------------------------------------------

