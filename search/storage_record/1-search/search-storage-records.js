// try to find the id to search
var storageId = _INPUT_.record.search.uid;
var keyWord = _INPUT_.record.search.keyWord;
var dateSearch = _INPUT_.record.search.date;
var traySearch = _INPUT_.record.search.tray;
var author = _INPUT_.record.search.author;
var description = _INPUT_.record.search.description;
var authority = _INPUT_.record.search.authority;

// Test if a search is needed
//if ( (storageId == null || storageId === "") && dateSearch == null && traySearch == null && (keyWord == null || keyWord === "") && (author == null || author === "") && (description == null || description === "") && null == authority) {
if ( traySearch == null ) {
	return;
}

// --------------------------------------------------------------------------------------
// Find the user
// --------------------------------------------------------------------------------------
function findRecords(storageId, keyWord, dateSearch, traySearch, author, description, authority, xmlDisplay, resultsXmlPath) {
    //Reset debug information
    xmlDisplay.bind(resultsXmlPath + ".input", {
        'url' : null,
        'uid' : null,
        'keyWord' : null,
        'author' : null,
        'authority' : null,
        'description' : null,
        'tray' : null
    });

    //-->Filters
    var filters = {
        'uid' : (null != storageId && "" !== storageId) ? storageId : null,
        'created>=' : (null != dateSearch && null != dateSearch.from && "" !== dateSearch.from) ? dateSearch.from.getTimeInMillis() : null,
        'created<=' : (null != dateSearch && null != dateSearch.to && "" !== dateSearch.to) ? dateSearch.to.getTimeInMillis() : null
    }
    
    var queryFilter = [];
    ['uid'].forEach(function (filter) {
        if (null != filters[filter] && "" !== filters[filter]) {
            queryFilter.push(filter + ':' + filters[filter]);
            xmlDisplay.bind(resultsXmlPath + ".input", {
            	filter : filters[filter]
            });
        }
    });
    
    ['created>=', 'created<='].forEach(function (filter) {
        if (null != filters[filter]) {
            //-->Use pattern yyyy-MM-dd
			log.info('Input date value filter : {}', filters[filter]);
            var created = new Date(parseInt(filters[filter]));
            var month = created.getMonth() + 1;
            var dayOfMonth = created.getDate();
            var createdDateValue = [ //
                created.getFullYear(), //
                ( month > 9 ? '' : '0') + month, //
                ( dayOfMonth >9 ? '' : '0') + (dayOfMonth) //
            ].join('-'); //
            queryFilter.push(filter + createdDateValue);
			log.info('Filtering by {} : {}', filter, createdDateValue);
            xmlDisplay.bind(resultsXmlPath + ".input", {
            	filter: createdDateValue
            });
        }
    });

    //-->Full text search
    searchTerms = [];
    if ( traySearch != null) {
        if ('all' != traySearch.getId()) {
            queryFilter.push('tray:' + traySearch.getId());
            log.debug('Filtering by tray : {}', traySearch.getId());
            xmlDisplay.bind(resultsXmlPath + ".input", {
                "tray": traySearch.getLabel()
            });
        }
    }
    
    if ( description != null) {
        searchTerms.push(description);
        log.debug('Filtering by description : {}', description);
        xmlDisplay.bind(resultsXmlPath + ".input", {
            "description": description
        });
    }
    
    if ( keyWord != null) {
        searchTerms.push(keyWord);
        log.debug('Filtering by keyWord : {}', keyWord);
        xmlDisplay.bind(resultsXmlPath + ".input", {
            "keyWord": keyWord
        });
    }
    
    if ( author != null ) {
        // perform the call
        fullUrl = _CONFIG_.get('account.ge.baseUrl.read') + '/private/users/' + author;
        searchResult = null;
        try {
            searchResult = nash.service.request(fullUrl) //
                .dataType('application/json') //
                .accept('json')
                .get();
            searchTerms.push(searchResult.asObject().trackerId);
            log.debug('Filtering by author : {}', searchResult.asObject().trackerId);
            xmlDisplay.bind(resultsXmlPath + ".input", {
                "author": author
            });
        } catch (error) {
            log.error('=> Error occured while calling account : ' + error);
            return null;
        }
    }
    
    if ( authority != null ) {
        searchTerms.push(authority.getId());
        log.debug('Filtering by authority author : {}', authority.getId());
        xmlDisplay.bind(resultsXmlPath + ".input", {
            "authority": authority.getLabel()
        });
    }

    fullUrl = _CONFIG_.get('storage.gp.url.ws.private') + '/v1/Record';

    //save call data
    xmlDisplay.bind(resultsXmlPath + ".input", {
        "url": fullUrl,
    });
    
    try {        
        var nashRequest = nash.service.request(fullUrl) //
            .param('startIndex', 0) //
            .param('maxResults', 100) //
            ;
        
        log.info('queryFilter : {}', JSON.stringify(queryFilter));
        queryFilter.forEach(function (filter) {
            nashRequest.param('filters', filter) //
        });
        
        if (searchTerms.length > 0) {
            nashRequest.param('searchTerms', searchTerms.join(' | '));
        }
        
        var searchResult = nashRequest //
            .connectionTimeout(20000) //
            .receiveTimeout(20000) //
            .param('orders', 'created:desc') //
            .param('orders', 'uid:desc') //
            .get() //
    } catch (error) {
        log.error('=> Error occured while calling storage : ' + error);
        xmlDisplay.bind(resultsXmlPath + ".output", {
            "status": "ERROR",
            "response": "" + error,
        });
        return null;
    };

    responseObj = searchResult.asObject();
    
    xmlDisplay.bind(resultsXmlPath + ".output", {
        "status": searchResult.getStatus(),
        "response": "" + responseObj,
    });

    // test the 200 code
    if (searchResult == null || searchResult.getStatus() != 200) {
        return null;
    }

    return responseObj;
};
// --------------------------------------------------------------------------------------


// Load the screen for the user
var display = nash.instance.load("display-search.xml");

var storageRecords = findRecords(storageId, keyWord, dateSearch, traySearch, author, description, authority, display, "record.debug.storageRecords");

if (null == storageRecords) {
    display.bind("record.results.storageRecords", { "nbRecords": "0" });
    display.bind("record.results.storageRecords", { "record": [] });
    return; 
}

display.bind("record.results.storageRecords", { "nbRecords": storageRecords.totalResults });
storageRecords.content.forEach(function (rec) {
    rec['download'] = '[Veuillez cliquer ici pour télécharger le dossier](' + _CONFIG_.get('storage.gp.url.ui.public') + '/archive/tray/'+ rec.tray + '/id/' + rec.id + '/download)';
});
display.bind("record.results.storageRecords", { "record": storageRecords.content });