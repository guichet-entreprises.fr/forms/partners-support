function getDateFormatShort(time){
	var d = new Date(parseInt(time));
	var date = ("0" + d.getDate()).slice(-2) + "/" + ("0" + (d.getMonth()+1)).slice(-2) + "/" + d.getFullYear() ;
	return date;
}

var display = nash.instance.load("display-search.xml");

if(_input.record.input.authority == null) return;

var rights = "{['" + _input.record.input.authority + "':['referent']]}";

var nbResults = _input.record.input.nbRecords;

if(nbResults == null) nbResults = 100;

// --------------------------------------------------------------------------------------
// Find the records
// --------------------------------------------------------------------------------------
function findRecords() {

    var fullUrl = '${forms.backoffice.private.url}/v1/Record';
    //var fullUrl = 'http://backoffice-forms-ws.recr.guichet-partenaires.loc:12030/api/record/v2/search';


    //save call data
    display.bind("record.debug.boRecords.input", {
        "url": fullUrl,
    });
    
    try {        
        var searchResult = nash.service.request(fullUrl)
            .param('startIndex', 0)
            .param('maxResults', nbResults)
            .param('orders', 'created:asc')
            .connectionTimeout(10000)
            .receiveTimeout(10000)
			.header('X-Roles', JSON.stringify(rights))
            .get()
    } catch (error) {
        log.error('=> Error occured while calling nash-bo : ' + error);
        display.bind("record.debug.boRecords.output", {
            "status": "ERROR",
            "response": "" + error + " " + nbResults + " " + rights,
        });
        return null;
    };

    responseObj = searchResult.asObject();
 

    return responseObj;
};
// --------------------------------------------------------------------------------------


// Load the screen for the user



var result = "";

var boRecords = findRecords();

if(boRecords == null){
	display.bind("record.results.boRecords", { "record": "no results" });
	return;
}

var output = '<div class="table-responsive"><table  class="table table-striped text-center"><thead><tr>';
output += "<th style=\"text-align:center\">id</th>";
output += "<th style=\"text-align:center\">date de création</th>";
output += "<th style=\"text-align:center\">titre</th>";

boRecords.content.forEach(function (rec) {
	result = result + rec.code + ";" + getDateFormatShort(rec.created) + ";" + rec.title  + '\n';
	output += "<tr><td>" + rec.code + "</td><td>" + getDateFormatShort(rec.created) + "</td><td>" + rec.title + "</td></tr>";
});

output+="</table></div>";

display.bind("record.results.boRecords", { "record": result});
display.bind("record.results.boRecords", { "data": output});
display.bind("record.results.boRecords", { "nbRecord": boRecords.totalResults});
