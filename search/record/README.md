# Description of searching for records:
<br />

The goal is to retrieve information about any record created.

<br />
<br />

# Team(s) concerned :
* The support and development teams

# Form input

The form allow the user to filter the search using criteria as below : 
* Record identifier
* Record's title as key words
* Creation date

This step will display to the user the records using the criteria selected previously.
The information displayed for a record is :
* Record's title
* Author identifier
* Record identifier
* Creation and modification date
* Tracker history
* Technical details about the formality
