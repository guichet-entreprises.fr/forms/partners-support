// functions
function pad(s) {
    return (s < 10) ? '0' + s : s;
}

function dateToString(src) {
    var dt = new Date(parseInt(src));
    return dt.getFullYear() + '-' + pad((dt.getMonth() + 1).toString()) + '-' + pad(dt.getDate().toString()) + ' ' + pad(dt.getHours().toString()) + ':' + pad(dt.getMinutes().toString()) + ':' + pad(dt.getMinutes().toString()) + ':' + pad(dt.getSeconds().toString());
}

// try to find the id to search
var uid = _INPUT_.record.search.uid;
var dateSearch = _INPUT_.record.search.date;
var description = _INPUT_.record.search.description;
var author = _INPUT_.record.search.author;
var spec = _INPUT_.record.search.spec;

// Test if a search is needed
if ( dateSearch == null && (uid == null || uid === "")  && (description == null || description === "") && (author == null || author === "") && (spec == null || spec === "") ) {
	return;
}

// --------------------------------------------------------------------------------------
// Find the record
// --------------------------------------------------------------------------------------
function findRecords(spec, uid, dateSearch, description, author, xmlDisplay, resultsXmlPath) {
	//-->Filters
	var filters = {
		'uid' : (null != uid && "" !== uid) ? uid : null,
	    'spec' : (null != spec && "" !== spec) ? spec : null,
		'description' : (null != description && "" !== description) ? description : null,
		'created>=' : (null != dateSearch && null != dateSearch.from && "" !== dateSearch.from) ? dateSearch.from : null,
		'created<=' : (null != dateSearch && null != dateSearch.to && "" !== dateSearch.to) ? dateSearch.to : null
	}
	
	var queryFilter = [];
	['spec', 'uid', 'description'].forEach(function (filter) {
		if (null != filters[filter] && "" !== filters[filter]) {
			queryFilter.push(filter + ':' + filters[filter]);
		}
	});
	
	['created>=', 'created<='].forEach(function (filter) {
		if (null != filters[filter] && "" !== filters[filter]) {
			
			var dateTemp = new Date(parseInt((filters[filter]).getTimeInMillis()));
			var year = dateTemp.getFullYear();
			var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
			var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
			var date = year + "-" + month + "-" + day ; 

			queryFilter.push(filter + date);
		}
	});
	
	if (null != author && "" !== author) {
		fullUrl = _CONFIG_.get('account.ge.baseUrl.read') + '/private/users/' + author;

		searchResult = null;
		try {
			searchResult = nash.service.request(fullUrl) //
				.dataType('application/json') //
				.accept('json')
				.get();

			// test the 200 code
			if (searchResult == null || searchResult.getStatus() != 200) {
				return null;
			}
			
			queryFilter.push('author:' + searchResult.asObject().trackerId);
		} catch (error) {
			log.info('=> Error occured while calling account : ' + error);
			return null;
		}
	}
	
	//-->Full text search
	fullUrl = _CONFIG_.get('forms.private.url') + '/v1/Record';

	//save call data
	xmlDisplay.bind(resultsXmlPath + ".input", {
		"url": fullUrl
	});
	
	try {		
		var nashRequest = nash.service.request(fullUrl) //
			.param('startIndex', 0) //
			.param('maxResults', 100) //
			;
		
		log.info('queryFilter : {}', JSON.stringify(queryFilter));
		queryFilter.forEach(function (filter) {
			nashRequest.param('filters', filter) //
		});
		
		if (null != description && description !== "") {
			nashRequest.param('q', description);
		}
		
		var searchResult = nashRequest //
			.connectionTimeout(10000) //
			.receiveTimeout(10000) //
			.param('orders', 'created:desc') //
			.param('orders', 'uid:desc') //
			.get() //
	} catch (error) {
		log.error('=> Error occured while calling account : ' + error);
		xmlDisplay.bind(resultsXmlPath + ".output", {
			"status": "ERROR",
			"response": "" + error,
		});
		return null;
	};

	responseObj = searchResult.asObject();
	
	xmlDisplay.bind(resultsXmlPath + ".output", {
		"status": searchResult.getStatus(),
		"response": "" + responseObj,
	});

	// test the 200 code
	if (searchResult == null || searchResult.getStatus() != 200) {
		return null;
	}

	return responseObj;
};
// --------------------------------------------------------------------------------------


// Load the screen for the user
var display = nash.instance.load("display-search.xml");

var nashRecords = findRecords(spec, uid, dateSearch, description, author, display, "record.debug.nashRecords");

if (null == nashRecords) {
	display.bind("record.results.nashRecords", { "nbRecords": "0" });
	display.bind("record.results.nashRecords", { "record": [] });
	return; 
}

nashRecords.content.forEach(function (record) {
	record['created'] = dateToString(record.created);
	record['updated'] = dateToString(record.updated);
});
display.bind("record.results.nashRecords", { "nbRecords": nashRecords.totalResults });
display.bind("record.results.nashRecords", { "record": nashRecords.content });