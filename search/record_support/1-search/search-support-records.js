
var display = nash.instance.load("display-search.xml");
// --------------------------------------------------------------------------------------
// Find the records
// --------------------------------------------------------------------------------------
function findRecords() {

    fullUrl = '${forms.support.private.url}/v1/Record';

    //save call data
    display.bind("record.debug.supportRecords.input", {
        "url": fullUrl,
    });
    
    try {        
        var nashRequest = nash.service.request(fullUrl) //
            .param('startIndex', 0) //
            .param('maxResults', 500);
 
        var searchResult = nashRequest //
            .connectionTimeout(10000) //
            .receiveTimeout(10000) //
            .param('orders', 'created:asc') //
            .param('orders', 'uid:asc') //
            .get() //
    } catch (error) {
        log.error('=> Error occured while calling storage : ' + error);
        display.bind("record.debug.supportRecords.output", {
            "status": "ERROR",
            "response": "" + error,
        });
        return null;
    };

    responseObj = searchResult.asObject();
 

    return responseObj;
};
// --------------------------------------------------------------------------------------


// Load the screen for the user

var result = "";

var supportRecords = findRecords();

if(supportRecords == null){
	display.bind("record.results.supportRecords", { "record": "no results" });
	return;
}

supportRecords.content.forEach(function (rec) {
	result = result + rec.code +'\n';
});
display.bind("record.results.supportRecords", { "record": result});
display.bind("record.results.supportRecords", { "nbRecord": supportRecords.totalResults});
