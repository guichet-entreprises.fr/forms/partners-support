# Description of replay a record :

The goal is to replay all post-user steps.

# Authorities concerned :
* *GE*
	* The support team
	* The admin team

## First step : Input form 

The form allows the user to choose where to look for the declarant and enter his **email** in order to his information.

## Second step : Confirmation form

This step will check the existance of a user matching the entered **email** and will display information about the found user.
