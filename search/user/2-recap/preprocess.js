var choice = _INPUT_.formSaisi.choixAccount.choice;
_log.info("choice is {}", choice);
var userEmail = _INPUT_.formSaisi.userEmail.email;
_log.info("userEmail is {}", userEmail);
var response_message = "";
if(Value('id').of(_INPUT_.formSaisi.choixAccount.choice).eq('ge')){
	_log.info("source is GE");
	//Si la source de recherche est guichet entreprises
	var userResponse = nash.service.request('${account.ge.baseUrl.read}/private/users/{mail}', userEmail) //
				.dataType('application/json') //
				.accept('json') //
				.get();
	var response_status = userResponse.getStatus();	
	var user = userResponse.asObject();
	_log.info("user is {}", user);
	_log.info("user.errorCode is {}", user.errorCode);
	if (user.errorCode == null) {
		response_message = "L'utilisateur a été trouvé avec succès.";
	}else{
		response_message = "L'utilisateur recherché n'existe pas.";
	}		

}else{
	_log.info("source is GP");
	//Si la source de recherche est guichet patenaire
	var userResponse = nash.service.request('${account.baseUrl.read}/private/users/{mail}', userEmail) //
				.dataType('application/json') //
				.accept('json') //
				.get();
	var response_status = userResponse.getStatus();	
	var user = userResponse.asObject();
	_log.info("user is {}", user);
	_log.info("user.errorCode is {}", user.errorCode);
	if (user.errorCode == null) {
		response_message = "L'utilisateur a été trouvé avec succès.";
	}else{
		response_message = "L'utilisateur recherché n'existe pas.";
	}	
}


// Retrieve record info from Account
var userInfo = {};

	
	userInfo.uid = user.trackerId;
	userInfo.civility = user.civility;
	userInfo.firstName = user.firstName;
	userInfo.lastName = user.lastName;
	userInfo.email = user.email;
	userInfo.phone = user.phone;


// bind response
var data = nash.instance.load('data-generated.xml');
data.bind('results',
{
	'response':{
		'message':response_message
	}, 		
	'userInfo': userInfo
});

