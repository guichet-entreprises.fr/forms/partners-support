# Description of archiving a partner record to STORAGE :

The goal is to replay all post-user steps.

# Authorities concerned :
* *GE*
	* The admin team

## First step : Archive a list of partner record

The form allows the user to enter a list of partner records **uid** in order to be moved into STORAGE.

A report will be displayed after every archiving attempt.
