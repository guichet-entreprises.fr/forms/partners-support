// Functions
function pad(number) {
    if ( number < 10 ) {
        return '0' + number;
    }
    return number;
}


// Read input
var uids = _INPUT_.code.input.uids;
var display = nash.instance.load("display.xml");

if ((uids == null) || (uids === "")) { return };

var content = uids.split(/\r?\n/);
var rights = '{"GE" : ["referent", "user"]}';

//-->Define table header
var labels = [
    "Identifiant du dossier partenaire",
    "Titre",
    "Archivé",
    "Identifiant STORAGE",
    "Commentaire"
];

// Add the global count
var style = '<style>tr:nth-child(2n+1) { background-color: #ddd; } tr:nth-child(2n+0) { background-color: #eee; }</style>';
var output = style + '<div class="table-responsive"><table class="table table-striped text-center"><thead><tr>';
for each (var col in labels) {
    output += '<th style="text-align:center">' + col + '</th>';
}
output += "</tr></thead>";


for (var idx = 0; idx < content.length; idx++) {
    // Start exec
    var line = content[idx];
    if (line === "") {
        continue;
    }
    
    var uid = line.split(';')[0];
    output += "<tr>";
    output += "<td>" + uid + "</td>";
    output += "<td>" + ((undefined == line.split(';')[1]) ? "Inconnu" : line.split(';')[1])  + "</td>";

    try {
        
        //TODO : nash.instance.from('nash:nashBo').load('uid').moveTo('storage:storgage?tray=partners')
                
        //-->Download record from NASH BO
        var downloadResponse = nash.service.request('${nashBo.baseUrl}/v1/Record/code/{code}/file', uid)  //
            .header('X-Roles', rights) //
            .connectionTimeout(10000) //
            .receiveTimeout(10000) //
            .continueOnError(true) //
            .get() //
            ;
        
        if (null == downloadResponse) { throw "Impossible de télécharger ce dossier"; }
        if (null != downloadResponse && downloadResponse.status == 403) { throw "Vous ne disposez pas des droits d'accès pour ce dossier"; }
        
        resourceAsBytes = downloadResponse.asBytes();
        if (resourceAsBytes.length == 0) { throw "Impossible de télécharger le dossier"; }
        log.debug('Download record {} successfully', uid);
        //<--
        
        //-->Get all metas from NASH BO
        var request = nash.service.request('${storage.gp.url.ws.private}/v1/tray/{storageTray}/file', 'partners');    
        var metasResponse = nash.service.request('${nashBo.baseUrl}/v1/Meta', uid)  //
            .param('filters', 'uid:' + uid) //
            .connectionTimeout(10000) //
            .receiveTimeout(10000) //
            .continueOnError(true) //
            .get() //
            ;
            
        if (null == metasResponse || metasResponse.status != 200) { throw 'Impossible de récupérer les métadonnées associées au dossier'; }
        metasResponse.asObject() //
            .content.forEach(function (meta) {
                request.param('metas', meta.key + ':' + meta.value);
            });
        log.debug('Get meta record {} successfully', uid);
        //<--
        
        //-->Add metas
        request.param('metas', 'message:Le dossier a été déplacé pour être archivé dans Storage');
        request.param('metas', 'uid:' + nash.record.description().getRecordUid());
        var created = new Date();
        request.param('metas', 'created:' + created.getFullYear() + '-' + pad(created.getMonth() + 1) + '-' + pad(created.getDate()) );
        //<--
        
        //Upload record to STORAGE
        var storeResponse = request.param('storageFilename', uid + '.zip') //
            .param('storageReferenceId', uid) //
            .connectionTimeout(10000) //
            .receiveTimeout(10000) //
            .dataType('form') //
            .post({ 'storageUploadFile' : resourceAsBytes }) //
            ;
            
        if (null == storeResponse || (storeResponse.status != 200 && storeResponse.status != 204)) { throw 'Une erreur technique est survenue lors du transfert du dossier dans STORAGE'; }
        log.debug('Store into partners tray successfully');
        
        //Remove record from NASH BO
        var deleteResponse = nash.service.request('${nashBo.baseUrl}/v1/Record/code/{code}', uid)  //
            .header('X-Roles', rights) //
            .connectionTimeout(10000) //
            .receiveTimeout(10000) //
            .delete() //
            ;
        if (null == deleteResponse || deleteResponse.status != 200) { throw 'Impossible de supprimer le dossier'; }
        log.debug('Remove partner record {} successfully', uid);

        output += "<td>OUI</td>";
        output += "<td>" + storeResponse.asObject()['id'] +"</td>";
        output += "<td></td>";
    } catch (error_msg) {
        output += "<td>NON</td>";
        output += "<td></td>";
        output += "<td>" + error_msg + "</td>";
    }
}
output+="</table></div>";

//-->Bind the report
display.bind("code.exec", { "report": output });
