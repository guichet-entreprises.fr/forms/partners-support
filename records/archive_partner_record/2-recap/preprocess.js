var inputGroup = _input.dataGroup.search;

// The record uid
var uid = inputGroup.uid;

log.info('Start searching info for the record '+uid);

// The user roles
var userRoles = JSON.parse(user.getRoles());

var searchResult = null;
var record;
var response_message;

var filters = [];
filters.push('uid:' + uid);

try {
	searchResult = nash.service.request('${nashBo.baseUrl}/v1/Record') //
		.param('maxResults', 1) //
		.param('filters', filters) //
		.connectionTimeout(10000) //
		.receiveTimeout(10000) //
		.dataType('application/json')//
		.accept('json') //
		.header('X-Roles', JSON.stringify(userRoles)) //
		.get();
		
		log.info('==> Result of record '+uid+' search : ' +searchResult.asObject());
		
		var response_status = searchResult.getStatus();		
		searchResult = searchResult.asObject();
		
	// Success
	if (searchResult != null && response_status == 200 && searchResult.totalResults == 1) {
		record = searchResult.content[0];
		response_message = 'Le dossier a été trouvé avec succès.';

		var recordBo = nash.instance.from("nash://nashBo").load(uid);
		var summary = recordBo.load('1-summary/data.xml');
		
		var recordInfo = {};
		recordInfo.uid = uid;
		recordInfo.title = record.title;
		recordInfo.authority = summary.get('view.informations.destFuncId');
		recordInfo.denomination = summary.get('view.informations.denomination');
		recordInfo.created = new Date(parseInt(record.created)).toISOString().slice(0, 19).replace('T', ' ');
		recordInfo.updated = new Date(parseInt(record.updated)).toISOString().slice(0, 19).replace('T', ' ');
	
	} 	
	// Record not found. 
	else if(searchResult != null && searchResult.totalResults == 0){
		response_message = 'Le dossier demandé n\'existe pas.';
	}
	// Error
	else{
		response_message = 'Une erreur technique est survenue lors de la recherche du dossier.';
	}

} catch(error){
	log.info('Error when searching for the record ' + uid + ' info :  => ' + error);
	response_message = 'Une erreur technique est survenue lors de la recherche du dossier.';
}

// bind response
var data = nash.instance.load('data-generated.xml');
data.bind('results',
{
	'response':{
		'message':response_message
	}, 		
	'recordInfo': recordInfo
});
