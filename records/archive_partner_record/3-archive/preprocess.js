var inputGroup = _input.dataGroup.search;

// The record uid
var uid = inputGroup.uid;

var response_message = null;

// Call nash to archive the record
try {
    var response = nash.service.request('${nashBo.baseUrl}/v1/Record/code/'+uid+'/archive') //
	.post(null);
	
	// Success
	if(response.status == 200){
		response_message = 'Le dossier a été archivé avec succès.';
	}
	// Record not found. 
	else if(response.status == 204){
		response_message = 'Le dossier demandé n\'existe pas.';
	}
	// Error
	else{
		response_message = 'Une erreur technique est survenue lors de l\'archivage.';
	}
	
} catch(error) {
	log.info('Error lors de l\archivage du dossier : ' + uid + ' => ' + error);
	response_message = 'Une erreur technique est survenue lors de l\'archivage.';
}

var data = nash.instance.load('data-generated.xml');
data.bind('dataGroup',
	{
		'result':{
			'response': response_message
		}
	});