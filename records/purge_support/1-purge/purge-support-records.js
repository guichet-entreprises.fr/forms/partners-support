
var display = nash.instance.load("display-purge.xml");
// --------------------------------------------------------------------------------------
// Find the records
// --------------------------------------------------------------------------------------
function purgeRecords() {

    fullUrl = '${forms.support.private.url}/v1/Record/code/';

    //save call data
    display.bind("record.debug.supportRecords.input", {
        "url": fullUrl,
    });
    var result = "";
    try {
		var content = _input.record.input.record.split(/\r?\n/);
		for(i=0 ; i < content.length; i++){
			if (content[i] === "") {
				continue;
			}
			var nashRequest = nash.service.request(fullUrl + content[i]); //
	 
			var purgeResult = nashRequest //
				.connectionTimeout(10000) //
				.receiveTimeout(10000) //
				.delete() //
			responseObj = purgeResult.asObject();
			result = result + content[i] + "\n";
		}
    } catch (error) {
        log.error('=> Error occured while calling storage : ' + error);
        display.bind("record.debug.supportRecords.output", {
            "status": "ERROR",
            "response": "" + error,
        });
        return null;
    };
    return result;
};

var supportRecords = purgeRecords();

display.bind("record.results.supportRecords", { "record": supportRecords});
