// try to find the id to search
var idOrEmail = _INPUT_.record.search.idOrEmail;

// Test if a search is needed
if ((idOrEmail == null) || (idOrEmail === "")) {
	return;
};

// --------------------------------------------------------------------------------------
// Find the user
// --------------------------------------------------------------------------------------
function findUser(idOrEmail, xmlDisplay, resultsXmlPath) {
	fullUrl = _CONFIG_.get('account.ge.baseUrl.read') + '/private/users/' + idOrEmail;

	//save call data
	xmlDisplay.bind(resultsXmlPath + ".input", {
		"idOrEmail": idOrEmail,
		"url": fullUrl,
	});

	// perform the call
	searchResult = null;
	try {
		searchResult = nash.service.request(fullUrl) //
			.dataType('application/json') //
			.accept('json')
			.get();

	} catch (error) {
		log.info('=> Error occured while calling account : ' + error);
		xmlDisplay.bind(resultsXmlPath + ".output", {
			"status": "ERROR",
			"response": "" + error,
		});
		return null;
	};

	responseObj = searchResult.asObject();

	xmlDisplay.bind(resultsXmlPath + ".output", {
		"status": searchResult.getStatus(),
		"response": "" + responseObj,
	});

	// test the 200 code
	if (searchResult == null || searchResult.getStatus() != 200) {
		return null;
	};

	// test the 1005 code
	if ((responseObj == null) || (responseObj['errorCode'] == "1005")) {
		xmlDisplay.bind(resultsXmlPath + ".output", { "found": "" });
		return null;
	};

	return responseObj;
};
// --------------------------------------------------------------------------------------


// --------------------------------------------------------------------------------------
// Bind the user
// --------------------------------------------------------------------------------------
function bindUser(xmlDisplay, xmlPath, userData) {
	xmlDisplay.bind(xmlPath, {
		"firstName": userData['firstName'],
		"lastName": userData['lastName'],
		"civility": userData['civility'],
		"email": userData['email'],
		"trackerId": userData['trackerId'],
		"connectionDate": userData['connectionDate'],
	});
};
// --------------------------------------------------------------------------------------

// Load the screen for the user
var display = nash.instance.load("display-search.xml");

var userFound = findUser(idOrEmail, display, "record.debug.account");

if (null == userFound) { return; };

// We haver a user

bindUser(display, "record.results.user", userFound);
