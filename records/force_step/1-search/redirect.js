log.info("Button search value : {}", _INPUT_.record.search.searchButton.value);
log.info("Button unlock value : {}", _INPUT_.record.results.unlock.unlockButton.value);

var recordUid = nash.record.description().getRecordUid();
log.info("Back to the first page for the record {}", recordUid);
return { url: "/record/" + recordUid + "/0/page/0" };