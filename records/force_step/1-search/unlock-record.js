log.info('Unlock process');

// try to find the id to search
log.info("_INPUT_ : {}", _INPUT_);
var uid = _INPUT_.record.search.uid;

// Test if a search is needed
if ( (uid == null || uid === "") ) {
	return;
}

//--------------------------------------------------------------------------------------
//Find the user
//--------------------------------------------------------------------------------------
function lockRecord(uid,xmlDisplay, resultsXmlPath) {
	log.info('Starting locking record {}', uid);
	fullUrl = _CONFIG_.get('forms.private.url') + '/v1/Record/code/' + uid + '/lock';

	//save call data
	xmlDisplay.bind(resultsXmlPath + ".input", {
		"url": fullUrl
	});
	
	try {		
		var lockResult = nash.service.request(fullUrl) //
		    .connectionTimeout(50000) //
		    .receiveTimeout(50000)
		    .accept('application/json') //
		    .post(null);
	} catch (error) {
		log.error('=> Error occured while calling nash : ' + error);
		xmlDisplay.bind(resultsXmlPath + ".output", {
			"status": "ERROR",
			"response": "" + error,
		});
		return false;
	};

	xmlDisplay.bind(resultsXmlPath + ".output", {
		"status": lockResult.getStatus()
	});

	// test the 200 code
	if (lockResult == null || lockResult.getStatus() != 200) {
		log.info('Locking record {} with failure', uid);
		return false;
	}

	log.info('Locking record {} with success', uid);
	return true;
};
//--------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------
// Find the user
// --------------------------------------------------------------------------------------
function unlockRecord(uid,xmlDisplay, resultsXmlPath) {
	log.info('Starting unlocking record {}', uid);
	var lockResult = lockRecord(uid, xmlDisplay, resultsXmlPath);
	if (!lockResult) {
		return false;
	}
	
	fullUrl = _CONFIG_.get('forms.private.url') + '/v1/Record/code/' + uid + '/unlock';

	//save call data
	xmlDisplay.bind(resultsXmlPath + ".input", {
		"url": fullUrl
	});
	
	try {		
		var unlockResult = nash.service.request(fullUrl) //
		    .connectionTimeout(50000) //
		    .receiveTimeout(50000)
		    .accept('application/json') //
		    .post(null);
	} catch (error) {
		log.error('=> Error occured while calling nash : ' + error);
		xmlDisplay.bind(resultsXmlPath + ".output", {
			"status": "ERROR",
			"response": "" + error,
		});
		return false;
	};

	xmlDisplay.bind(resultsXmlPath + ".output", {
		"status": unlockResult.getStatus()
	});

	// test the 200 code
	if (unlockResult == null || unlockResult.getStatus() != 200) {
		log.info('Unlocking record {} with failure', uid);
		return false;
	}

	log.info('Unlocking record {} with success', uid);
	return true;
};
// --------------------------------------------------------------------------------------


// Load the screen for the user
var display = nash.instance.load("display-search.xml");
var unlockRecord = unlockRecord(uid, display, "record.debug.nashRecords");
display.bind("record.results.unlock", { "status": unlockRecord ? "OK" : "KO" });
