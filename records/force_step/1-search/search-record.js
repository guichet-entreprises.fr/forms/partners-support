log.info('Search record process');

//functions
function pad(s) {
    return (s < 10) ? '0' + s : s;
}

function dateToString(src) {
    var dt = new Date(parseInt(src));
    return dt.getFullYear() + '-' + pad((dt.getMonth() + 1).toString()) + '-' + pad(dt.getDate().toString()) + ' ' + pad(dt.getHours().toString()) + ':' + pad(dt.getMinutes().toString()) + ':' + pad(dt.getMinutes().toString()) + ':' + pad(dt.getSeconds().toString());
}

// try to find the id to search
var uid = _INPUT_.record.search.uid;

// Test if a search is needed
if ( (uid == null || uid === "") ) {
	return;
}

// --------------------------------------------------------------------------------------
// Find the user
// --------------------------------------------------------------------------------------
function findRecords(uid,xmlDisplay, resultsXmlPath) {
	//-->Filters
	var filters = {
		'uid' : (null != uid && "" !== uid) ? uid : null,
	}
	
	var queryFilter = [];
	['uid'].forEach(function (filter) {
		if (null != filters[filter] && "" !== filters[filter]) {
			queryFilter.push(filter + ':' + filters[filter]);
		}
	});
	
	//-->Full text search
	fullUrl = _CONFIG_.get('forms.private.url') + '/v1/Record';

	//save call data
	xmlDisplay.bind(resultsXmlPath + ".input", {
		"url": fullUrl
	});
	
	try {		
		var nashRequest = nash.service.request(fullUrl) //
			.param('startIndex', 0) //
			.param('maxResults', 1) //
			;
		
		log.info('queryFilter : {}', JSON.stringify(queryFilter));
		queryFilter.forEach(function (filter) {
			nashRequest.param('filters', filter) //
		});
		
		var searchResult = nashRequest //
			.connectionTimeout(10000) //
			.receiveTimeout(10000) //
			.param('orders', 'created:desc') //
			.param('orders', 'uid:desc') //
			.get() //
	} catch (error) {
		log.error('=> Error occured while calling account : ' + error);
		xmlDisplay.bind(resultsXmlPath + ".output", {
			"status": "ERROR",
			"response": "" + error,
		});
		return null;
	};

	responseObj = searchResult.asObject();
	
	xmlDisplay.bind(resultsXmlPath + ".output", {
		"status": searchResult.getStatus(),
		"response": "" + responseObj,
	});

	// test the 200 code
	if (searchResult == null || searchResult.getStatus() != 200) {
		return null;
	}

	return responseObj;
};
// --------------------------------------------------------------------------------------


// Load the screen for the user
var display = nash.instance.load("display-search.xml");

var nashRecords = findRecords(uid, display, "record.debug.nashRecords");

if (null == nashRecords) {
	display.bind("record.results.nashRecords", { "nbRecords": "0" });
	display.bind("record.results.nashRecords", { "record": null });
	return; 
}

var recordFound = nashRecords.content[0]
recordFound['created'] = dateToString(recordFound.created);
recordFound['updated'] = dateToString(recordFound.updated);

var finished = 'oui';
recordFound.details.steps.elements.forEach(function (step) {
	if (step.status !== 'done') {
		finished = 'non';
		return;
	}
});
recordFound['finished'] = finished;

display.bind("record.results.nashRecords", { "nbRecords": nashRecords.totalResults });
display.bind("record.results.nashRecords", { "record": recordFound });