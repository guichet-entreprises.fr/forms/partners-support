var cerfaFields = {};

cerfaFields['date']  		 			 = $dk45FA2ALPHA.dk450.receiverInfoGroup.date;
cerfaFields['destinataire']  	 		 = $dk45FA2ALPHA.dk450.receiverInfoGroup.destinataire;
cerfaFields['refDossier']  	 			 = $dk45FA2ALPHA.dk450.receiverInfoGroup.refDossier;
cerfaFields['orgDestinataire'] 			 = $dk45FA2ALPHA.dk450.receiverInfoGroup.orgDestinataire.numeroLibelleAdresseOD 
										 + ', ' + ($dk45FA2ALPHA.dk450.receiverInfoGroup.orgDestinataire.complementAdresseOD != null ? $dk45FA2ALPHA.dk450.receiverInfoGroup.orgDestinataire.complementAdresseOD : '')
										 + ', ' + $dk45FA2ALPHA.dk450.receiverInfoGroup.orgDestinataire.codePostalAdresseOD
										 + '  ' + $dk45FA2ALPHA.dk450.receiverInfoGroup.orgDestinataire.villeAdresseOD;
cerfaFields['texte']  	 		 		 = $dk45FA2ALPHA.dk450.receiverInfoGroup.texte;



var finalDoc1 = nash.doc
	.load('Documents/Document.pdf')
	.apply(cerfaFields);

var finalDoc2 = nash.doc
	.load('Documents/Courrier générique aux OD_VF.pdf')
	.apply(cerfaFields);

finalDoc1.append(finalDoc2.save('cerfa.pdf'));

var finalDocItem = finalDoc1.save('Courrier Guichet-Entreprises.pdf');

return spec.create({
id : 'review',
label : 'Envoie du courrier',
groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Courrier générique',
            description : 'Voici le courrier à envoyer au CFE. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});