//the reference id
var referenceId = '';

// the recipient name
var recipientName = $dk45FA2ALPHA.dk450.receiverInfoGroup.destinataire;

// the complet name of the recipient
var recipientNameCompl = '';

// the recipient address
var recipientAddressName = $dk45FA2ALPHA.dk450.receiverInfoGroup.orgDestinataire.numeroLibelleAdresseOD ;

// the complet address of the recipient
var recipientAddressNameCompl =  $dk45FA2ALPHA.dk450.receiverInfoGroup.orgDestinataire.complementAdresseOD != null ? $dk45FA2ALPHA.dk450.receiverInfoGroup.orgDestinataire.complementAdresseOD : '';

// the postal code
var recipientPostalCode = $dk45FA2ALPHA.dk450.receiverInfoGroup.orgDestinataire.codePostalAdresseOD;

// the recipient city
var recipientCity = $dk45FA2ALPHA.dk450.receiverInfoGroup.orgDestinataire.villeAdresseOD; 

// the attachment path
var filePath = '../2-confirmation/generated/generated.formulaire-1-Courrier Guichet-Entreprises.pdf';

var data = [];

var response = nash.service.request('${exchange.baseUrl}/private/postmail') //
.connectionTimeout(2000) //
.receiveTimeout(5000) //
.dataType("form") //
.param("referenceId", referenceId) //
.param("recipientName", recipientName) //
.param("recipientNameCompl", recipientNameCompl) //
.param("recipientAddressName", recipientAddressName) //
.param("recipientAddressNameCompl", recipientAddressNameCompl) //
.param("recipientPostalCode", recipientPostalCode) //
.param("recipientCity", recipientCity) //
.put({ "attachmentFile" : nash.util.resourceFromPath(filePath)});

//Show success message if response is 200
if(response.status == 200){
	return spec.create({ 
		id : 'ConfirmSend',
		label : 'Résultat de la demande',
		groups : [ spec.createGroup({
			id : 'group',
			label : 'Statut',
			help : 'Votre courrier papier a été envoyé avec succès.'
		}) ]
	});
}
//show message with the potential error and block the step
else{
	//Response
	return spec.create({ 
		id : 'ConfirmSend',
		label : 'Résultat de la demande',
		groups : [ 
			spec.createGroup({
				id : 'group',
				label : 'Statut',
				warnings : [
					{ id : 'warningConfiguration', label : "Une erreur technique est survenue lors de l'envoi de votre courrier papier. Veuillez contacter le support technique à l'adresse suivante : support.guichet-entreprises@helpline.fr." }
				]
			}) 
		]
	});
}