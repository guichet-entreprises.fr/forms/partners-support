//Get fields from preivous step
var search = $context.search;

//Get record from source
try {
	var record = nash.instance.from("storage://storage").load(search.record.identifier);
	
	//-->Missing metadata name 'authority' ?
	if (null != record && (null == record.getMeta() || null == record.metas('authority') || record.metas('authority').isEmpty()) ) {
		var groups = [
			spec.createGroup({
				id : 'result',
				label : "Informations du dossier",
				warnings : [ { description : "Aucune autorité n'est configurée dans les métadonnées du dossier recherché " + search.record.identifier + " dans l'application Storage. A la validation de cette étape, vous serez redirigé à la précédente étape." } ],
				data : [			
					spec.createGroup({
						id : 'record',
						label : "Recherche dossier",
						data : [
							spec.createData({
								'id' : 'state',
								'label': 'Statut',
								'type': 'StringReadOnly',
								'value' : 'Inconnu'
							})
						]
					})
				]
			})
		];

		return spec.create({
			id : 'recap',
			label : 'Rappel des autorités compétentes relatives au dossier',
			groups : groups
		});
	}
} catch (error) {
	log.error(error);
	var groups = [
		spec.createGroup({
			id : 'result',
			label : "Informations du dossier",
			warnings : [ { description : "Aucun dossier trouvé pour le dossier recherché " + search.record.identifier + " dans l'application Storage. A la validation de cette étape, vous serez redirigé à la précédente étape." } ],
			data : [			
				spec.createGroup({
					id : 'record',
					label : "Recherche dossier",
					data : [
						spec.createData({
							'id' : 'state',
							'label': 'Statut',
							'type': 'StringReadOnly',
							'value' : 'Inconnu'
						})
					]
				})
			]
		})
	];

	return spec.create({
		id : 'recap',
		label : 'Rappel des autorités compétentes relatives au dossier',
		groups : groups
	});
}

//-->Init checkboxes conftype 
var referentials = [];
record.metas('authority').forEach(function(funcId) {
	try {
		var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', funcId) //
			.connectionTimeout(10000) //
			.receiveTimeout(10000) //
			.accept('json') //
			.get();

		var receiverInfo = response.asObject();
		var ediCode = !receiverInfo.details.ediCode ? '' : receiverInfo.details.ediCode;
		var funcLabel = !receiverInfo.label ? funcId + ' (' + ediCode + ')' : receiverInfo.label + ' (' + ediCode + ')';
				
		referentials.push( { 'id': funcId, 'label' : funcLabel } );
	} catch (error) {
		log.error("An error occured when finding authority with code {}", funcId);
		referentials.push( { 'id': funcId, 'label' : funcId} );
	}
});

//-->Add record details
var titles = record.metas('title');
var title = (null != titles && !titles.isEmpty()) ? titles.iterator().next() : '';

var denominations = record.metas('denomination');
var denomination = (null != denominations && !denominations.isEmpty()) ? denominations.iterator().next() : '';

var details = [
	{
		'id' : 'uid',
		'label' : 'Identifiant du dossier',
		'value' : record.getDescription().getRecordUid(),
		'mandatory' : true
	},
	{
		'id' : 'title',
		'label' : 'Titre du dossier',
		'value' : title,
		'mandatory' : false
	},
	{
		'id' : 'denomination',
		'label' : 'Dénomination du dossier',
		'value' : denomination,
		'mandatory' : false
	}
];
var dataRecord = [];
details.forEach(function(elem) {
	dataRecord.push(
		spec.createData({
			'id' : elem.id,
			'label': elem.label,
			'mandatory':elem.mandatory,
			'type': 'StringReadOnly',
			'value': elem.value
		})
	);
});

//-->Add authorities checkboxes
var dataAuthority = [];
dataAuthority.push(
	spec.createData({
		'id' : 'authority',
		'label': "Veuillez sélectionner la ou les autorités compétentes pour lesquelles vous désirez rejouer le dossier",
		'mandatory':true,
		'type': 'CheckBoxes',
		'conftype': {
			'referential': {
				'text': referentials
			}
		}
	})
);

//-->Create reccap as data.xml
var groups = [
	spec.createGroup({
		id : 'result',
		label : 'Liste des autorités compétentes',
		help : 'Ci-dessous, un récapitulatif des informations relatives au dossier. Vous trouverez également les autorités destinataires du dossier.',
		data : [
			spec.createGroup({
				id : 'record',
				label : "Informations du dossier",
				data : dataRecord
			}),
			spec.createGroup({
				id : 'select',
				label : "Sélection de l'autorité compétente",
				data : dataAuthority
			})
		]
	})
];

return spec.create({
    id : 'recap',
    label : 'Rappel des autorités compétentes relatives au dossier',
    groups : groups
});