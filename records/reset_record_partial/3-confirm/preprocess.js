
log.info("Record dtate : {}", $recap.result.record.state);

var search = $context.search;
var record = nash.instance.from("storage://storage").load(search.record.identifier);
for (var i = 0; i < $recap.result.select.authority.size(); i++) {
	var funcId = $recap.result.select.authority.get(i).getId();
	var stepId = record.metas('replay-' + funcId).iterator().next();
	record.step(stepId).duplicate();
	log.info("Duplicate step {} with success", stepId);
}

record.uploadTo("markov://markov");
log.info("Record uploaded to markov with success : {}", record.getDescription().getRecordUid());

for (var i = 0; i < $recap.result.select.authority.size(); i++) {
	var funcLabel = $recap.result.select.authority.get(i);
	nash.tracker.post(record.getDescription().getRecordUid(), "Le dossier a été rejoué pour l'autorité compétente " + funcLabel);
}