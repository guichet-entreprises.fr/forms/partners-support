# Description of replay a record :

The goal is to replay all post-user steps.

# Authorities concerned :
* *GE*
	* The support team
	* The admin team

* *GE/Helpline*
	* The support team

## First step : Input form 

The form allows the user to enter the **uid** of the declarant's record in order to replay the post-user steps.

## Second step : Confirmation form

This step will check the existance of a record matching the entered **uid** and will display information about the found record.

## Third step : Replay request

This step will send a request for the record to be replayed.