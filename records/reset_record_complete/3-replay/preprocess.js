var uid = _input.dataGroup.search.uid;

_log.info("recordUid is {}", uid);

// Retrieve record
var response_message = '';
try {
	var record = nash.service.request('${nash.ws.private.url}/v1/Record/code/{code}/file', uid) //
	    .get();
	
	
} catch(error){
	log.info('Error when retrieveing record ' + uid + ' info :  => ' + error);
	response_message = 'Une erreur technique est survenue lors du téléchargement du dossier.';	
}


//Prepare Attachment
var attachment = {
		'attachment' : record.asBytes()
}

if (record.getStatus() == 200){
	try {
		
		//Send to Markov, AWAIT pile
		var responseMarkov = nash.service.request('${ws.markov.url}/v1/queue/AWAIT') //
		.param('overwritePreviousOne', true) //
		.connectionTimeout(10000) //
		.receiveTimeout(10000) //
		.dataType('form') //
		.post(attachment);
		
		_log.info("responseMarkov is {}", responseMarkov.getStatus());
		
		// Success
		if(responseMarkov.status == 200){
			response_message = 'Le dossier a été envoyé vers Markov avec succès.';
			
			log.info('Send a message to TRACKER');
			
			// Tracker message
			nash.tracker.post(uid, 'Le dossier a été rejoué');
			
			log.info('Message sent to TRACKER');
		}
		// Record not found. 
		else if(responseMarkov.status == 204){
			response_message = 'Le dossier demandé n\'existe pas.';
		}
		// Error
		else{
			response_message = 'Une erreur technique est survenue lors de l\'envoi vers Markov.';
		}
	
	} catch(error){
		log.info('Error when retrieveing record ' + uid + ' info :  => ' + error);
		response_message = 'Une erreur technique est survenue lors de l\'envoi du dossier vers Markov.';
	}
	
	
}

// Bind results
var data = nash.instance.load('data-generated.xml');
data.bind('dataGroup',
	{
		'inputGroup':{
			'uid':uid
		},
		'outputGroup':{
			'response': response_message
		}
	});