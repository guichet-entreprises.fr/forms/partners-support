var inputGroup = _input.dataGroup.search;

// The record uid
var uid = inputGroup.uid;

var response_message = null;

// The user roles
var userRoles = JSON.parse(user.getRoles());
// ajout du role si le user n'est pas sur "GE" : pour voir les dossiers
if(!userRoles.hasOwnProperty("GE")){
	userRoles["GE"]= ["support", "referent"];
}
// Call nash to archive the record
try {
    var response = nash.service.request('${nash.ws.private.url}/v1/Record/code/'+uid)
		.header('X-Roles', JSON.stringify(userRoles)) 
		.delete();
	
	// Success
	if(response.status == 200){
		response_message = 'Le dossier a été supprimé avec succès.';
	}
	// Record not found. 
	else if(response.status == 204){
		response_message = 'Le dossier demandé n\'existe pas.';
	}
	// Error
	else{
		response_message = 'Une erreur technique est survenue lors de la suppression : ' + response.status + " - " + response;
	}
	
} catch(error) {
	log.info('Error lors de l\archivage du dossier : ' + uid + ' => ' + error);
	response_message = 'Une erreur technique est survenue lors de la suppression :' + error;
}

var data = nash.instance.load('data.xml');
data.bind('dataGroup',
	{
		'result':{
			'response': response_message
		}
	});