var inputGroup = _input.dataGroup.search;

// The record uid
var uid = inputGroup.uid;

log.info('Start searching info for the record '+uid);

// The user roles
var userRoles = JSON.parse(user.getRoles());
// ajout du role si le user n'est pas sur "GE" : pour voir les dossiers
if(!userRoles.hasOwnProperty("GE")){
	userRoles["GE"]= ["support", "referent"];
}
var searchResult = null;
var record;
var response_message;

var filters = [];
filters.push('uid:' + uid);

try {
	searchResult = nash.service.request('${nash.ws.private.url}/v1/Record') //
		.param('maxResults', 1) //
		.param('filters', filters) //
		.connectionTimeout(10000) //
		.receiveTimeout(10000) //
		.dataType('application/json')//
		.accept('json') //
		.header('X-Roles', JSON.stringify(userRoles)) //
		.get();
		
		log.info('==> Result of record '+uid+' search : ' +searchResult.asObject());
		
		var response_status = searchResult.getStatus();		
		searchResult = searchResult.asObject();
		
	// Success
	if (searchResult != null && response_status == 200 && searchResult.totalResults == 1) {
		record = searchResult.content[0];
		response_message = 'Le dossier a été trouvé avec succès.';
	} 	
	// Record not found. 
	else if(searchResult != null && searchResult.totalResults == 0){
		response_message = 'Le dossier demandé n\'existe pas.';
	}
	// Error
	else{
		response_message = 'Une erreur technique est survenue lors de la recherche du dossier.';
	}

} catch(error){
	log.info('Error when searching for the record ' + uid + ' info :  => ' + error);
	response_message = 'Une erreur technique est survenue lors de la recherche du dossier.';
}

var recordInfo = {};
var name;
var trackerId;

// record info
if(record != null){
	
	var userResponse = null;
	try {
		userResponse = nash.service.request('${account.ge.baseUrl.read}/private/users/{mail}', record.author) //
			.dataType('application/json') //
			.accept('json') //
			.get();
	} catch (e) {
		log.info('Error when calling account with author '+record.author+' => '+e);
	}
	if (userResponse != null && userResponse.getStatus() == 200) {
		log.info('==> Response of account for the author '+record.author+' => '+userResponse.asObject());
		
		userResponse = userResponse.asObject();
		name = userResponse.firstName + ' ' + userResponse.lastName;
		trackerId = userResponse.trackerId;
	}
	
	
	recordInfo.uid = uid;
	recordInfo.author = record.author;
	recordInfo.name = name;
	recordInfo.trackerId = trackerId;
	recordInfo.title = record.title;
	recordInfo.created = new Date(parseInt(record.created)).toISOString().slice(0, 19).replace('T', ' ');
	recordInfo.updated = new Date(parseInt(record.updated)).toISOString().slice(0, 19).replace('T', ' ');
}

// bind response
var data = nash.instance.load('data.xml');
data.bind('results',
{
	'response':{
		'message':response_message
	}, 		
	'recordInfo': recordInfo
});
