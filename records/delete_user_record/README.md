# Description of remove a record :

The goal is to remove logically a record.

# Authorities concerned :
* *GE*
	* The support team
	* The admin team

* *GE/Helpline*
	* The support team

# First step : Input form

The form allows the user to enter the **uid** of the declarant's record in order to remove it logically.

## Second step : Confirmation form

This step will check the existance of a record matching the entered **uid** and will display information about the found record.

## Third step : Remove request

This step will send a request for the record to be removed.