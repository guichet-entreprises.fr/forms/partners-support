

function getDateFormat(d){
	var date = ("0" + d.getDate()).slice(-2) + "/" + ("0" + d.getMonth()).slice(-2) + "/" + d.getFullYear() 
		+ " à " + ("0" + d.getHours()).slice(-2) + "h" + ("0" + d.getMinutes()).slice(-2) + ":" + ("0" + d.getSeconds()).slice(-2);
	return date;
}

var idTracker = _record.trackerForms.infoForms.idDossier;

var url = '${tracker.baseUrl}/v1/uid/' + idTracker;
log.info('using tracker URL : {}', url);
	 var response = nash.service.request(url) //
			.connectionTimeout(10000) //
			.receiveTimeout(10000)
			.accept('json')
			.get();
			
var responseMessage =  response.asObject().messages;
var messagesTracker = "";

for(var element in  responseMessage) {
  var date = new Date(parseInt(responseMessage[element].created));
  messagesTracker  = messagesTracker + getDateFormat(date) + " (" + responseMessage[element].client + ") : " + responseMessage[element].content + '\n';
}
log.info('list messages tracker : {}', messagesTracker);
		
return spec.create({
	id: 'tracker',
	label: "Récapitlatif",
	groups: [spec.createGroup({
			id: 'tracker',
			label: "Votre message à bien été ajouté :",
			data: [spec.createData({
					id: 'dataTracker',
					label: "Historique du dossier : " + idTracker,
					type: 'TextReadOnly',
					mandatory: false,
					value: messagesTracker
				})
			]
		})]
});
