
var idTracker = _record.trackerForms.infoForms.idDossier;
var message   = _record.tracker.tracker.messageTracker;
var currentUserId = nash.record.description().getAuthor();

// call tracker service to publish a message
var response = nash.service.request('${tracker.baseUrl}/v1/uid/{idTracker}/msg',idTracker) //
		.connectionTimeout(10000) //
		.receiveTimeout(10000)
		.param("content", [ message ])
		.param("author", [ currentUserId ])
		.post("");

			
return spec.create({
	id: 'tracker',
	label: "Récapitlatif",
	groups: [spec.createGroup({
			id: 'tracker',
			label: "Historique",
			data: [spec.createData({
					id: 'dataTracker',
					label: "Historique du dossier : " + idTracker + message,
					type: 'TextReadOnly',
					mandatory: false,
					value: response.asObject()
				})
			]
		})]
});
