# Description :

The goal is to force archiving for a partner record.

# Authorities concerned :
* *GE*
	* The admin team

# Single step : Search and archive a partner record

The form allows the user to search for partner record using differents criteria :
* Backoffice record identifier
* Authority
* Date range

This step displays completed partner records to be archived.

After choosing a single record, this one will be : 
* stored into Storage (partner tray)
* removed into Backoffice
