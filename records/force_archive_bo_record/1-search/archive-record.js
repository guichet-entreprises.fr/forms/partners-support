// --------------------------------------------------------------------------------------
// Functions
// --------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------
// Pad
// --------------------------------------------------------------------------------------
function pad(s) { return (s < 10) ? '0' + s : s; }

//--------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------
// Archive record
// --------------------------------------------------------------------------------------
function archiveRecord(uid) {
    log.info('Starting archive backoffice record {}', uid);
    
    //-->1-Download
    fullUrl = _CONFIG_.get('forms.backoffice.private.url') + '/v1/Record/code/' + uid + '/file';
    
    try {        
        var downloadResult = nash.service.request(fullUrl) //
            .connectionTimeout(50000) //
            .receiveTimeout(50000) //
            .header('X-Roles', rights) //
            .accept('application/zip') //
            .get();
    } catch (error) {
        log.error('=> Error occured while calling nash : ' + error);
        return;
    };

    // test the 200 code
    if (downloadResult == null || downloadResult.getStatus() != 200) {
        log.info('Downloading record {} with failure', uid);
        return;
    }
    
    log.info('Downloading backoffice record {} with success', uid);
    var resourceAsBytes = downloadResult.asBytes();
    
    //-->2-Archive into storage partners tray
    var request = nash.service.request('${storage.gp.url.ws.private}/v1/tray/{storageTray}/file', 'partners');    
    var metasResponse = nash.service.request('${forms.backoffice.private.url}/v1/Meta', uid)  //
        .param('filters', 'uid:' + uid) //
        .connectionTimeout(10000) //
        .receiveTimeout(10000) //
        .continueOnError(true) //
        .get() //
        ;
        
    if (null == metasResponse || metasResponse.status != 200) {
        log.error('Impossible de récupérer les métadonnées associées au dossier');
        return;
    }
    metasResponse.asObject() //
        .content.forEach(function (meta) {
            request.param('metas', meta.key + ':' + meta.value);
        });
    log.info('Get meta record {} successfully', uid);
    
    request.param('metas', 'message:Le dossier a été déplacé pour être archivé dans Storage');
    var created = new Date();
    request.param('metas', 'created:' + created.getFullYear() + '-' + pad(created.getMonth() + 1) + '-' + pad(created.getDate()) );
    
    var storeResponse = request.param('storageFilename', uid + '.zip') //
        .param('storageReferenceId', uid) //
        .connectionTimeout(10000) //
        .receiveTimeout(10000) //
        .dataType('form') //
        .post({ 'storageUploadFile' : resourceAsBytes }) //
        ;
    
    if (null == storeResponse || (storeResponse.status != 200 && storeResponse.status != 204)) {
        log.error('Une erreur technique est survenue lors du transfert du dossier dans STORAGE');
        return;
    }
    log.info('Store into partners tray successfully');

    //-->3-Remove record from backoffice
    try {        
        var removeRecordResult = nash.service.request('${forms.backoffice.private.url}/v1/Record/code/{code}', uid) //
            .header('X-Roles', rights) //
            .connectionTimeout(50000) //
            .receiveTimeout(50000)
            .delete();
        log.debug('Remove partner record {} successfully', uid);
    } catch (error) {
        log.error('=> Error occured while calling nash : ' + error);
        return;
    };
        
    log.info('Archiving backoffice record {} with success', uid);
    return;
};
// --------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------
// Main method
// --------------------------------------------------------------------------------------
log.info('Archive record process');
log.info("_INPUT_ : {}", _INPUT_);

if ( null == _INPUT_.record.results.nashRecords.record ) {
    return;
}

var rights = '{"GE" : ["referent", "user"]}';
var records = _INPUT_.record.results.nashRecords.record;
var uid = null;
for (var idx in records) {
    var record = records[idx];
    log.info('Checking record {}', record.code);
    if (null != record.code && null != record.archiveButton && null != record.archiveButton.value && record.archiveButton.value == 'yes') {
        uid = record.code;
        archiveRecord(uid);
        break;
    }
}
