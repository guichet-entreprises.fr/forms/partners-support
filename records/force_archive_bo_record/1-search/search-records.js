// --------------------------------------------------------------------------------------
// Functions
// --------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------
// Pad
// --------------------------------------------------------------------------------------
function pad(s) {
    return (s < 10) ? '0' + s : s;
}

//--------------------------------------------------------------------------------------
// Date to string
//--------------------------------------------------------------------------------------
function dateToString(src) {
    var dt = new Date(parseInt(src));
    return dt.getFullYear() + '-' + pad((dt.getMonth() + 1).toString()) + '-' + pad(dt.getDate().toString()) + ' ' + pad(dt.getHours().toString()) + ':' + pad(dt.getMinutes().toString()) + ':' + pad(dt.getMinutes().toString()) + ':' + pad(dt.getSeconds().toString());
}

var uid = _INPUT_.record.search.uid;
var dateSearch = _INPUT_.record.search.date;
var authority = _INPUT_.record.search.authority;

// --------------------------------------------------------------------------------------
// Find the records
// --------------------------------------------------------------------------------------
function findRecords(uid, dateSearch, authority) {
    //-->Filters
    var filters = {
        'uid' : (null != uid) ? uid : null,
        'author' : (null != authority && null != authority.getId()) ? authority.getId() : null,
        'created>=' : (null != dateSearch && null != dateSearch.from && "" !== dateSearch.from) ? dateSearch.from : null,
        'created<=' : (null != dateSearch && null != dateSearch.to && "" !== dateSearch.to) ? dateSearch.to : null
    }
    
    var queryFilter = [];
    ['uid', 'author'].forEach(function (filter) {
        if (null != filters[filter] && "" !== filters[filter]) {
            queryFilter.push(filter + ':' + filters[filter]);
        }
    });
    
    ['created>=', 'created<='].forEach(function (filter) {
        if (null != filters[filter] && "" !== filters[filter]) {
            
            var dateTemp = new Date(parseInt((filters[filter]).getTimeInMillis()));
            var year = dateTemp.getFullYear();
            var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
            var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
            var date = year + "-" + month + "-" + day ; 

            queryFilter.push(filter + date);
        }
    });
    
    //-->Full text search
    fullUrl = '${forms.backoffice.private.url}/v1/Record';
    
    try {        
        var nashRequest = nash.service.request(fullUrl) //
            .param('startIndex', 0) //
            .param('maxResults', 100) //
            ;
        
        log.info('queryFilter : {}', JSON.stringify(queryFilter));
        queryFilter.forEach(function (filter) {
            nashRequest.param('filters', filter) //
        });

        nashRequest.param('filters', 'stepId:archived');
        nashRequest.param('filters', 'stepStatus:done');
        
        var searchResult = nashRequest //
            .connectionTimeout(10000) //
            .receiveTimeout(10000) //
            .param('orders', 'created:desc') //
            .param('orders', 'uid:desc') //
            .get() //
    } catch (error) {
        log.error('=> Error occured while calling backoffice-forms : ' + error);
        return null;
    };

    // test the 200 code
    if (searchResult == null || searchResult.getStatus() != 200) {
        return null;
    }

    return searchResult.asObject();
};
// --------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
// Main method
//--------------------------------------------------------------------------------------
var display = nash.instance.load("display-search.xml");

var nashRecords = findRecords(uid, dateSearch, authority);

if (null == nashRecords) {
    display.bind("record.results.nashRecords", { "nbRecords": "0" });
    display.bind("record.results.nashRecords", { "record": [] });
    return; 
}

nashRecords.content.forEach(function (record) {
    record['created'] = dateToString(record.created);
    record['updated'] = dateToString(record.updated);
});
display.bind("record.results.nashRecords", { "nbRecords": nashRecords.totalResults });
display.bind("record.results.nashRecords", { "record": nashRecords.content });