
// --------------------------------------------------------------------------------------
// FUNCTIONS
// --------------------------------------------------------------------------------------
function getDateFormat(d){
    var date = ("0" + d.getDate()).slice(-2) + "/" + ("0" + (d.getMonth()+1)).slice(-2) + "/" + d.getFullYear() 
        + " à " + ("0" + d.getHours()).slice(-2) + "h" + ("0" + d.getMinutes()).slice(-2) + ":" + ("0" + d.getSeconds()).slice(-2);
    return date;
}

function convertStringToByte(str){
    var bytes = []; // char codes
    for (var i = 0; i < str.length; ++i) {
        var code = str.charCodeAt(i);
        bytes = bytes.concat([code & 0xff, code / 256 >>> 0]);
    }
    return bytes;
}


function getTrackerMessages(ref) {
	log.info('Get tracker messages from ref {}', ref)
	try {
		return nash.service.request('${tracker.baseUrl}/v1/uid/{uid}', ref).connectionTimeout(10000).receiveTimeout(10000).accept('json').get().asObject().messages;
	} catch (error) {
		log.error("Cannot get messages from reference {} with stack trace : {}", ref, error);
	}
	return [];
}

function getNumLiasse(uid) {
	try {
		var references = nash.service.request('${tracker.baseUrl}/v1/uid/{uid}/refs', uid).connectionTimeout(10000).receiveTimeout(10000).accept('json').get().asList();
		var liasses = [];
		for (idx = 0; idx < references.size(); idx++) {
			if (references.get(idx).startsWith("H10000")) {
				liasses.push(references.get(idx));
			}
		}		
		return liasses.reverse()[0] || "";
	} catch (error) {
		log.error("Cannot get references from uid {} with stack trace : {}", uid, error);
	}
	return "";
}


// --------------------------------------------------------------------------------------
// START
// --------------------------------------------------------------------------------------
if(_input.messages.search.reference.year == null || _input.messages.search.reference.month == null || _input.messages.search.idDossier == null) return;

var content = _input.messages.search.idDossier.split(/\r?\n/);
log.info("Record identifiers : {}", content);

var i;
var result = [];
var response = "";        
var messagesTracker = "";

var label = [
    "ID dossier",
    "eddie",
    "trasnmis",
    "accepté/rejeté",
    "commentaire",
];

var labelEddie = "Un dossier a été déposé dans Eddie à l\'autorité compétente";
var output = "";
var outputCSV = "";

// Add the global count
output += '<div class="table-responsive"><table  class="table table-striped text-center"><thead><tr>';
for each (var col in label) {
    output += "<th>" + col + "</th>";
}
for each (var col in label) {
    outputCSV += col + ";";
}
output += "</tr></thead>";
outputCSV += "\n";

//-->Load data step file
var display = nash.instance.load("display.xml");

//->Searching for GREFFE returning messages (info and errors)
var trackerReturnReference = _input.messages.search.reference.year + "-" + _input.messages.search.reference.month + ".SPT";
var trackerReturnMessages = getTrackerMessages(trackerReturnReference);

if (trackerReturnMessages.length == 0) {
    display.bind("messages.result", { "message": "Cannot get messages from reference " + trackerReturnReference });
}

log.info('trackerReturnMessages : {}', trackerReturnMessages);
					
for (i=0 ; i < content.length; i++){
    if (content[i] === "") {
        continue;
    }
	var recordUid = content[i];
    try {
        log.info("Analyzing return messages for record : {}", recordUid);
		var trackerRecordMessages = getTrackerMessages(recordUid);
		log.info('trackerRecordMessages : {}', trackerRecordMessages);
        output += "<tr>";
        
        var transmise = false;
        var acceptee = false;
        var rejetee = false;
        var eddie = false;
        var eddieMsg = [];
        var accepteeDate;
        var transmiseDate;
        var rejeteeDate;
        var rejeteeMsg = "";
        var comment = "";
        
		for (var message in trackerRecordMessages) {
			//-->Checking if the record has been transferred using EDDIE channel
            if ( trackerRecordMessages[message].content.indexOf(labelEddie) !== -1){
                eddie = true;
                eddieMsg.push(trackerRecordMessages[message].content.substring(labelEddie.length()));
            }
			//<--
		}
		
        for (var element in  trackerReturnMessages) {
			//-->Checking GREFFE returning messages
            if ( trackerReturnMessages[element].content.indexOf("[CRE_DOS_TRANSMISE] [" + recordUid + "]") !== -1){
				transmise = true;
				var date = new Date(parseInt(trackerReturnMessages[element].created));
                transmiseDate = getDateFormat(date);
			}
            if ( trackerReturnMessages[element].content.indexOf("[CRE_DOS_ACCEPTEE_SES] [" + recordUid + "]") !== -1){
                acceptee = true;
                var date = new Date(parseInt(trackerReturnMessages[element].created));
                accepteeDate = getDateFormat(date);
				comment = getNumLiasse(recordUid);
            }
            if ( trackerReturnMessages[element].content.indexOf("[CRE_DOS_REJETEE_SES] [" + recordUid + "]") !== -1){
                rejetee = true;
                rejeteeDate = getDateFormat(new Date(parseInt(trackerReturnMessages[element].created)));
				rejeteeMsg = trackerReturnMessages[element].content;
				comment = getNumLiasse(recordUid);
            }
			//<--
        }
        
        output += "<td>" + recordUid + "</td>";
        outputCSV += recordUid + ";";
        
        //numéro de dossier
        if (eddie) {
            output += "<td>" + eddieMsg.join(' | ') + "</td>";
            outputCSV += eddieMsg.join(' | ') + ";";
        } else{
            output += "<td>NON</td>";
            outputCSV += "NON;";
        }
        
        //transmis ou non
        if (transmise) {
            output += "<td>OUI le " + transmiseDate + " </td>";
            outputCSV += "OUI le " + transmiseDate + ";";
        } else {
            output += "<td>NON</td>";
            outputCSV += "NON;";
        }
        
        //accépté ou rejeté par le SES partenaire
        if (acceptee) {
            output += "<td color bgcolor=\"#00FF00\"> accepté le " + accepteeDate + "</td>";
            outputCSV += "accepté le " + accepteeDate + ";";
        } else if (rejetee) {
            output += "<td bgcolor=\"#FF0000\"> rejeté le " + rejeteeDate + " : " + rejeteeMsg +"</td>";
            outputCSV += "rejeté le " + rejeteeDate + " : " + rejeteeMsg +";";
        } else {
            output += "<td></td>";
            outputCSV += ";";
        }
        
        output += "<td>" + comment + "</td>";
        outputCSV += "\"" + comment + "\";";

        output += "</tr>";
        outputCSV += "\n";
    } catch (error) {
        log.error("erreur lors de la formalité de vérification des messages tracker SES : {}", error);
        //Response
        display.bind("messages.result", { "message": error });
		output += "<tr><td>" + recordUid + "</td></tr>";
        outputCSV += recordUid + "\n";
    }
}
output+="</table></div>";

display.bind("messages.result", { "data": output });

//création du fichier CSV et ajout dans les metas pour le bouton de telechargement. 
// Le lien pointe vers le webservice nash "/document.zip" qui ne télécharge que les documents renseignés dans les metas
nash.record.saveFile("extract.csv", convertStringToByte(outputCSV));

if (nash.record.meta() == null) {
	nash.record.meta([{'name':'document', 'value': '/extract.csv'}]);
}






