log.debug('Hanging payment pre-process');

//--------------- FUNCTIONS -------------------//
function updateMetas(proxyResult) {
	if (null == proxyResult || null == proxyResult.files) { return; }
	
	var metas = [];
	var proxyFiles = proxyResult.files;
	for(var i=0; i<proxyResult.files.size(); i++){
		if (!proxyResult.files.get(i).getLabel().endsWith(".pdf")) {
			proxyFiles.remove(i);
		} else {
			metas.push({'name':'document', 'value': '/'+proxyFiles.get(i).getAbsolutePath()});
		}
	}
	//-->Ajout du recu du paiement dans les méta
	if (metas.length > 0) {
		nash.record.meta(metas);
	}
	return proxyFiles;
}

function specCreateSuccessData(proxyResult) {
	return spec.create({
		id: 'review',
		label: "Règlement des frais liés au traitement de votre dossier",
		groups: [spec.createGroup({
			id: 'proxyResult',
			label: "Règlement des frais liés au traitement de votre dossier",
			description: "Votre paiement a bien été pris en compte. Votre dossier va être transmis au(x) service(s) compétent(s).",
			data: [
				spec.createData({
					id: 'files',
					description:'Le justificatif de votre paiement ci-dessous est téléchargeable.',
					type: 'FileReadOnly',
					value:  updateMetas(proxyResult)
				})
			]
		})]
	});
}

function specCreateReplayData() {
	return spec.create({
		id: 'review',
		label: "Règlement des frais liés au traitement de votre dossier",
		groups: [spec.createGroup({
			id: 'generatedFile',
			label: "Règlement des frais liés au traitement de votre dossier",
			description: "Pour effectuer une nouvelle tentative de paiement ci-dessous, veuillez cliquer sur le bouton « Etape suivante » ci-dessous.",
			data: [	]
		})]
	});
}

function specCreateCriticalData(proxyResult) {		
	return spec.create({
		id: 'review',
		label: "Règlement des frais liés au traitement de votre dossier",
		groups: [spec.createGroup({
			id: 'proxyResult',
			label: "Règlement des frais liés au traitement de votre dossier",
			description: "Votre dossier requiert une intervention de l'équipe Guichet Entreprises. Une alerte est transmise à notre équipe d'intervention. Votre dossier est mis en attente pour le moment. Vous recevrez prochainement un courriel vous informant de la résolution du problème.",
			data: [
				spec.createData({
					id: 'files',
					label : 'Échec de la tentative de paiement',
					description : "Le justificatif de votre paiement ci-dessous est téléchargeable.",
					type: 'FileReadOnly',
					value:  ( (null != proxyResult && null != proxyResult.files) ? updateMetas(proxyResult) : null )
				})
			]
		})]
	});
}

function specCreateBackToDashboardData(proxyResult) {	
	return spec.create({
		id: 'review',
		label: "Règlement des frais liés au traitement de votre dossier",
		groups: [spec.createGroup({
			id: 'proxyResult',
			label: "Règlement des frais liés au traitement de votre dossier",
			data: [
				spec.createData({
					id: 'files',
					label : 'Résultat de la tentative de paiement',
					description : "Une erreur est survenue lors du règlement des frais associés au dossier. Vous allez être redirigé vers votre tableau de bord.",
					type: 'FileReadOnly',
					value:  ( (null != proxyResult && null != proxyResult.files) ? updateMetas(proxyResult) : null )
				})
			]
		})]
	});
}
//--------------- FUNCTIONS -------------------//


//--------------- Variables init --------------//
var proxyResult = _INPUT_.proxyResult;
//--------------- Variables init --------------//
log.debug('Proxy result response : {}', proxyResult);

if (proxyResult.status == 'SUCCESS') {
	return specCreateSuccessData(proxyResult);
	
} else if (proxyResult.status == 'TECHNICAL' || proxyResult.status == 'FRAUD') {
	return specCreateReplayData();
	
} else if (proxyResult.status == 'CRITICAL') {
	return specCreateCriticalData(proxyResult);
	
} else {
	return specCreateBackToDashboardData(proxyResult);
}
