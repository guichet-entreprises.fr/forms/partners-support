var cart = _INPUT_.record.cart;
log.info("input cart is {}", cart);

var itemsToPay = [];

for (var idx = 0; idx < cart.items.size(); idx++) {
	var elm = cart.items[idx];
	var item = {  
		"name":nash.util.generateUid(),
		"price": elm.price,
		"description":elm.description,
		"partnerLabel" : elm.partnerLabel,
		"ediCode" : elm.ediCode,
		"errorCodeTest" : '00000' != elm.errorCodeTest ? elm.errorCodeTest : null
	};
	log.info("item is {}", item);
	itemsToPay.push(item);
}

var cartToPay = {    
	"recordUid" : nash.record.description().recordUid,  
	"email" : cart.email,
	"userCivility": cart.userCivility,
	"userLastName": cart.userLastName,
	"userFirstName": cart.userFirstName,
	"description": nash.record.description().title,
	"paymentType": 'DB',            
	"reference": nash.record.description().recordUid,
	"items":itemsToPay	        
};
		
var nfo = nash.hangout.pay({'cart' : JSON.stringify(cartToPay)});

return nfo;