_log.info("===> preprocess list forms");

//prepare info to send 

var target = _CONFIG_.get($searchListForms.infoForms.target.getId());
var targetLabel = $searchListForms.infoForms.target.getLabel();
var tagged;


if($searchListForms.infoForms.tagged.id == "publishOnly"){
	tagged = "true";
}else{
	tagged = "false";
}

// call forge service to find the list 
var response = nash.service.request(target + '/v1/Specification?maxResults%3D10000&filters%3Dtagged%3A{tagged}', tagged) //
        .connectionTimeout(60000)
        .receiveTimeout(60000)
        .get();
_log.info("response  {}", response);

// Result value of functional id 
_log.info("listFormsJson {}",listFormsJson);
var listFormsJson = response.asString();
var responseJson = JSON.parse(listFormsJson);
var listForms = "";

for (i = 0; i < responseJson.content.length; i++){
	listForms = listForms + responseJson.content[i].code + ";"  + responseJson.content[i].title + ";" + responseJson.content[i].reference + "\n";
} 
var nbForms = responseJson.content.length;
// load data.xml
var data = nash.instance.load("data.xml");

var html = createDataTable(responseJson.content);
// set value inside data.xml
data.bind("request",{
    input:{
        tagged:$searchListForms.infoForms.tagged,
        target:targetLabel + " (" + target + ")"
    },
    output:{
		nbForms:nbForms,
        listForms:listForms,
		resultHtml:html,
        resultJson:listFormsJson
    }
});

function createDataTable(content) {
	var table = '<table class="table table-stripped table-condensed table-hover dataTable no-footer">'
								+ '<thead>'
									+ '<tr role="row">'
										+ '<th class="name" tabindex="0" rowspan="1" colspan="1">nom</th>'
										+ '<th class="name" tabindex="0" rowspan="1" colspan="1">reference-id</th>'
										+ '<th class="name" tabindex="0" rowspan="1" colspan="1">Code</th>'
										+ '<th class="date" tabindex="0" rowspan="1" colspan="1">publié</th>'
									+ '</tr>'
								+ '</thead>'
								+ '<tbody>';
	for(i = 0; i < content.length; i++){
		table = table + '<tr>'
				+ '<td>'+ content[i].title +'</td>'
				+ '<td width="100%">'+ content[i].reference +'</td>'
				+ '<td style="white-space: nowrap;">'+ content[i].code +'</td>'
				+ '<td>'+ content[i].tagged +'</td>'
				+ '</tr>';
	}
	table = table + '</tbody>' + '</table>';
	return table;
}
