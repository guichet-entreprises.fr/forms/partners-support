
//--------------- prepare info  before import the file -------------------------
_log.info(" _INPUT_ is {}", _INPUT_);

var content = $verifUser.dataGroup.list.split(/\r?\n/);
_log.info("list des users : {}", content);

var i;
var result = [];
var response = "";
for(i=0 ; i < content.length; i++){
	try{
		_log.info("ListUser appel Account : {}", content[i]);
		response = nash.service.request('${account.baseUrl.read}/private/users/{tracker}', content[i]) //
			.dataType('application/json') //
			.accept('json')
			.get();	
		if (response != null and response.getStatus() == 200) {
			res = response.asObject();
			if(res.errorCode == 1005){
				result.push(content[i]);
			}else{
				result.push(res.trackerId + ";" + res.email);
			}
		}else{
			result.push(null);
		}
	}catch(error) {
		_log.info("erreur lors de la formalité de vérification des user Account : {}", error);
		//Response
		return spec.create({ 
			id : 'ConfirmSend',
			label : 'Résultat de la demande',
			groups : [ 
				spec.createGroup({
					id : 'group',
					label : 'Statut',
					warnings : [ 
						{ id : 'warningConfiguration', label : "Une erreur technique est survenue. " + error + " \ncontent :" + content}
					]
				}) 
			]
		});
	}
}
_log.info("ListUser resultat : {}", result);
return spec.create({ id : 'userData', groups : [
	spec.createGroup({ id : 'accountParameters', label : 'Paramètres des comptes', data : [
		spec.createData({ id : 'result', label : 'résultat', type: 'Text', value: result.join("\n")})
	]})
]});
