
var messagesErrors = [];

var display = nash.instance.load("display.xml");

function getDateFormat(d){
	var date = ("0" + d.getDate()).slice(-2) + "/" + ("0" + (d.getMonth()+1)).slice(-2) + "/" + d.getFullYear() 
		+ " à " + ("0" + d.getHours()).slice(-2) + "h" + ("0" + d.getMinutes()).slice(-2) + ":" + ("0" + d.getSeconds()).slice(-2);
	return date;
}
function getDateFormatShort(d){
	var date = ("0" + d.getDate()).slice(-2) + "/" + ("0" + (d.getMonth()+1)).slice(-2) + "/" + d.getFullYear() ;
	return date;
}

function convertStringToByte(str){
	
_log.info("convertStringToByte begin : {}", new Date());
	var bytes = []; // char codes
	for (var i = 0; i < str.length; ++i) {
		var code = str.charCodeAt(i);
		bytes = bytes.concat([code & 0xff, code / 256 >>> 0]);
	}
	
_log.info("convertStringToByte end : {}", new Date());
	return bytes;
}

function searchNumLiasse(responseMessage){
	var result = "";
	for(var element in  responseMessage) {
		if(responseMessage[element].uid.startsWith("H10000")){
			result = responseMessage[element].uid;
		}
	}
	return result;
}

_log.info(" _INPUT_ is {}", _INPUT_);

var inputDate = _input.messages.search.date;

if(inputDate == null){
	var today = new Date();
	var twoWeeksBefore = new Date();
	twoWeeksBefore.setDate(today.getDate() - 14);
	var date = {'from': getDateFormatShort(twoWeeksBefore), 'to': getDateFormatShort(today)};

	display.bind("messages.search", { "date": date });
	return;
} 

_log.info("d'ate d'effet : {}", inputDate);

var i;	

var label = [
	"ID",
	"message",
	"date",
];


var output = "";
var outputCSV = "";

// Add the global count
output += '<div class="table-responsive"><table  class="table table-striped text-center"><thead><tr>';
for each (var col in label) {
	output += "<th>" + col + "</th>";
}
for each (var col in label) {
	outputCSV += col + ";";
}
output += "</tr></thead>";
outputCSV += "\n";

//on ajoute un jour a la date de fin (bug du jour à 0h00) pour inclure le dernier jour dans la recherche

var dateTo = new Date(parseInt( inputDate.to.getTimeInMillis()));
dateTo.setDate(dateTo.getDate() + 1);
//search for tracker messages errors

try{
	_log.info("label appel tracker : 2020-02.SPT.ERROR");
	//var url = '${tracker.baseUrl}/v1/uid/' + content[i];
	var url = "${tracker.baseUrl}/v1/uid/2020-02.SPT.ERROR";
	
	log.info('using tracker URL : {}', url);
			var response = nash.service.request(url) //
				.connectionTimeout(10000) //
				.receiveTimeout(10000)
				.accept('json')
				.get();
				
	var responseMessage =  response.asObject().messages;

	log.info('messages length : {}', responseMessage.length);
	for(var element in  responseMessage) {

		if(responseMessage[element].created < inputDate.from.getTimeInMillis()
		||  responseMessage[element].created > dateTo) continue;

		var numLiasse = "";
		var regex = /C\d{4}A\d{4}L(\d{6})D\d{8}H.*?\./gm;
		var match = regex.exec(responseMessage[element].content);
		if( match != null && match.length == 2){
			numLiasse = "H10000" + match[1]
		}

		output += "<tr>";
		output += "<td><a target=\"_blank\" href=\"http://support.guichet-partenaires.fr/tracker_detail.php?uid=" + numLiasse + "\">" + numLiasse + "</a></td>" 
		output += "<td>" + responseMessage[element].content.replace(/\n/gi, '<br>') + "</td>";
		output += "<td>" + getDateFormat(new Date(parseInt(responseMessage[element].created)))+ "</td>";

		output += "</tr>";
		outputCSV += "\n";
	}
	
}catch(error) {
	_log.info("erreur lors de la formalité de vérification des user Account : {}", error);
	//Response
	display.bind("messages.result", { "message": error });
}

output+="</table></div>";

display.bind("messages.result", { "data": output });

//création du fichier CSV et ajout dans les metas pour le bouton de telechargement. 
// Le lien pointe vers le webservice nash "/document.zip" qui ne télécharge que les documents renseignés dans les metas
//nash.record.saveFile("extract.csv", convertStringToByte(outputCSV));

//var metas = [];
//metas.push({'name':'document', 'value': '/extract.csv'});
//nash.record.meta(metas);
