# Description of testing payment :

The goal is to test payment functionality.

# Authorities concerned :
* *GE*
	* The admin team

## First step : Searching for an user 

Search a Guichet-Entreprises user by its identifier or its email address.

## Second step : Building a cart

Build a cart to one or multiple authorities.
This step allow you to simulate different types of payment return. 

## Third step : Creating a record

This step will create a record with the cart built to the Guichet-Entreprises user found previously.
This record will be accessed from the user's dashboard.