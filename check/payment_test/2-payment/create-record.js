log.debug('_INPUT_ : {}', _INPUT_);

var recordUid = nash.record.description().recordUid;
var recordTitle = nash.record.description().title;

var items = [];

for (var idxVad = 0; idxVad < _INPUT_.record.cart.vad.size(); idxVad++) {
	var vad = _INPUT_.record.cart.vad[idxVad];

	for (var idxItem = 0; idxItem < vad.item.size(); idxItem++) {
		var elm = vad.item[idxItem];
		var item = {
			"name": nash.util.generateUid(),
			"price": elm.price.amount,
			"description":elm.description,
			"partnerLabel" : vad.authority.label,
			"ediCode" : vad.authority.id,
			"errorCodeTest" : vad.errorCodeTest.getId()
		};
		items.push(item);
		log.info("Push item for authority {} : {}", vad.authority.label, item);
	}
}

_log.info("items is {}", items);

var user = _INPUT_.record.results.user;

var cart = {    
	"recordUid" : recordUid,  
	"email" : user.email,
	"userCivility": user.civility,
	"userLastName": user.lastName,
	"userFirstName": user.firstName,
	"description": nash.record.description().title,
	"paymentType":"DB",
	"items":items,	                    
	"reference": recordUid
};

_log.info("cart is {}", cart);

//-->Create record for the target user
var recordNash = nash.instance.from('${forms.support.private.url}').to('${forms.private.url}').createRecord('ref:Support/Paiement/Simuler un paiement');
recordNash.description.setAuthor(user.trackerId);
recordNash.description.setTitle(recordTitle);
recordNash.description.setFormUid(recordUid);
//<--

// Binding the 1st step
recordNash.load('1-cart/data.xml').bind("record", { "cart" : cart });

var data = nash.instance.load("display.xml");
try {
	var recordNashUid = recordNash.save();
	log.info('User record identifier {}', recordNashUid);
    data.bind("request", {
        "output" : {
             'url' : _CONFIG_.get('forms.private.url'),
             'email' : user.email,
             'recordUid' : recordNashUid,
             'message' : "Félicitations !! Le dossier a été crée avec succès !! L'utilisateur pourra alors effectuer un paiement depuis son tableau de bord."
        }
    });
} catch (error) {
    log.error(error);
    data.bind("request", {
        "output" : {
             'url' : _CONFIG_.get('forms.private.url'),
             'email' : user.email,
             'message' : "Oups !! Une erreur technique est survenue lors de la création du dossier à l'utilsateur " + user.email
        }
    });
}