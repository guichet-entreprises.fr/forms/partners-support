// try to find the id to search
var idOrEmail = _INPUT_.record.input.idOrEmail;

// Test if a search is needed
if ((idOrEmail == null) || (idOrEmail === "")) {
	return;
};

// --------------------------------------------------------------------------------------
// Find the user
// --------------------------------------------------------------------------------------
function findUser(idOrEmail, xmlDisplay, resultsXmlPath) {
	fullUrl = _CONFIG_.get('account.ge.baseUrl.read') + '/private/users/' + idOrEmail;

	// perform the call
	searchResult = null;
	try {
		searchResult = nash.service.request(fullUrl) //
			.dataType('application/json') //
			.accept('json')
			.get();

	} catch (error) {
		log.info('=> Error occured while calling account : ' + error);
		return null;
	};

	return searchResult.asObject();
};
// --------------------------------------------------------------------------------------


// --------------------------------------------------------------------------------------
// Bind the user
// --------------------------------------------------------------------------------------
function bindUser(xmlDisplay, xmlPath, userData) {
	xmlDisplay.bind(xmlPath, {
		"firstName": userData['firstName'],
		"lastName": userData['lastName'],
		"civility": userData['civility'],
		"email": userData['email'],
		"trackerId": userData['trackerId'],
		"connectionDate": userData['connectionDate'],
	});
};
// --------------------------------------------------------------------------------------

// Load the screen for the user
var display = nash.instance.load("display-search.xml");

var userFound = findUser(idOrEmail, display);

if (null == userFound) { return; };

// We haver a user
bindUser(display, "record.results.user", userFound);
