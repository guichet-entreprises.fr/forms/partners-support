var partnerId = _INPUT_.authorityData.partnerID;
var partnerLabel = _INPUT_.authorityData.labelFunc;
var authorityLabel = _INPUT_.authorityData.nomAuth;
var amountToPay = _INPUT_.authorityData.amountToPay;
var description = nash.record.description().title; 
var recordUid = nash.record.description().recordUid; 
_log.info("partnerId is {}", partnerId);
_log.info("partnerLabel is {}", partnerLabel);
_log.info("description is {}", description);
_log.info("recordUid is {}", recordUid);

var paymentDescription = "Paiement du support Guichet-Entreprises"; 
_log.info("paymentDescription is {}", paymentDescription);
var paymentName = "PYMSUPP - "+recordUid;
_log.info("paymentName is {}", paymentName);

//Récupérer les informations de l'utilisateur connecté

var userResponse = nash.service.request('${account.ge.baseUrl.read}/private/users/{id}', nash.record.description().author) //
.accept('text') //
.get();

var user = userResponse.asObject();
_log.info("user  is {}", user);

var email = user.email;

var cart = {    

				"recordUid" : recordUid,  
				"dossierId" : recordUid,
				"email" : user.email,
				"userCivility": "MR",
				"userLastName": "Guichet-Entreprises",
				"userFirstName": "Support",
				"description": "Paiement vers une autorite",
				"paymentType":"DB",
				"items":[
					{
						"name":paymentName,
						"price": amountToPay,
						"description":paymentDescription,
						"ediCode" : partnerId,
						"partnerLabel":	partnerLabel					
					}],	                    
				"reference": recordUid
		};

_log.info("cart is {}", cart);
		
var nfo = nash.hangout.pay({'cart' : JSON.stringify(cart)});

return nfo;
/* }catch(error){
		_log.info("une erreur est survenue lors de l'envoi du reçu de paiement");
} */