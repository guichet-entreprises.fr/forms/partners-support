var recordUid = nash.record.description().recordUid;

_log.info("************INPUT : {}************", _INPUT_);
	
_log.info("************Fromalité payante!************");	
// check inout information
var proxyResult = _INPUT_.proxyResult;
_log.info("proxyResult is {}", proxyResult);
_log.info("proxyResult status  is {}", proxyResult.status);	
	


var nameToCheck = recordUid+"_recu.pdf";
	_log.info("nameToCheck is {}", nameToCheck);
	var fileExists = false ;
	for(var i=0; i<proxyResult.files.length; i++){
	var indice = i+1;
	_log.info("label is {}", proxyResult.files[i].label);
	_log.info("proxyResult {} element is {}", i, proxyResult.files[i]);
		if(proxyResult.files[i].label.indexOf(nameToCheck)!=-1){
			var	fileName = "proxyResult.files-"+indice+"-"+proxyResult.files[i].label;
			fileExists = true ;
		}
	}
	_log.info("file name is {}", fileName);
	
	_log.info("Sending Mail....");
	var userResponse = nash.service.request('${account.ge.baseUrl.read}/private/users/{id}', nash.record.description().author).accept('text').get();
	var user = userResponse.asObject();
	_log.info("user object is {}", user);
	
	if(user != null && user.email !=null){
		var sendMail = true;
	}else{
		var sendMail = false;
	}
	_log.info("Send mail --> {}", sendMail);
	
	// Paramétrage du mail 
	if (sendMail == true && fileExists) {
		try{
			_log.info("fileName sendMail is {}", fileName);
			var exchangeSender = _CONFIG_.get('exchange.email.sender');
			_log.info("exchangeSender is {}", exchangeSender);
			var emailTo = user.email;
			_log.info("sending mail to{}", emailTo);
			var prenomDestinataire = user.firstName;
			var nomDestinataire = user.lastName;
			var civiliteDestinataire = user.civility;
				if (prenomDestinataire == null) {prenomDestinataire="";}
			_log.info("prenomDestinataire is {} ", prenomDestinataire);
			if (nomDestinataire == null) {nomDestinataire="";}
			_log.info("nomDestinataire is {} ", nomDestinataire);
			if (civiliteDestinataire == null) {civiliteDestinataire="";}
			_log.info("civiliteDestinataire is {} ", civiliteDestinataire);
			var emailObject = "Reçu de paiement pour le dossier: "+recordUid;
			var attachementFileName="Recu de paiement.pdf"
			var attachement= "/5-payment/generated/"+fileName;
			_log.info("attachement is {}", attachement);
			var emailContent ="<html> <body style='font-family: Times New Roman,serif;'> <div class='head'> <img style='display: block; margin: 0 auto;' src='https://account.guichet-entreprises.fr/assets/images/guichet-entreprises/logo/long-base.png'> </div> <div class='content' style='color: #555555; width: 900px; margin: 0 auto;'> <div class='dear' style='padding-left: 40px; padding-bottom: 20px;text-transform: capitalize;'>Bonjour "+civiliteDestinataire+" "+prenomDestinataire+" "+nomDestinataire+"</div> <div class='message' style='padding-left: 40px; text-indent: 20px; line-height: 2em;'>Nous vous confirmons la prise en compte de votre paiement sur <a href='https://www.guichet-entreprises.fr/'>www.guichet-entreprises.fr</a>, dans le cadre de votre dossier "+recordUid+". Vous en trouverez le reçu en pièce jointe de ce courriel.</br></br>Nous vous remercions de votre confiance,</br></br>L’équipe Guichet Entreprises </div> </div> </body> </html>";
			// call exchange service to send mail with pj
			_log.info("send mail .......");
			var response = nash.service.request('${exchange.baseUrl}/email/send') //
				.connectionTimeout(2000) //
				.receiveTimeout(5000) //
				.dataType("form") //
				.param("sender", [ exchangeSender ]) //
				.param("recipient", [ emailTo ]) //
				.param("object", [ emailObject ]) //
				.param("content", [ emailContent ]) //
				.param("attachmentPDFName", [ attachementFileName ]) //
				.post({ "file" : nash.util.resourceFromPath(attachement) });
				_log.info("response send mail .......");
			// output to confirm
			_log.info("status is {}", response.status);
		}catch(error){
			_log.error("Error occured when trying send email to user with payment receipt {}", error);
		}
	}
			
if( /^(000.000.|000.100.1|000.[36]).*/g.test(proxyResult.status)== true || /^(000.400.0|000.400.100).*/g.test(proxyResult.status)==true || /^(000.200).*/g.test(proxyResult.status)==true || proxyResult.status == 'OK'){
	
	if (fileExists){

		var metas = [];
		var proxyFiles = proxyResult.files;
		for(var i=0; i<proxyResult.files.size(); i++){
			if (!proxyResult.files.get(i).getLabel().endsWith(".pdf")) {
				proxyFiles.remove(i);
			} else {
				metas.push({'name':'document', 'value': '/'+proxyFiles.get(i).getAbsolutePath()});
			}
		}
		//-->Ajout du recu du paiement dans les méta
		if (metas.length > 0) {
			nash.record.meta(metas);
		}
		
		return spec.create({
							id: 'review',
							label: "Génération du recu de paiement",
							groups: [spec.createGroup({
									id: 'proxyResult',
									label: "Récapitulatif de votre paiement",
									description: "Votre dossier va être transmis au service compétent.",
									data: [
										spec.createData({
											id: 'files',
											description:'Veuillez trouver ci-dessous le récapitulatif de votre paiement.',
											type: 'FileReadOnly',
											value:  proxyFiles
										})
									]
								})]
						});	
					
	}else{
		return spec.create({
						id: 'review',
						label: "Génération du recu de paiement",
						groups: [spec.createGroup({
								id: 'generatedFile',
								label: "Récapitulatif de votre paiement",
								description: "Votre dossier va être transmis au service compétent.",
								data: [	]
							})]
					});	
	}
	

}else {
	
	if (fileExists){

		var metas = [];
		var proxyFiles = proxyResult.files;
		for(var i=0; i<proxyResult.files.size(); i++){
			if (!proxyResult.files.get(i).getLabel().endsWith(".pdf")) {
				proxyFiles.remove(i);
			} else {
				metas.push({'name':'document', 'value': '/'+proxyFiles.get(i).getAbsolutePath()});
			}
		}
		//-->Ajout du recu du paiement dans les méta
		if (metas.length > 0) {
			nash.record.meta(metas);
		}
		
		return spec.create({
							id: 'review',
							label: "Génération du recu de paiement",
							groups: [spec.createGroup({
									id: 'proxyResult',
									label: "Récapitulatif de votre paiement",
									data: [
										spec.createData({
											id: 'files',
											label : 'Résultat de la tentative de paiement',
											description : "Un problème est survenu lors de la tentative du paiement, veuillez contacter votre support.",
											type: 'FileReadOnly',
											value:  proxyFiles
										})
									]
								})]
						});	
					
	}else{
		return spec.create({
						id: 'paymentResult',
						label: "Paiement en erreur",
						groups: [spec.createGroup({
								id : 'proxyResult',
								label : 'Résultat de la tentative de paiement',
								description : "Un problème est survenu lors de la tentative du paiement, veuillez contacter votre support.",
								data : []
							})]
					});	
	}
}
