//check information of receiver
var grp = _INPUT_;

_log.info("grp is {}",grp);

var destFuncId = _INPUT_.authorityData.destFuncId;
var telAuth = _INPUT_.authorityData.tel;
var nomAuth = _INPUT_.authorityData.nomAuth;
var email = _INPUT_.authorityData.email;
var amountToPay = _INPUT_.authorityData.amountToPay;


return spec.create({
		id: 'authorityData',
		label: "L'autorité compétente:",
		groups: [spec.createGroup({
				id: 'authorityData',
				label: "Rappel de l'autorité ",
				data: [spec.createData({
					    id: 'nomAuth',
					    label: "Nom de l'autorité",
					    type: 'StringReadOnly',
					    value: nomAuth
					}),spec.createData({
						id: 'email',
						label: "L'adresse courriel de l'autorité compétente",
						type: 'StringReadOnly',						
						value: email
					}),   spec.createData({
					    id: 'tel',
					    label: "téléphone de l'autorité",
					    type: 'StringReadOnly',
					    value: telAuth
				}), spec.createData({
					    id: 'amountToPay',
					    label: "Montant à payer",
					    type: 'StringReadOnly',
					    value: amountToPay
				})			
				]
			})]
	});