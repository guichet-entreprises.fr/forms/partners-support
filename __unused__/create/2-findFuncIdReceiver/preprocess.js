var grp = _INPUT_;
_log.info("grp is  {}",_INPUT_);
var codeAuthority = _INPUT_.paymentInformation.authorityValue.id;
_log.info("codeAuthority is  {}",codeAuthority);
var amountToPay = _INPUT_.paymentInformation.amount;
_log.info("amountToPay is  {}",amountToPay);

var partnerID = null;

// call directory with funcId to find all information of Authorithy
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', codeAuthority) //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .accept('json') //
	           .get();

//result			     
var receiverInfo = response.asObject();
_log.info("receiverInfo is  {}",receiverInfo);
// prepare all information of receiver to create data.xml

var funcId = !receiverInfo.entityId ? null : receiverInfo.entityId;
var funcLabel = !receiverInfo.label ? null :receiverInfo.label;
var details = !receiverInfo.details ? null : receiverInfo.details;
_log.info("details is  {}",details);
var ediCode = !details.ediCode ? null : details.ediCode;
var payment = !details.payment ? null : details.payment;
var email = !details.profile.email ? null : details.profile.email;
var tel = !details.profile.tel ? null : details.profile.tel;

_log.info("adress is  {}",details.profile.address);
_log.info("email is  {}", email);
_log.info("recipientName is  {}",details.profile.address.recipientName);
_log.info("addressNameCompl is  {}",details.profile.address.addressNameCompl);
_log.info("cityNumber is  {}",details.profile.address.cityNumber);

var nomAuth = !details.profile.address.recipientName ? null : details.profile.address.recipientName;
var authCP = !details.profile.address.cityNumber ? null : details.profile.address.cityNumber;

var authAddressNameCompl = !details.profile.address.addressNameCompl ? null : details.profile.address.addressNameCompl;
var authSpecial = !details.profile.address.special ? null : details.profile.address.special;
var authCity = !details.profile.address.cityName ? null : details.profile.address.cityName;
var postalCode = !details.profile.address.postalCode ? null : details.profile.address.postalCode;
var authAdress = authAddressNameCompl + " " + authSpecial;

var attachment = attachment ? null : attachment;

// input email of receiver

emailObject = "Nouveau dossier disponible"
emailContent = "<html>"+"Bonjour "+funcLabel +",<br\><br\>" +"Le nouveau dossier " + nash.record.description().recordUid + " est disponible sur dashboard-back-office.<br/>" +"Pour y accéder cliquer sur le lien : <a href=\"https://backoffice-dashboard.dev.guichet-partenaires.fr\">cliquez ici</a>"+"<html>"

// create data.xml content data-to-send with exchange service
_log.info("emailObject is  {}",emailObject);
_log.info("emailContent is  {}",emailContent);
return spec.create({
		id: 'email',
		label: "les informations d'une autorité compétente",
		groups: [spec.createGroup({
				id: 'authorityData',
				label: "Contenu d'une autorité compétente",
				data: [spec.createData({
						id: 'destFuncId',
						label: "Le code de l'autorité compétente",
						description: 'Code interne',
						type: 'String',
						mandatory: true,
						value: funcId
					}), spec.createData({
						id: 'labelFunc',
						label: "Le libellé fonctionnelle de l'autorité compétente",
						type: 'String',
						mandatory : true,
						value: funcLabel
					}), spec.createData({
						id: 'email',
						label: "L'adresse email de l'autorité compétente",
						type: 'email',
						mandatory:true,
						value: email
					}),	spec.createData({
					    id: 'emailPj',
					    label: "Le chemin de la pièce jointe",
					    type: 'String',
					    value: attachment
				}),   spec.createData({
					    id: 'tel',
					    label: "telephone",
					    type: 'String',
					    value: tel
				}), spec.createData({
					    id: 'nomAuth',
					    label: "Nom autorité",
					    type: 'String',
					    value: funcLabel
				}),spec.createData({
				    id: 'partnerID',
				    label: "Payment Partner ID",
				    type: 'String',
				    value: ediCode
				}),spec.createData({
				    id: 'amountToPay',
				    label: "Montant à payer",
				    type: 'String',
				    value: amountToPay
				})
				
				]
			}),
			spec.createGroup({
				id: 'emailData',
				label: "Le Contenu du message",
				data: [spec.createData({
						id: 'email',
						label: "L'adresse électronique de l'autorité compétente ",
						type: 'Email',
						mandatory: true,
						value: email
					}), spec.createData({
						id: 'title',
						label: "Description de la demande",
						type: 'String',
						value: emailObject
					}), spec.createData({
						id: 'body',
						label: "Le contenu du message",
						type: 'Text',
						value: emailContent
					}), spec.createData({
						id: 'attachment',
						label: "Le chemin de la pièce jointe",
						type: 'String',
						value: attachment
					}),spec.createData({
						id: 'destFuncId',
						label: "Code de l'autorité compétente",
						type: 'String',
						value: funcId
					}),spec.createData({
						id: 'telAuth',
						label: "telephone autorité",
						type: 'String',
						value: tel
					}) ,spec.createData({
					    id: 'nomAuth',
					    label: "Nom autorité",
					    type: 'String',
					    value: funcLabel
					}),
						spec.createData({
						id: 'partnerID',
						label: "Payment Partner ID",
						type: 'String',
						value: ediCode
					})
				]
			})]
	});