var approvedGroup = _input.data.approvedGroup.question;
var response_message='';
var numberOfRM=0;
var numberOfZoneUpdated=0;
if (Value('id').of(approvedGroup).eq('Oui')) {

	// deletion of existing rm 
	  var responseSetOfRMToDelate = nash.service.request('${directory.baseUrl}/v1/authority') //
	  .connectionTimeout(10000) //
	  .receiveTimeout(10000) //
	  .accept('json') //
	  .param('maxResults', 400)//
	  .param('filters', "details.parent:GE/RM")//
	  .get();
	
	  var responseSetOfRMToDelateObject=responseSetOfRMToDelate.asObject();
	  
	  if(responseSetOfRMToDelateObject.content.length !=0){
		  for (var k = 0 ; k < responseSetOfRMToDelateObject.content.length ; k++) {
			  
			  var removedRM = nash.service.request('${directory.baseUrl}/v1/authority/'+responseSetOfRMToDelateObject.content[k].id) //
			  .connectionTimeout(10000) //
			  .receiveTimeout(10000) //
			  .delete();
		  }
	  }
	
	  // looking for CMAs to duplicate them
	  var responseSetOfCMA = nash.service.request('${directory.baseUrl}/v1/authority') //
	  .connectionTimeout(10000) //
	  .receiveTimeout(10000) //
	  .accept('json') //
	  .param('maxResults', 400)//
	  .param('filters', "details.parent:GE/CMA")//
	  .get();
	
    var responseSetOfCMAObject=responseSetOfCMA.asObject();
	
     //_log.info("responseSetOfCMA [****** {} *******]",responseSetOfCMAObject.content);
     _log.info("Numbre of CMA  [****** {} *******]",responseSetOfCMAObject.content.length);

   for (var i = 0 ; i < responseSetOfCMAObject.content.length ; i++) {
       try{ 
		    var template_RM ={
		        "entityId":"",
		        "label":"",
		        "details":{
		            "parent": "GE/RM",
		            "profile": {
		                "tel": "",
		                "fax": "",
		                "email": "",
		                "url": "",
		                "observation": "",
		                "address": {
		                    "recipientName": "",
		                    "addressNameCompl": "",
		                    "compl": "",
		                    "special": "",
		                    "postalCode": "",
		                    "cedex": "",
		                    "cityNumber": ""
		                }
		            },
		            "transferChannels": {
		                "backoffice": {
		                    "state": "enabled"
		                },
		                "email": {
		                    "emailAddress": "",
		                    "state": "disabled"
		                },
		                "address": {
		                    "state": "disabled",
		                    "addressDetail": {
		                        "recipientName": "",
		                        "recipientNameCompl": "",
		                        "addressName": "",
		                        "addressNameCompl": "",
		                        "postalCode": "",
		                        "cityName": ""
		                    }
		                },
		                "ftp": {
		                    "state": "disabled",
		                    "token": "",
		                    "pathFolder": "",
		                    "ftpMode": "",
		                    "comment": "",
		                    "delegationAuthority": ""
		                }
		            }
		        }
		    };
		
		    //remplissage du template
		    //Create tracker ID of new RM 
		   
	         var responseCreateTrackerId = nash.service.request('${tracker.baseUrl}/v1/uid') //
		            .connectionTimeout(10000) //
		            .receiveTimeout(10000)//
		            .accept('text/plain')//
		            .param("author",["System"])// 
		            .put(null);
		            
		    var newTracketId=responseCreateTrackerId.asString();
		     _log.info("newTracketId: {}",newTracketId);
		
		    template_RM.entityId=newTracketId;
	        var newLabel =responseSetOfCMAObject.content[i].label.toString();
	        newLabel=newLabel.replace(/CMA/, 'RM');
		    template_RM.label= newLabel;
	        template_RM.details.profile.tel=responseSetOfCMAObject.content[i].details.profile.tel;
	        template_RM.details.profile.fax=responseSetOfCMAObject.content[i].details.profile.fax;
	        template_RM.details.profile.email=responseSetOfCMAObject.content[i].details.profile.email;
	        template_RM.details.profile.url=responseSetOfCMAObject.content[i].details.profile.url;
	        template_RM.details.profile.observation=responseSetOfCMAObject.content[i].details.profile.observation;
		    template_RM.details.profile.address.recipientName=responseSetOfCMAObject.content[i].label.replace(/CMA/, 'RÉPERTOIRE DES MÉTIERS');
		    template_RM.details.profile.address.addressNameCompl=responseSetOfCMAObject.content[i].details.profile.address.addressNameCompl;
		    template_RM.details.profile.address.compl=responseSetOfCMAObject.content[i].details.profile.address.compl;
		    template_RM.details.profile.address.special=responseSetOfCMAObject.content[i].details.profile.address.special;
		    template_RM.details.profile.address.postalCode=responseSetOfCMAObject.content[i].details.profile.address.postalCode;
		    template_RM.details.profile.address.cedex=responseSetOfCMAObject.content[i].details.profile.address.cedex;
		    template_RM.details.profile.address.cityNumber=responseSetOfCMAObject.content[i].details.profile.address.cityNumber;
		    
		   _log.info("create new RM..." );
	       //_log.info("create new RM... {}",template_RM );
		    //Create Authority ID 
		    var response = nash.service.request('${directory.baseUrl}/v1/authority/merge') //
		        .connectionTimeout(10000) //
		        .receiveTimeout(10000) //
		        .dataType('application/json')//
		        .accept('text/plain') //
		        .put(template_RM);
	
		    numberOfRM++;
	        // ADD RM Authority on Zone
	        // search zone 
	        var responseSetOfZones = nash.service.request('${directory.baseUrl}/v1/authority') //
	        .connectionTimeout(10000) //
	        .receiveTimeout(10000) //
	        .accept('json') //
	        .param('maxResults',990)//
	        .param('filters', "details.parent:ZONES")//
	        .param('filters', "details.CMA:"+responseSetOfCMAObject.content[i].entityId)//
	        .get();
	
	        var responseSetOfZonesObject=responseSetOfZones.asObject();
	        _log.info("Search Zones of CMA  [****** {} *******]",responseSetOfCMAObject.content[i].entityId);
	        _log.info("Number of Zones from CMA (var responseSetOfZonesObject )[****** {} *******]",responseSetOfZonesObject.content.length);
	        //_log.info("List of Zones from CMA (var responseSetOfZonesObject )[****** {} *******]",responseSetOfZonesObject.content);
	
	        for (var j = 0 ; j < responseSetOfZonesObject.content.length ; j++) {
	            responseSetOfZonesObject.content[j].details.RM=newTracketId;
	        
	            //_log.info("Modification de la zone  [****** {} *******]",responseSetOfZonesObject.content[i].label);
	            //_log.info("Contenu de la zone  [****** {} *******]",responseSetOfZonesObject.content[i].details);
	        
	            //_log.info("sauvgarde effectuée [****** {} *******]",responseSetOfZonesObject.content[i].label);
	            try{
		            var response = nash.service.request('${directory.baseUrl}/v1/authority/merge') //
		                .connectionTimeout(10000) //
		                .receiveTimeout(10000) //
		                .dataType('application/json')//
		                .accept('text/plain') //
		                .put(responseSetOfZonesObject.content[j]);
		            numberOfZoneUpdated++;
	            }catch(e){
	            	 _log.error("Une erreur est survenue lors de la mise à jour de la Zone",responseSetOfZonesObject.content[j].label);
	            	 response_message = "Une erreur est survenue, merci de bien vouloir consulter les logs";
	            }
	        }
	        
       }catch(e){
    	   _log.error("Une erreur est survenue lors de la mise à jour de la Zone",responseSetOfCMAObject.content[i].label);
    	   response_message = "Une erreur est survenue, merci de bien vouloir consulter les logs";
       }

    }

}else{
    response_message = 'Vous ne voulez pas créer les RM, merci de votre visite';
}



var data = nash.instance.load('data-generated.xml');
data.bind('regulation', {
    'result':{
        'response': response_message,
        'numberOfRM': ""+numberOfRM,
        'numberOfZoneUpdated': ""+numberOfZoneUpdated
    },
});

