# Description of clic regulation :

The goal is to regulate a clic.

# Authorities concerned :
* *GE*
	* The support team
	* The admin team

# First step : Input form

The form allows the user to : 
* Select a reference to regulate.
* Enter the counter.
* Enter a date on which to apply the clic.
* Enter a comment.

## Second step : Regulation request

This step will send a request for the reference to be regulated.