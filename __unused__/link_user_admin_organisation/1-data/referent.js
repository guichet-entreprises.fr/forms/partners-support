                     
var users = {
	'referent.cci@yopmail.com' : 'C',
	'referent.urssaf@yopmail.com' : 'U',
	'referent.ca@yopmail.com' : 'X',
	'referent.cma@yopmail.com' : 'M',
	'referent.greffe@yopmail.com' : 'G'
};

var mergeAuthority = [];
for (var email in users) {
	var userResponse = nash.service.request('${account.baseUrl.read}/private/users/{id}', email) //
		.accept('text') //
		.get();
	
	var userObject = userResponse.asObject();
	log.info('User account {}', userObject);
	
	var authorityResponse =  nash.service.request('${directory.baseUrl}/v2/ref/list/authority?filters=details.ediCode~' + users[email] + '&maxResults=999').accept('json').get();
		
	if (200 == authorityResponse.getStatus()) {
		var authorities = authorityResponse.asObject();
		authorities['content'].forEach(function(authority) {
			log.info('Update user roles for authority uid : {}', authority['entityId']);
			
			var roleResponse = nash.service.request('${directory.baseUrl}/v1/authority/{authorityCode}/user/{userId}/role/{role}', authority['entityId'], userObject.trackerId, 'referent').accept('text').post(null); 
			if (200 != roleResponse.getStatus()) {
				log.error('An error occured when update user roles for authority uid {}', authority['entityId']);
			}
		});		
	}
}
