                     
var email = "admin.ge@yopmail.com";
var userResponse = nash.service.request('${account.baseUrl.read}/private/users/{id}', email) //
	.accept('text') //
	.get();

if (200 != userResponse.getStatus()) {
	throw "Unable to find user email " + email
}


var authorityResponse =  nash.service.request('${directory.baseUrl}/v2/ref/list/authority?filters=entityId:GE&maxResults=1').accept('json').get();
if (200 != authorityResponse.getStatus()) {
	throw "Unable to find Guichet-Entreprises authoriy"
}
	
var userObject = userResponse.asObject();
log.info('User account {}', userObject);

var authorities = authorityResponse.asObject();
authorities['content'].forEach(function(authority) {
	log.info('Start updating user roles for authority uid : {}', authority['entityId']);
	
	nash.service.request('${directory.baseUrl}/v1/authority/{authorityCode}/user/{userId}/role/{role}', authority['entityId'], userObject.trackerId, 'admin').accept('text').post(null); 
	nash.service.request('${directory.baseUrl}/v1/authority/{authorityCode}/user/{userId}/role/{role}', authority['entityId'], userObject.trackerId, 'referent').accept('text').post(null); 
	nash.service.request('${directory.baseUrl}/v1/authority/{authorityCode}/user/{userId}/role/{role}', authority['entityId'], userObject.trackerId, 'user').accept('text').post(null); 
	nash.service.request('${directory.baseUrl}/v1/authority/{authorityCode}/user/{userId}/role/{role}', authority['entityId'], userObject.trackerId, 'accountant').accept('text').post(null); 
	nash.service.request('${directory.baseUrl}/v1/authority/{authorityCode}/user/{userId}/role/{role}', authority['entityId'], userObject.trackerId, 'all').accept('text').post(null); 
	
	log.info('Update user roles for authority uid {} with success', authority['entityId']);
});		
