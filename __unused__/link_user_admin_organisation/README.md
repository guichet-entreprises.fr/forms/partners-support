# Description :
<br />

The goal is to link partners users to their authorities.

[Click here to get more information about user partners account](https://gipge-rsi-confluence.atlassian.net/wiki/spaces/GE/pages/1151107074/DEV+Comptes+Guichet-Entreprises+et+Guichet-Partenaires)

<br />
<br />

# Team(s) concerned :
* The Development teams
