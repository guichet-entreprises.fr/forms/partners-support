var uid = _input.dataGroup.search.uid;

_log.info("recordUid is {}", uid);
_log.info('Start searching info for the record '+uid);

// The user roles
var userRoles = JSON.parse(user.getRoles());

_log.info("userRoles is {}", userRoles);

var searchResult = null;
var record;
var response_message;

var filters = [];
filters.push('uid:' + uid);

try {
	

	searchResult = nash.service.request('${payment.proxy.api.private.baseUrl}/api/paybox/{uid}', uid)//
		.connectionTimeout(10000) //
		.receiveTimeout(10000) //
		.accept('json') //
		.get();
		
		log.info('==> Result of record '+uid+' search : ' +searchResult.asObject());
		
		var response_status = searchResult.getStatus();	

		log.info('==> Result status is : '+ response_status);		
		
		searchResult = searchResult.asObject();
		
		log.info('==> searchResult is : '+ searchResult);
	// Success
	if (searchResult != null && response_status == 200) {
		record = searchResult;
		log.info('==> record is : '+ record);
		response_message = 'Les informations du paiement ont été trouvées avec succès.';
	} 	
	// Record not found. 
	else if(searchResult != null){
		response_message = 'Le dossier demandé n\'existe pas.';
	}
	// Error
	else{
		response_message = 'Une erreur technique est survenue lors de la recherche du dossier.';
	}

} catch(error){
	log.info('Error when searching for the record ' + uid + ' info :  => ' + error);
	response_message = 'Une erreur technique est survenue lors de la recherche du dossier.';
}

// Retrieve payment info from Proxy
var recordInfo = {};
var name;
var trackerId;

 if(record != null){
	log.info('entred in record');
	
	recordInfo.uid = uid;
	recordInfo.refCmd = record.refCmd;
	recordInfo.amount = record.amount/100;
	recordInfo.status = record.status;
	var payments = record.payments;
	
	for(var i=0; i<payments.length; i++){
		var amountToBeCast = payments[i]['amount'];
		log.info('payment amount is : '+ amountToBeCast);
		payments[i]['amount'] = amountToBeCast/100;
	}
	log.info('payments is {}', payments);

 }

// bind response
var data = nash.instance.load('data-generated.xml');
data.bind('results',
{
	'response':{
		'message':response_message
	}, 		
	'recordInfo': recordInfo,
	'paymentsInfo':{
		'payments':payments
	}
});