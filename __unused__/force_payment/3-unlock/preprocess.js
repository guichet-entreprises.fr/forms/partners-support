//Get input data
var input = _INPUT_;
_log.info("input is {}", input);

try{
var recordId  = _INPUT_.results.recordInfo.uid;
_log.info("recordId is {}", recordId);


//Call payment Refund WS : 

var response = nash.service.request('${nash.ws.private.url}/v1/Record/code/{code}/unlock',recordId) //
        .connectionTimeout(50000) //
        .receiveTimeout(50000)
        .accept('application/json') //
        .post(null);
		
var response_status = response.getStatus();

log.info('==> Result status is : '+ response_status);		

return spec.create({
								id : "unlockResult",
								label : "Déblocage",
								groups : [ spec.createGroup({
									id : 'result',
									label : 'Résultat de la tentative de débloquage du dossier',
									description : "Le dossier a été débloqué avec succès, vous pouvez procéder à la finalisation de celui-ci.",
									data : []
								}) ]
							});

}catch (e){
_log.info("error when calling nash to unlock record  {} {}",  _INPUT_.results.recordInfo.uid, e);
return spec.create({
								id : "unlockResult",
								label : "Déblocage",
								groups : [ spec.createGroup({
									id : 'result',
									label : 'Résultat de la tentative de débloquage du dossier',
									description : "Une erreure s'est passé lors de l'appel de nash pour débloquer le dossier.",
									data : []
								}) ]
							});
}