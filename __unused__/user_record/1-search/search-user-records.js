// try to find the id to search
var trackerId = _INPUT_.record.results.user.trackerId;

// Test if a search is needed
if ((trackerId == null) || (trackerId === "")) {
	return;
};

// --------------------------------------------------------------------------------------
// Find the user
// --------------------------------------------------------------------------------------
function findRecords(trackerId, recordId, dateSearch, xmlDisplay, resultsXmlPath) {
	fullUrl = _CONFIG_.get('forms.private.url') + '/v1/Record';

	//save call data
	xmlDisplay.bind(resultsXmlPath + ".input", {
		"trackerId": trackerId,
		"url": fullUrl,
	});

	userRoles = {};
	userRoles['GE/' + trackerId] = ['*'];

	// perform the call
	searchResult = null;
	var filters = {
		'uid' : (null != recordId && "" !== recordId) ? recordId : null,
		'created>=' : (null != dateSearch && null != dateSearch.from && "" !== dateSearch.from) ? dateSearch.from : null,
		'created<=' : (null != dateSearch && null != dateSearch.to && "" !== dateSearch.to) ? dateSearch.to : null
	};
	
	var queryFilter = [];
	if (null != filters['uid'] && "" !== filters['uid']) {
		queryFilter.push('uid:' + filters['uid']);
	}
	
	['created>=', 'created<='].forEach(function (filter) {
		if (null != filters[filter] && "" !== filters[filter]) {
			
			var dateTemp = new Date(parseInt((filters[filter]).getTimeInMillis()));
			var year = dateTemp.getFullYear();
			var month = ((dateTemp.getMonth() + 1) < 10) ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1);
			var day = (dateTemp.getDate() < 10) ? "0" + (dateTemp.getDate()) : dateTemp.getDate();
			var date = year + "-" + month + "-" + day ; 

			queryFilter.push(filter + date);
		}
	});
	
	log.info('filters : ' + JSON.stringify(queryFilter.join("&")));
	
	try {
		var nashRequest = nash.service.request(fullUrl) //
			.param('startIndex', 0) //
			.param('maxResults', 20) //
			;
		
		queryFilter.forEach(function (filter) {
			nashRequest.param('filters', filter) //
		});
		
		var searchResult = nashRequest
			.connectionTimeout(10000) //
			.receiveTimeout(10000) //
			.dataType('application/json')//
			.header('X-Roles', JSON.stringify(userRoles)) //
			.accept('json') //
			.get() //
	} catch (error) {
		log.info('=> Error occured while calling account : ' + error);
		xmlDisplay.bind(resultsXmlPath + ".output", {
			"status": "ERROR",
			"response": "" + error,
		});
		return null;
	};

	responseObj = searchResult.asObject();

	xmlDisplay.bind(resultsXmlPath + ".output", {
		"status": searchResult.getStatus(),
		"response": "" + responseObj,
	});

	// test the 200 code
	if (searchResult == null || searchResult.getStatus() != 200) {
		return null;
	};

	return responseObj;
};
// --------------------------------------------------------------------------------------


// Load the screen for the user
var display = nash.instance.load("display-search.xml");

var recordId = _INPUT_.record.search.recordId;
var dateSearch = _INPUT_.record.search.date;

var userRecords = findRecords(trackerId, recordId, dateSearch, display, "record.debug.userRecords");

if (null == userRecords) { return; };

display.bind("record.results.userRecords", { "nbRecords": userRecords.totalResults });
display.bind("record.results.userRecords", { "record": userRecords.content });