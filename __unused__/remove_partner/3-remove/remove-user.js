// Load the screen for the user
var display = nash.instance.load("display-result.xml");

// URL liste
var targetUrlAccounts = {
	"ge": _CONFIG_.get('account.ge.baseUrl.read'),
	"gp": _CONFIG_.get('account.baseUrl.read'),
};


for (var plateform in targetUrlAccounts) {

	email = null;
	data = null
	if (plateform == "ge") {
		data = _INPUT_.results.ge
	};

	if (plateform == "gp") {
		data = _INPUT_.results.gp
	};

	fullUrl = targetUrlAccounts[plateform] + '/private/users/' + email;

	//save call data
	display.bind("results." + plateform + ".input", {
		"email": email,
		"url": fullUrl,
	});

	// perform the call
	searchResult = null;
	try {

		searchResult = nash.service.request(fullUrl, idOrEmail) //
			.dataType('application/json') //
			.accept('json')
			.get();

	} catch (error) {
		log.info('=> Error occured while calling account : ' + error);
		display.bind("results." + plateform + ".output", {
			"status": "ERROR",
			"response": "" + error,
		});
		continue;
	};

	responseObj = searchResult.asObject();

	display.bind("results." + plateform + ".output", {
		"status": searchResult.getStatus(),
		"response": "" + responseObj,
	});

	// test the 200 code
	if (searchResult == null || searchResult.getStatus() != 200) {
		continue;
	};

	// test the 1005 code
	if ((responseObj == null) || (responseObj['errorCode'] == "1005")) {
		display.bind("results." + plateform + ".output", { "found": "" });
		continue;
	};

	// OK there is a result
	display.bind("results." + plateform + ".output", {
		"found": plateform + "ok",
	});

	display.bind("results." + plateform + ".result", {
		"firstName": responseObj['firstName'],
		"lastName": responseObj['lastName'],
		"civility": responseObj['civility'],
		"language": responseObj['language'],
		"email": responseObj['email'],
		"password": responseObj['password'],
		"phone": responseObj['phone'],
		"trackerId": responseObj['trackerId'],
		"fcId": responseObj['fcId'],
		"phoneCheck": responseObj['phoneCheck'],
		"connectionDate": responseObj['connectionDate'],
		"mailType": responseObj['mailType'],
		"referrerFirstName": responseObj['referrerFirstName'],
		"referrerLastName": responseObj['referrerLastName'],
		"oldEmail": responseObj['oldEmail'],
		"remove": plateform + "removenok",
	});
};



var authorityObject = {};
var response_message;
var inputGroup = _input.results;

// The input informations
var trackerId = inputGroup.userInfo.trackerId;
var authorities = inputGroup.authoritiesGroup.authorities;

var successUnrigistration = true;
log.info('=> authorities length : ' + authorities.length);

if (authorities[0].length > 0) {

	// Unregister the user from the authorities
	for (var i = 0; i < authorities.length; i++) {
		log.info('=> Unregister the user ' + trackerId + ' in the authority ' + authorities[i]);

		try {
			// Retrieve directory informations for each authority
			var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', authorities[i]) //
				.get();

			var jsonBlock = response.asObject();
			log.info('=> Retrieved informations for the authority : ' + authorities[i] + ' : ' + jsonBlock);

			// Extract authority users list
			var users = jsonBlock.details.users;
			log.info('=> Retrieved users for the authority : ' + authorities[i] + ' : ' + users);

			var details = {}
			details["users"] = [];

			// Remove the user from the authority users list
			for (var j = 0; j < users.length; j++) {
				if (users[j].id != trackerId) {
					details["users"].push(users[j]);
				}
			}

			authorityObject.entityId = authorities[i];
			authorityObject.details = details;

			_log.info("details are  {}", details);
			_log.info("authorityObject is  {}", authorityObject);

			// Save the modification in directory
			var response = nash.service.request('${directory.baseUrl}/v1/authority/merge') //
				.connectionTimeout(10000) //
				.receiveTimeout(10000) //
				.dataType('application/json')//
				.accept('json') //
				.put(authorityObject);


		} catch (error) {
			successUnrigistration = false;
			response_message = 'Une erreur est survenue lors de la suppression de l\'utilisateur dans directory.';
			log.info('=> Error occured while unregistering the user from the authorities : ' + error);
		}
	}
}

if (successUnrigistration) {
	// Deleting the user in account
	var userResponse = null;
	var resultAccount = null;

	try {

		log.info('=> Account url : ' + _CONFIG_.get('account.baseUrl.read'));
		log.info('=> Account email : ' + inputGroup.userInfo.email);

		userResponse = nash.service.request('${account.baseUrl.read}/private/users/{mail}', inputGroup.userInfo.email)
			.dataType('application/json') //
			.accept('json')
			.delete();

		if (userResponse != null && userResponse.getStatus() == 200) {
			resultAccount = userResponse.asObject();
		}
	} catch (e) {
		// HTTP 400 if the user does not exist	
	}

	if (resultAccount == null || resultAccount.errorCode == 1005) {
		response_message = 'Une erreur technique est survenue lors de la suppression du compte.';
		log.info('=> resultAccount ' + resultAccount);
	} else {
		response_message = 'L\'utilisateur ' + trackerId + ' a été supprimé avec succès.';
	}
}


// bind response
var data = nash.instance.load('data-generated.xml');
data.bind('results',
	{
		'response': {
			'message': response_message
		}
	});