// Load the screen for the user
var display = nash.instance.load("display-result.xml");

// all values
var idOrEmail = _INPUT_.account.id
var targetUrlAccounts = {
	"ge": _CONFIG_.get('account.ge.baseUrl.read'),
	"gp": _CONFIG_.get('account.baseUrl.read'),
};

for (var plateform in targetUrlAccounts) {
	fullUrl = targetUrlAccounts[plateform] + '/private/users/' + idOrEmail;

	//save call data
	display.bind("results." + plateform + ".input", {
		"idOrEmail": idOrEmail,
		"url": fullUrl,
	});

	// perform the call
	searchResult = null;
	try {

		searchResult = nash.service.request(fullUrl, idOrEmail) //
			.dataType('application/json') //
			.accept('json')
			.get();

	} catch (error) {
		log.info('=> Error occured while calling account : ' + error);
		display.bind("results." + plateform + ".output", {
			"status": "ERROR",
			"response": "" + error,
		});
		continue;
	};

	responseObj = searchResult.asObject();

	display.bind("results." + plateform + ".output", {
		"status": searchResult.getStatus(),
		"response": "" + responseObj,
	});

	// test the 200 code
	if (searchResult == null || searchResult.getStatus() != 200) {
		continue;
	};

	// test the 1005 code
	if ((responseObj == null) || (responseObj['errorCode'] == "1005")) {
		display.bind("results." + plateform + ".output", { "found": "" });
		continue;
	};

	// OK there is a result
	display.bind("results." + plateform + ".output", {
		"found": plateform + "ok",
	});

	display.bind("results." + plateform + ".result", {
		"firstName": responseObj['firstName'],
		"lastName": responseObj['lastName'],
		"civility": responseObj['civility'],
		"language": responseObj['language'],
		"email": responseObj['email'],
		"password": responseObj['password'],
		"phone": responseObj['phone'],
		"trackerId": responseObj['trackerId'],
		"fcId": responseObj['fcId'],
		"phoneCheck": responseObj['phoneCheck'],
		"connectionDate": responseObj['connectionDate'],
		"mailType": responseObj['mailType'],
		"referrerFirstName": responseObj['referrerFirstName'],
		"referrerLastName": responseObj['referrerLastName'],
		"oldEmail": responseObj['oldEmail'],
		"remove": plateform + "removenok",
	});
};