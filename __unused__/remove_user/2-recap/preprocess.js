//TODO BIND
// get the user in Account
var userResponse = null;
var user;
try {
	var userToDelete = null
	if($deleteUser.infoUser.userEmail != null){
		userToDelete = $deleteUser.infoUser.userEmail;
	}else{
		userToDelete = $deleteUser.infoUser.userTracker;
	}
	userResponse = nash.service.request('${account.ge.baseUrl.read}/private/users/{mail}', userToDelete) //
		.dataType('application/json') //
		.accept('json')
		.get();
		
	if (userResponse != null && userResponse.getStatus() == 200) {
		user = userResponse.asObject();
	}
} catch (e) {
	// HTTP 400 if the user does not exist	
}

if (user == null || user.errorCode == 1005) {
	return spec.create({ id : 'userData', groups : [
		spec.createGroup({ id : 'parameters', label : 'Paramètres', data : [
			spec.createGroup({ id : 'accountParameters', label : 'Paramètres du compte', data : [

				spec.createData({ id : 'error', label : 'ERREUR', type: 'StringReadOnly', value: "user introuvable : " + user})
			]})
		]})
	]});
} else {
	var messageDisplayed = "au clic sur étape suivante, le compte ci dessous sera supprimé :";
	// prepare a form to display the already existing user
	return spec.create({ id : 'userData', groups : [
		spec.createGroup({ id : 'accountParameters', label : 'Paramètres du compte', data : [
			spec.createGroup({ id : 'warning',      label : 'Code',                description: messageDisplayed,        data: [] }),
			spec.createData({ id : 'trackerId',     label : 'trackerId',           type: 'StringReadOnly',               value: user.trackerId }),
			spec.createData({ id : 'user',          label : 'user',                type: 'StringReadOnly',               value: user.toString()}),
			spec.createData({ id : 'civility',      label : 'Civilité',            type: 'StringReadOnly',               value: user.civility }),
			spec.createData({ id : 'lastName',      label : 'Nom',                 type: 'StringReadOnly',               value: user.lastName }),
			spec.createData({ id : 'firstName',     label : 'Prénom',              type: 'StringReadOnly',               value: user.firstName }),
			spec.createData({ id : 'email',         label : 'Courriel',            type : 'StringReadOnly',              value: user.email }),
			spec.createData({ id : 'connexionDate', label : 'date de derniere connexion', type : 'StringReadOnly',     value: user.connectionDate }),
			spec.createData({ id : 'phone',         label : 'Numéro de téléphone', type : 'StringReadOnly',              value: user.phone })
		]})
	]});
}
