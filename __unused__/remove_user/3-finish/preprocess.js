//TODO BIND
// get the user in Account
var userResponse = null;
var result = null;
try {

	userResponse = nash.service.request('${account.ge.baseUrl.read}/private/users/{mail}', $userData.accountParameters.email)
		.dataType('application/json') //
		.accept('json')
		.delete();
	
	if (userResponse != null && userResponse.getStatus() == 200) {
		result = userResponse.asObject();
	}
} catch (e) {
	// HTTP 400 if the user does not exist	
}

if (result == null || result.errorCode == 1005) {
	return spec.create({ id : 'resultGroup', groups : [
		spec.createGroup({ id : 'parameters', label : 'Paramètres', data : [
			spec.createGroup({ id : 'accountParameters', label : 'résultat', data : [
				spec.createData({ id : 'error', label : 'ERREUR', type: 'StringReadOnly', value: "le compte n'a pas été supprimé : " + userResponse})
			]})
		]})
	]});
}else{
	return spec.create({ id : 'resultGroup', groups : [
		spec.createGroup({ id : 'parameters', label : 'Paramètres', data : [
			spec.createGroup({ id : 'accountDeleted', label : 'résultat', data : [

				spec.createData({ id : 'ok', label : 'OK', type: 'StringReadOnly', value: "compte " + $userData.accountParameters.email + "(" + $userData.accountParameters.trackerId  + ") supprimé"})
			]})
		]})
	]});
}
