![Guichet Entreprises](https://welcome.guichet-entreprises.fr/assets/images/guichet-entreprises/logo/long-base.png)

# Rechercher les informations de paiement d'un dossier

## Description
La formalité permet de rechercher les informations d'un paiement effectuer par un déclarant à partir de l'un des filtres suivants:

 - Numéro de dossier 
 - Numéro de commande
 - D'une période 

## Capture écrans


 ![enter image description here](images/FA_search_payment.png)

 
## Utilisateurs de la formalité

La formalité est à destination de l'administrateur du Guichet Entreprises.


# Détails

**Version de la formalité**: 0.1

**auteur de la formalité**: Minecraft


## UML diagrams

You can render UML diagrams using [Mermaid](https://mermaidjs.github.io/).

```mermaid
sequenceDiagram
Support ->> Welcome partenaires : sélectionner la formalité
Welcome partenaires -->>Nash support : redirection vers nash support
Nash support -->> Support : affichage de la formalité
Support -->> Nash support : selectionner l'autorité
loop Recherche
Nash support -->> Support : Affichage des critère de recherche
Support  -->> Support : Remplir les filres
Support -->> Nash support : Rechercher
Nash support -->> Support : Affichages du résulatat de la recherche
end
```
