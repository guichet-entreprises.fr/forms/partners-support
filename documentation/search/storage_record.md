![Guichet Entreprises](https://welcome.guichet-entreprises.fr/assets/images/guichet-entreprises/logo/long-base.png)

# Recherche de dossier dans Storage

## Description
La formalité permet de rechercher un/les dossier(s) à partir des filtres suivants:

 - Identifiant Storage
 - Mot-clé : Numéro de liasse, dénomination ou autre mot-clé
 - ID ou Email du compte de l'utilisateur
 - Période de dépôt 
 - autorité destinatrice
 - Titre ou description du dossier
 - Zone d'archivage
 
####résultat de la recherche

La formalité affiche l'ensemble des dossiers qui correspondent aux critères de recherche et pour chaque dossier trouvé on affiche:
   
 - Identifiant Storage
 - Référence associée
 - Zone d'archivage
 - Historique Tracker de la référence


## Capture écrans


 ![enter image description here](images/storage_search.png)
 
 ![enter image description here](images/storage_search_2.png)

 
## Utilisateurs de la formalité

La formalité est à destination de l'administrateur du Guichet Entreprises.


# Détails

**Version de la formalité**: 0.1

**auteur de la formalité**: Minecraft

**Rôle **: admin

## UML diagrams

You can render UML diagrams using [Mermaid](https://mermaidjs.github.io/).

```mermaid
sequenceDiagram
Support ->> Welcome partenaires : sélectionner la formalité
Welcome partenaires -->>Nash support : redirection vers nash support
Nash support -->> Support : affichage de la formalité
loop Recherche
Support -->> Support : Remplir les critères de recherche
Support -->> Nash support : Rechercher
Nash support -->> Support : Affichages du résulatat de la recherche
end
```
