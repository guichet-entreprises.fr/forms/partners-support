![Guichet Entreprises](https://welcome.guichet-entreprises.fr/assets/images/guichet-entreprises/logo/long-base.png)

# Recherche dossier

## Description
La formalité permet de rechercher les informations d'un/des dossier(s) à partir des filtres suivants:

 - Identifiant de dossier
 - Identifiant du modèle de formalité
 - D'une période
 -ID ou Email du compte de l'utilisateur
 -Titre ou description du dossier
 
résultat de la recherche

La formalité affiche l'ensemble des dossiers qui correspondent aux critères de recherche et pour chaque dossier trouvé on affiche:
  
 
 - Identifiant de dossier
 - Identifiant de l'auteur
 - Date et heure de création
 - Date de la dernière modification
 - Titre de la formalité (nom de la démarche)
 - Historique tracker
 - Détails sur le dossier (information sur le modèle de formalité)


## Capture écrans


 ![enter image description here](images/search_record.png)
 
 ![enter image description here](images/search_record_2.png)

 
## Utilisateurs de la formalité

La formalité est à destination de l'équipe support du Guichet Entreprises.


# Détails

**Version de la formalité**: 0.1

**auteur de la formalité**: Minecraft

**Rôle **: admin et support

## UML diagrams

You can render UML diagrams using [Mermaid](https://mermaidjs.github.io/).

```mermaid
sequenceDiagram
Support ->> Welcome partenaires : sélectionner la formalité
Welcome partenaires -->>Nash support : redirection vers nash support
Nash support -->> Support : affichage de la formalité
loop Recherche
Support -->> Support : Remplir les critères de recherche
Support -->> Nash support : Rechercher
Nash support -->> Support : Affichages du résulatat de la recherche
end
```
