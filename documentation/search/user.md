![Guichet Entreprises](https://welcome.guichet-entreprises.fr/assets/images/guichet-entreprises/logo/long-base.png)

# Recherche des informations d'un utilisateur

## Description

La formalité permet de rechercher les informations d'un utilisateur (déclarant ou partenaires) à partir d'une adresse mail:


## Capture écrans


 ![enter image description here](images/search_user.png)
 
 ![enter image description here](images/search_user_2.png)

 
## Utilisateurs de la formalité

La formalité est à destination de léquipe support du Guichet Entreprises.


# Détails

**Version de la formalité**: 0.1

**auteur de la formalité**: Minecraft

**Rôle **: admin et support

## UML diagrams

You can render UML diagrams using [Mermaid](https://mermaidjs.github.io/).

```mermaid
sequenceDiagram
Support ->> Welcome partenaires : sélectionner la formalité
Welcome partenaires -->>Nash support : redirection vers nash support
Nash support -->> Support : affichage de la formalité
Support -->> Support : Remplir les critères de recherche
Support -->> Nash support : Rechercher
Nash support -->> Support : Affichages du résulatat de la recherche

```
