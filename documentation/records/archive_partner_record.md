![Guichet Entreprises](https://welcome.guichet-entreprises.fr/assets/images/guichet-entreprises/logo/long-base.png)

# Suppression logique d'un dossier partenaire

## Description

La formalité permet d'archiver un dossier qui se trouve dans le dashboard des partenaires en saisissant un numéro de dossier partenaires :

## Capture écrans

 ![enter image description here](images/archive_partner_record.png)

 
 
## Utilisateurs de la formalité

La formalité est à destination de l'aministrateur et du sopport du Guichet Entreprises.


# Détails

**Version de la formalité**: 0.1

**auteur de la formalité**: Mendeleïev

**Rôle**: admin

## UML diagrams


