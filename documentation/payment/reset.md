
![enter image description here](https://welcome.guichet-entreprises.fr/assets/images/guichet-entreprises/logo/long-base.png)

# Réinitialiser l'étape de paiement

## Description

La formalité permet de réinitialiser l'étape de paiement dans le cas ou le dossier est bloqué suite à une erreur technique afin que les déclarants puissent retenter le paiement.

## Capture écrans


![enter image description here](images/reset_payment_1.png)

![enter image description here](images/reset_payment_2.png)

   
 
## Utilisateurs de la formalité

La formalité est à destination de l'administrateur du Guichet Entreprises.


# Détails

**Version de la formalité**: 0.1

**auteur de la formalité**: Minecraft

**Rôle **: Admin



## UML diagrams

You can render UML diagrams using [Mermaid](https://mermaidjs.github.io/).

```mermaid
sequenceDiagram
Support ->> Welcome partenaires : sélectionner la formalité
Welcome partenaires -->>Nash support : redirection vers nash support
Nash support -->> Support : affichage de la formalité
loop 0 dossiers
Support -->> Nash support : remplir les filtrès et recherche les dossiers en erreur
Nash support -->> Support : afficher le résultat de la recherche
end
Support  -->> Nash support : Réinitialiser le(s) dossier(s)
Nash support -->> Support : Affichages du résulatat de la Réinitialisation

```
