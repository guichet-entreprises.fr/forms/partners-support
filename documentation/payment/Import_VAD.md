
![enter image description here](https://welcome.guichet-entreprises.fr/assets/images/guichet-entreprises/logo/long-base.png)

# Import des comptes de vente à distance (VAD)

## Description

Cette formalité permet d'ajouter ou de mettre à jour des comptes vad via l'import d'un fichier CSV qui respecte un format bien défini

## Structure du fichier CSV

Le fichier CSV doit avoir les colonnes suivantes: 

ch_cfe
ch_pbxsite
ch_pbxrang
ch_pbxident
ch_pbxclehmac
ch_pbxcledirectplus


## Capture écrans


  ![enter image description here](images/import_comptes_VAD_1.png)

 
## Utilisateurs de la formalité

La formalité est à destination de l'administrateur du Guichet Entreprises.


# Détails

**Version de la formalité**: 0.1

**auteur de la formalité**: Minecraft

**Rôle**: Admin


## UML diagrams

You can render UML diagrams using [Mermaid](https://mermaidjs.github.io/).

```mermaid
sequenceDiagram
Support ->> Welcome partenaires : sélectionner la formalité
Welcome partenaires -->>Nash support : redirection vers nash support
Nash support -->> Support : affichage de la formalité
Support -->> Nash support : selectionner le l'application cible (Forms1/forms2)et le fichier CSV
Nash support -->> Support : Affichage du résultat de l'import 
Support  -->> Support : quitter

```
