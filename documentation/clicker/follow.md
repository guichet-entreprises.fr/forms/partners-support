![Guichet Entreprises](https://welcome.guichet-entreprises.fr/assets/images/guichet-entreprises/logo/long-base.png)

# Suivre l'activité du Guichet Entreprises

## Description

La formalité permet de compter le nombre de click suite à un événement, à partir de l'un des filtres suivants:

- Dossiers Crées
- Dossiers Finalisés
- Dossiers Transmis à toutes les autorités
- Dossiers Transmis à une autorité particulière
- Dossiers De création d'entreprises
- Dossiers De modification de statut d'entreprises
- Dossiers De cessation d'activité
- En utilisant un mot-clé particulier

## Capture écrans

 ![enter image description here](images/follow_1.png)

 
 
## Utilisateurs de la formalité

La formalité est à destination de l'aministrateur du Guichet Entreprises afin d'avoir des statistiques.


# Détails

**Version de la formalité**: 0.1

**auteur de la formalité**: Minecraft

**Rôle**: admin

## UML diagrams

You can render UML diagrams using [Mermaid](https://mermaidjs.github.io/).

```mermaid
sequenceDiagram
Support ->> Welcome partenaires : sélectionner la formalité
Welcome partenaires -->>Nash support : redirection vers nash support
Nash support -->> Support : affichage de la formalité
loop Recherche
Nash support -->> Support : Affichage des critère de recherche
Support  -->> Support : Remplir les filres
Support -->> Nash support : Rechercher
Nash support -->> Support : Affichages du résulatat de la recherche
end
```
