![Guichet Entreprises](https://welcome.guichet-entreprises.fr/assets/images/guichet-entreprises/logo/long-base.png)

# Suivre l'activité du Guichet Entreprises

## Description

La formalité permet la régulation de clics liées à un événement en ajoutant des clics positive ou négatives.


## Capture écrans


 ![enter image description here](images/adjust_1.png)

 
## Utilisateurs de la formalité

La formalité est à destination de l'aministrateur du Guichet Entreprises afin d'avoir des statistiques.


# Détails

**Version de la formalité**: 0.1

**auteur de la formalité**: Minecraft

**Rôle**: admin

## UML diagrams

You can render UML diagrams using [Mermaid](https://mermaidjs.github.io/).

```mermaid
sequenceDiagram
Support ->> Welcome partenaires : sélectionner la formalité
Welcome partenaires -->>Nash support : redirection vers nash support
Nash support -->> Support : affichage de la formalité
Support -->> Support : remplir les champs
Support  -->> Support : Remplir les filres
Support -->> Nash support : Modifier
Nash support -->> Support : Affichages du résulatat de la modification
```
