

var trackerRegex = /\d{4}-\d{2}-\w{3}-\w{3}-\d{2}/;

//-->Get target plateform
var targetLabel = $context.action.plateform.target.getId();
var target = _CONFIG_.get(targetLabel);
var content = $context.action.references.listRefID.split(/\r?\n/);

var i;
var resultOK = [];
var resultKO = [];
var response = "";

log.info('Target selected : {}', $context.action.plateform.target.getId());
_log.info("list des referencesID/code : {}", content);

for(i=0 ; i < content.length; i++){
	try{
		line = content[i];
		_log.info("work on content[i] : " + content[i]);
		token = line.split('#')[0].split(';')[0].trim();
		var codeToRemonve = "";
		if(token == ""){
			continue;
		}
		_log.info("token : " + token + " | encoded : " + encodeURI(token));
		if(!trackerRegex.test(token)){
			//-->Call NASH WS to get a specification from a reference
			response = nash.service.request(target + '/v1/Specification') //
				.connectionTimeout(60000) //
				.receiveTimeout(60000) //
				.param("filters", "tagged%3Atrue")
				.param("filters", "reference%3A" + encodeURI(token))
				.get();
				var res = response.asObject();
			if (response != null and response.getStatus() == 200 and res.content != null and res.content.length == 1) {
				codeToRemonve = res.content[0].code;
				_log.info("code trouvé : {}", codeToRemonve);
			}else{
				resultKO.push(token + ";code non trouvé;" + response.getStatus());
				_log.info("code non trouvé : {}", res);
				continue;
			}
		}else{
			//-->Call NASH WS to get a specification from a reference
			response = nash.service.request(target + '/v1/Specification/code/' + token) //
				.connectionTimeout(60000) //
				.receiveTimeout(60000) //
				.get();
				var res = response.asObject();
			if (response != null and response.getStatus() == 200 and res != null) {
				codeToRemonve = res.code;
				_log.info("code trouvé : {}", codeToRemonve);
			}else{
				resultKO.push(token + ";code non trouvé;" + response.getStatus());
				_log.info("code non trouvé : {}", res);
				continue;
			}
		}

		_log.info("code to remove : {}", codeToRemonve);
		if(codeToRemonve !== ""){
			var response = nash.service.request(target + '/v1/Specification/code/' + codeToRemonve) //
					.connectionTimeout(60000)
					.receiveTimeout(60000)
					.delete();
			_log.info("remove : {}", response.status);
			if(response.getStatus() == 200){
				resultOK.push(token + "\n");
			}else{
				resultKO.push(token + ";code non trouvé;" + response.getStatus());
			}
		}
	}catch(error) {
		_log.info("erreur lors de la formalité de dépublication : " + error);
		resultKO.push(content[i] + ";" + "KO ;" + error + "\n")
	}
}
log.info('target : {}', target);



var data = nash.instance.load('data.xml');
data.bind("request",{
    input:{
        target:targetLabel + " (" + target + ")"
    },
    output:{
		refIdOK : resultOK.join("\n"),
		refIdKO : resultKO.join("\n")
	}
});