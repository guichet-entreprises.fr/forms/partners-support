var recordUid = nash.record.description().getRecordUid();
log.info("Redirection to the first step for the record {}", recordUid);

return { url: "/record/" + recordUid + "/0" };