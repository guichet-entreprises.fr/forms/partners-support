var reference = $recap.model.formality.reference;
log.info('reference : {}', reference);

var title = $recap.model.formality.title;
log.info('Title : {}', title);

var quotaReference = $recap.model.formality.quotaReference;
log.info('Référence Tracker du quota : {}', quotaReference);

var quotaNew = -1;
if ($recap.model.open.enterQuota) {
	quotaNew = $recap.model.open.quota;
}
	
var response = nash.service.request('${tracker.baseUrl}/v1/uid/{idTracker}/msg', quotaReference) //
	.connectionTimeout(10000) //
	.receiveTimeout(10000) //
	.param('content', quotaNew) //
	.param("author", 'support') //
	.post(null);
		
var data = nash.instance.load('data.xml');
data.bind('model.formality', {
	'reference' : reference,
	'title' : title,
	'quotaReference' : quotaReference,
	'quota' : ''+quotaNew
});