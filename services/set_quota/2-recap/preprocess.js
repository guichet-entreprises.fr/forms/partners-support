//-->Get last message from quota reference
function tracker(reference) {
	try {
		var response = nash.service.request('${tracker.baseUrl}/v1/uid/' + reference) //
			.connectionTimeout(10000) //
			.receiveTimeout(10000) //
			.dataType('application/json')//
			.accept('json') //
			.get();
		if (null == response || response.status != 200 || null == response.asObject()['messages']) {
			return [];
		}		
		return response.asObject()['messages'];	
	} catch (error) {
		return [];
	}
}
//<--

var reference = $context.quota.formality.reference;
var specification = nash.service.request('${nash.ws.private.url}/v1/Specification/ref/' + encodeURI(reference) + '/info') //
	.connectionTimeout(10000) //
	.receiveTimeout(10000) //
	.dataType('application/json')//
	.accept('json') //
	.get() //
    .asObject();
log.info("Specification found : {}", specification);
var data = nash.instance.load('data.xml');

if (null != specification) {
	var tagged = specification.tagged ? 'Oui' : 'Non';

	var quotaReference = 'GE-QUOTA-'+specification.code;

	var messages = tracker(quotaReference);
	var quota = messages.length == 0 ? -1 : messages[messages.length - 1]['content'];
	log.info("Quota : {}", quota);
	if (isNaN(parseInt(quota))) {
		quota = -1;
	} else {
		quota = parseInt(quota);
	}

	data.bind('model.formality', {
		'reference' : reference,
		'tagged' : tagged,
		'title' : specification.title,
		'quotaReference' : quotaReference,
		'quota' : ''+quota
	});
} else {
	data.bind('model.formality', {
		'reference' : reference
	});
}