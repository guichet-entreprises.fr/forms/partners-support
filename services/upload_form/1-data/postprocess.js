log.info('Choice selected : {}', $context.action.plateform.choice.getId());

//-->Get target plateform
var target = _CONFIG_.get($context.action.plateform.choice.getId());

log.info('target : {}', target);
var numberOfFormalities = $context.action.choices.file.length;
log.info('Number of formalities to upload : {}', numberOfFormalities);


for(i=0 ; i < numberOfFormalities; i++){
	//-->Get file content
	var idFile=i+1;
	var nameOfFormality=$context.action.choices.file[i].getLabel();
	var recordPath = '/1-data/uploaded/action.choices.file-'+idFile+'-'+nameOfFormality;
	var recordAsBytes = nash.util.resourceFromPath(recordPath).getContent();
	var response;
	try{
		log.info('uploading... formality: {}', nameOfFormality);
		
		//-->Call NASH WS to create a specification
		response = nash.service.request(target + '/v1/Specification') //
			.connectionTimeout(60000) //
			.receiveTimeout(60000) //
			.param("published", "true") //
			.param("author", "support") //
			.post(recordAsBytes);
		
		log.info('formality: {} was uploaded', nameOfFormality);
	}catch(error){
		_log.info("ean error occurred while uploading: "+$context.action.choices.file[i].getLabel());
			
		return spec.create({
		id : 'confirmGroup',
		label : 'Résultat de la demande',
		groups : [ spec.createGroup({
			id : 'group',
			label: 'Statut de la demande',
			help : 'erreur on target ' + target +  " | " +error
		}) ]
	});
	}

}

return spec.create({
    id : 'confirmGroup',
    label : 'Résultat de la demande',
    groups : [ spec.createGroup({
	    id : 'group',
		label: 'Statut de la demande',
		help : 'Le dossier a été envoyé sur la plateforme ' + $context.action.plateform.choice.getLabel() + ' avec succès'
    }) ]
});