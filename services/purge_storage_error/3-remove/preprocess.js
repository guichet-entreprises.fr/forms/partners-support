// En cas de time out le reload de l'écran ne relance pas la recherche
var display = nash.instance.load("display.xml");

var idUser = _record.purgeStorage.infoForms.idUser;
var message  = _record.purgeStorage.infoForms.message;
var tray = _record.purgeStorage.infoForms.tray;


display.bind("remove.input", {"idUser" : idUser, "message" : message , "tray" : tray });

var fullUrl = '${storage.gp.url.ws.private}/v1/tray/{storageTray}/file/{storageId}';

var content = _INPUT_.prev.result.listRecord.split(/\r?\n/);
var resultsKO = [];
var resultsOK = [];
var debugStack = [];
content.filter(function(line){return line !== ""}).forEach(function (line) {
	
	var input = line.split(";");
	var storageId = input[0];
	var recordId = input[1];
	var trayValue = input[2];


	try {
		//remove storage record
		nash.service.request(fullUrl, trayValue, storageId) //
			.connectionTimeout(20000) //
			.receiveTimeout(20000) //
			.delete(); 
			
		// call tracker service to publish a message
		var response = nash.service.request('${tracker.baseUrl}/v1/uid/{idTracker}/msg', recordId) //
			.connectionTimeout(10000) //
			.receiveTimeout(10000)
			.param("content", [ message ])
			.param("author", [ 'support' ])
			.post("");			
		
		resultsOK.push(storageId);
		
	} catch (error) {
		log.error('=> Error occured while calling storage : ' + error);
		resultsKO.push(storageId);
		debugStack.push(error)
	};	
});

display.bind("remove.result", {"listRecord" :"KO : \n" + resultsKO.join(" | ") +  "\nOK : \n" + resultsOK.join(" | ") });
display.bind("remove.debug", {"debug": resultsOK + "\n" + resultsKO + "\n" + debugStack.join(" | ")});

