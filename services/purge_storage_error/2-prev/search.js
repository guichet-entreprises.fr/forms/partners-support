
// En cas de time out le reload de l'écran ne relance pas la recherche
if(_INPUT_.prev.result.listRecord != null){
	return;
}

var display = nash.instance.load("display.xml");

var idUser = _record.purgeStorage.infoForms.idUser;
var message  = _record.purgeStorage.infoForms.message;
var tray = _record.purgeStorage.infoForms.tray;


display.bind("prev.input", {"idUser" : idUser, "message" : message , "tray" : tray });

var fullUrl = _CONFIG_.get('storage.gp.url.ws.private') + '/v1/Record';

var searchTerms = [];
var queryFilter = [];
var searchResult;

try {

    if ( tray != null) {
        if ('all' != tray.getId()) {
            queryFilter.push('tray:' + tray.getId());
        }
    }
	
	if (idUser != null) {
		searchTerms.push(idUser);
	}
	
	var nashRequest = nash.service.request(fullUrl)
		.param('startIndex', 0)
		.param('maxResults', 100);
			
        queryFilter.forEach(function (filter) {
            nashRequest.param('filters', filter) 
        });
        
	if (searchTerms.length > 0) {
		nashRequest.param('q', idUser);
	}
	
	searchResult = nashRequest 
		.connectionTimeout(200000) 
		.receiveTimeout(200000) 
		.param('orders', 'created:desc') 
		.param('orders', 'uid:desc') 
		.get();
			
} catch (error) {
	log.error('=> Error occured while calling storage : ' + error);
	display.bind("prev.debug", {"debug": error + fullUrl});
	return null;
};	

responseObj = searchResult.asObject();
var result = "";

responseObj['content'].forEach(function (record) {
            result += record['id'] + ";";
            result += record['referenceId'] + ";";
            result += record['tray'] + ";";
            result += "\n";
        });

display.bind("prev.result", {"listRecord" : result });
display.bind("prev.debug", {"debug": responseObj});

