var Directory = require('/js/directory.js');

// try to find the id to search
log.info('=> preprocess search-parameters.js : ');
log.info('=> _INPUT_.setParameters.results.input.authority : ' + _INPUT_.setParameters.results.input.authority);
var authorityId = _INPUT_.setParameters.results.input.authority;

// Test if a search is needed
if ((authorityId == null) || (authorityId === "")) {
	return;
};

// Load the screen for the user
var display = nash.instance.load("display-search.xml");

resultsXmlPath = "setParameters.results"
_log.info("authorityFuncId is  {}", authorityId);

//save call data
display.bind(resultsXmlPath + ".input", {
	"authorityId": authorityId,
});

var debug = {};
var parameters = Directory.getAuthorityParameters(authorityId, debug);

// OK there is a result
display.bind(resultsXmlPath + ".output", {
	"status": debug.status,
	"parameters": "" + parameters,
	"url": debug.url,
	"found": "ok",
});

display.bind(resultsXmlPath, { "parameters": Directory.transformParameterToCardinalite(parameters) });
