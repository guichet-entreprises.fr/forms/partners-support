var recordUid = nash.record.description().getRecordUid();
log.info("Redirection to the first page for the record {}", recordUid);

if(_INPUT_.setParameters.search.searchButton.value =='yes'){
    return { url: "/record/" + recordUid + "/0/page/0" };
}

if(_INPUT_.setParameters.search.authority == null){
    return { url: "/record/" + recordUid + "/0/page/0" };
}

