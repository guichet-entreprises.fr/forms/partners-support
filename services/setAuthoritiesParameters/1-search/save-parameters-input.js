if(_INPUT_.setParameters.search.authority == null){
    return;
}
log.info('=> preprocess save-channel-input.js : ' + _INPUT_.setParameters.search.authority.getId());
log.info('=> _input.setParameters.search.searchButton : ' + _input.setParameters.search.searchButton.value);
// Load the screen for the user
var display = nash.instance.load("display-search.xml");


// Save the value 
display.bind("setParameters.results.input", { "authority": _INPUT_.setParameters.search.authority.getId() });
