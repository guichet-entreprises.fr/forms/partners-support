var directory = require('/js/directory.js');

if(_INPUT_.setParameters.search.authority == null){
    return;
}

_log.info("input is {}", _INPUT_);
var authorityFuncId = _INPUT_.setParameters.search.authority.getId();
_log.info("authorityFuncId is {}", authorityFuncId);
var parameters = directory.transformParameterFromCardinalite(_INPUT_.setParameters.results.parameters);
_log.info("parameters is {}", parameters);
var data = nash.instance.load("display-set.xml");

data.bind("request", { input: { authority: authorityFuncId } });

var debug = {}
var savedParameters = {};
try {
	savedParameters = directory.setAuthorityParameters(authorityFuncId, parameters, debug);
} catch (e) {
	_log.error("error while calling directory : " + e);

	data.bind("request.output", { response: e });
	return;
} finally {
	data.bind("request.output", { url: debug.url, status: debug.status });
}

_log.info("update successful");
data.bind("request.output", { response: "update successful" });

data.bind("request.input", { parameters: directory.transformParameterToCardinalite(savedParameters), authority: debug.authorityLabel });
