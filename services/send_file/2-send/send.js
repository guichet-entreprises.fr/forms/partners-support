
var eddieBaseUrl = _CONFIG_ ? _CONFIG_.get('eddie.file.send.url') : '';
log.info('eddieBaseUrl => ' + eddieBaseUrl);

var token = _input.eddie.results.input.channelId;
log.info('token = _input.eddie.results.input.channelId => ' + token);

var pathFolder = _input.eddie.send.pathFolder ? _input.eddie.send.pathFolder : "";

var comment = "";
var fini =  _input.eddie.send.fini;

var data = nash.instance.load('display-send.xml');
data.bind('request', {
    'input': {
       'token' : token,
       'pathFolder' : pathFolder
    },
    'output' : {
        'url' : eddieBaseUrl
    }
});

var list_file_send = [];
var list_file_not_send = [];

for(var i = 0; i < _input.eddie.send.file.length; i++){
    var pj = _input.eddie.send.file[i]

    var attachment = "/1-search/uploaded/eddie.send.file-" + pj.id + "-" + pj.label;
    var streamName = pathFolder + "/" + pj.label;

    log.info('Nom du fichier à envoyer à Eddie => ' + attachment);

    // Call Eddie service
    try {
        var content = nash.util.resourceFromPath(attachment).getContent();
        var response = nash.service.request('${eddie.file.send.url}/exchange') //
        .connectionTimeout(2000) //
        .receiveTimeout(5000) //
        .dataType("application/octet-stream") //
        .param("uid", token) //
        .param("comment", comment) //
        .param("streamName", streamName)
        .post(content);
        
        log.info('envoi => ' + response.status);
        log.info('response => ' + response);

        // send .fini if needed 
        if (response.status == 200) {
            list_file_send.push(pj.label)
            if(fini){
                // Building the zip.fini file with MD5 content
                var finiAttachmentContent = nash.util.createChecksum(content, 'MD5');
                
                // Returns the next file name (with extension .fini) to send to EDDIE
                var finiFilename = attachment + '.fini';
                
                nash.record.saveFile(finiFilename, finiAttachmentContent);
                
                //Call Eddie to send zip.fini file
                streamName =  pathFolder + "/" + pj.label + ".fini";
                response = nash.service.request('${eddie.file.send.url}/exchange') //
                    .connectionTimeout(2000) //
                    .receiveTimeout(5000) //
                    .dataType("application/octet-stream") //
                    .param("uid", token) //
                    .param("comment", comment) //
                    .param("streamName", streamName)
                    .post(finiAttachmentContent);
                // Response
                if (response.status == 200) {
                    log.info('ok => ' + streamName);
                    list_file_send.push(pj.label + ".fini")

                }
                // Show message with the potential error and block the step
                else {
                    log.info('error =>  response'+ response.status + " " + streamName);
                    list_file_not_send.push(pj.label + ".fini" + " -> " + response.status)
                }
            }
        }
        // Show message with the potential error and block the step
        else {
            list_file_not_send.push(pj.label + " -> " + response.status)
            log.info('error =>  response '+ response.status + " " + streamName);
        }
    // Show error message and block the step
    }catch(error) {
        // Response
        log.info('error while sending to Eddie => ' + error);
        list_file_not_send.push(pj.label + " -> " + error)
    }
}

data.bind('request.output', {
    'ok' : list_file_send.join('\n'),
    'ko' : list_file_not_send.join('\n')
});

