
// try to find the id to search
log.info('=> preprocess search-channel.js : ');
log.info('=> _INPUT_.eddie.results.input.channelId : ' + _INPUT_.eddie.results.input.channelId);
var channelId = _INPUT_.eddie.results.input.channelId;

// Test if a search is needed
if ((channelId == null) || (channelId === "")) {
	return;
};

// Load the screen for the user
var display = nash.instance.load("display-search.xml");


resultsXmlPath = "eddie.results"
var fullUrl = "${eddie.channel.create.url}/channels/list/uid=" + channelId;

//save call data
display.bind(resultsXmlPath + ".input", {
	"channelId": channelId,
	"url": fullUrl,
});

// perform the call
searchResult = null;
try {
	searchResult = nash.service.request(fullUrl) //
		.dataType('application/json') //
		.accept('json')
		.get();

} catch (error) {
	log.info('=> Error occured while calling eddie : ' + error);
	display.bind(resultsXmlPath + ".output", {
		"status": "ERROR",
		"response": "" + error,
		"found": "ko",
	});
	return
};

var response = searchResult.asList();
log.info('=> response : ' + response);

if(response.length == 0){
	display.bind(resultsXmlPath + ".output", {
		"status": searchResult.getStatus(),
		"response": "NOT FOUND",
		"found": "ko",
	});
	return;
}

display.bind(resultsXmlPath + ".output", {
	"status": searchResult.getStatus(),
	"response": "" + response[0],
});

// OK there is a result
display.bind(resultsXmlPath + ".output", {
	"found": "ok",
});

display.bind(resultsXmlPath + ".result", {
	"channel": response[0],
});
