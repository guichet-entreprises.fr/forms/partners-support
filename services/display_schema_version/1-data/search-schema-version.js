// try to find the id to search
log.info('_INPUT_ : {}', _INPUT_);
var applications = _INPUT_.schema.search.application;

// Test if a search is needed
if ( applications == null ) {
	return;
}

log.info('applications : {}', applications);	

var referentials = [
	{ 'app' : 'profiler', 'property': 'forms.profiler.private.url'},
	{ 'app' : 'forms', 'property': 'forms.private.url'},
	{ 'app' : 'directory', 'property': 'forms.directory.private.url'},
	{ 'app' : 'backoffice', 'property': 'forms.backoffice.private.url'},
	{ 'app' : 'support', 'property': 'forms.support.private.url'},
	{ 'app' : 'markov', 'property': 'ws.markov.url'}
];

// --------------------------------------------------------------------------------------
// Find schema versiob user
// --------------------------------------------------------------------------------------
function searchSchemaVersion(applications, xmlDisplay) {
	var schemaVersionResult = [];
	
	referentials.forEach(function(referential) {
		var targetApp = referential.app;
		if ( Value('id').of(applications).contains(referential.app)) {
			try {
				var fullUrl = _CONFIG_.get(referential.property) + '/schema/version';						
				var schemaVersion = nash.service.request(fullUrl) //
					.connectionTimeout(10000) //
					.receiveTimeout(10000) //
					.get() //
					.asString();
					;
				schemaVersionResult.push({
					'name' : targetApp,
					'version' : schemaVersion,
					'url' : fullUrl,
					'message' : 'OK'
				});
			} catch (error) {
				log.error('=> Error occured while calling schema version for application : ' + targetApp);
				schemaVersionResult.push({
					'name' : targetApp,
					'version' : 'UNAVAILABLE',
					'url' : fullUrl,
					'message' : error
				});
			};
		}
	});
	
	return schemaVersionResult;
};
// --------------------------------------------------------------------------------------


// Load the screen for the user
var display = nash.instance.load("display-search.xml");

var schemaVersionResults = searchSchemaVersion(applications, display);

display.bind("schema.results", { "application": schemaVersionResults });