// try to find the id to search
var specUid = _input.specification.search.model.uid;
var specReference = _input.specification.search.model.reference;
var specPriority = _input.specification.search.model.priority;
var specPlatform = _input.specification.search.plateform.target;

// Test if a search is needed
if ( (specUid == null || specUid === "") && (specReference == null || specReference === "") && (specPriority == null || specPriority === "")  && specPlatform == null ) {
	return;
}

var referentials = [
	{ 'app' : 'profiler', 'property': 'forms.profiler.private.url'},
	{ 'app' : 'forms', 'property': 'forms.private.url'},
	{ 'app' : 'directory', 'property': 'forms.directory.private.url'},
	{ 'app' : 'backoffice', 'property': 'forms.backoffice.private.url'},
	{ 'app' : 'support', 'property': 'forms.support.private.url'}
];

// --------------------------------------------------------------------------------------
// Find the specifications
// --------------------------------------------------------------------------------------
function updatePrioritySpecification(uid, reference, platform, priority, xmlDisplay, resultsXmlPath) {

	var target = undefined;
	referentials.forEach(function(referential) {
		var targetApp = referential.app;
		if ( Value('id').of(platform).contains(referential.app)) {
			target = referential.property;
		}
	});	
	
	// perform the call
	if ( undefined == target) {
		return null;
	}
	
	fullUrl = _CONFIG_.get(target) + '/v1/Specification';
	
	var suffixUrl = {
		'uid' : (null != uid && "" !== uid) ? '/code/' + uid + '/priority/' + priority : null,
		'reference' : (null != reference && "" !== reference) ? '/ref/' + reference + '/priority/' + priority : null
	};
	fullUrl = encodeURI(fullUrl + ( suffixUrl['uid'] != null ? suffixUrl['uid'] : suffixUrl['reference'] ));
	
	//save call data
	xmlDisplay.bind(resultsXmlPath + ".input", {
		"uid": uid,
		"reference": reference,
		"platform": platform,
		"url": fullUrl,
	});
	
	try {
		var specificationResult = nash.service.request( fullUrl ) //
			.connectionTimeout(10000) //
			.receiveTimeout(10000) //
			.put(null) //
	} catch (error) {
		log.error('=> Error occured while calling nash : ' + error);
		return;
	}
	
	responseObj = specificationResult.asObject();
	
	xmlDisplay.bind(resultsXmlPath + ".output", {
		"status": specificationResult.getStatus(),
		"response": "" + responseObj,
	});

	// test the 200 code
	if (specificationResult == null || specificationResult.getStatus() != 200) {
		return null;
	}

	return responseObj;
};
// --------------------------------------------------------------------------------------


// Load the screen for the user
var display = nash.instance.load("display.xml");

var specificationRecord = updatePrioritySpecification(specUid, specReference, specPlatform, specPriority, display, "specification.debug.specificationRecords");

if (null == specificationRecord) {
	display.bind("specification.results.specificationRecords", { "nbRecords" : "0" });
	display.bind("specification.results.specificationRecords", { "specification": null });
	return; 
}

display.bind("specification.results.specificationRecords", { "nbRecords" : "1" });
display.bind("specification.results.specificationRecords", { "specification": specificationRecord });
