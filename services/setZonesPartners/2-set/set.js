var directory = require('/js/directory.js');

var content = $majZones.setconfiguration.input.listzones.split(/\r?\n/);
_log.info("content is {}", content);
var configuration = directory.transformParameterFromCardinalite($majZones.setconfiguration.input.configuration);
_log.info("configuration is {}", configuration);

var data = nash.instance.load("display-set.xml");

var listOk = [];
var listKo = [];
for(i=0 ; i < content.length; i++){
	if(content[i] == ""){
		continue;
	}
	var debug = {}
	var savedConfiguration = {};
	try {
		savedConfiguration = directory.updateAuthority(content[i], configuration, debug);
		listOk.push(content[i]);
	} catch (e) {
		_log.error("error while calling directory : " + e);
	
		listKo.push("error while calling directory for : " + content[i] + " (status : " + debug.status + " | " + e);
	} finally {
		data.bind("request.output", { url: debug.url, status: debug.status });
	}
}


data.bind("request", { input: { zones: listOk.join(", ") + "\n" + listKo.join("\n") } });



_log.info("update successful");
data.bind("request.output", { response: "update successful" });

data.bind("request.input", { configuration: directory.transformParameterToCardinalite(configuration), authority: debug.authorityLabel });
